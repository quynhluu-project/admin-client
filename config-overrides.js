const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = function override(config) {
  config.resolve.plugins = [
    new TsconfigPathsPlugin({
      configFile: './tsconfig.json',
    }),
  ].concat(config.resolve.plugins || []);
  return config;
};