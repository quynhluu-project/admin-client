/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {
            boxShadow: {
                'custom-1': 'rgba(17, 17, 26, 0.1) 0px 0px 16px',
                'custom-2': 'rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px',
                'custom-3': 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
                'custom-4': '1px 1px 10px rgba(0,0,0,.15)',
            },
            container: {
                padding: {
                    DEFAULT: '1rem',
                    sm: '1rem',
                    lg: '1rem',
                    xl: '1rem',
                    '2xl': '1rem',
                },
            },
        },
    },
    plugins: [],
};
