import { useState, useEffect, useContext } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import DefaultLayout from './layouts';
import { privateRoutes, publicRoutes } from './routes';
import { isAuthenticated } from './core/util/auth';
import AppLoading from './core/common/components/app-loading';
import { AccessControl } from './core/common/constants';
import { socket } from '~/core/socket';

function PrivateRoute({ children, accessControl }: { children: JSX.Element; accessControl: AccessControl[] }) {
    const { isLogin, profile } = isAuthenticated();

    const permission = AccessControl.OWNER;
    if (accessControl.length > 0) {
        if (!accessControl?.includes(permission)) {
            return <Navigate to={'/'} />;
        }
    }
    return isLogin ? <>{children}</> : <Navigate to="/login" />;
}

function App() {
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        console.log(socket.id);

        window.addEventListener('beforeunload', () => setLoading(true));
        window.addEventListener('load', () => setLoading(false));

        return () => {
            window.removeEventListener('beforeunload', () => setLoading(true));
            window.removeEventListener('load', () => setLoading(false));
        };
    }, []);
    return loading ? (
        <main>
            <div className="w-screen h-screen">
                <AppLoading />
            </div>
        </main>
    ) : (
        <BrowserRouter>
            <div className="app">
                <Routes>
                    {publicRoutes.map((route) => {
                        const Page = route.component;
                        switch (route.layout) {
                            case null:
                                return <Route key={route.path} path={route.path} element={<Page />}></Route>;
                            default:
                                return (
                                    <Route key={route.path} path="/" element={<DefaultLayout />}>
                                        <Route path={route.path} element={<Page />}></Route>;
                                    </Route>
                                );
                        }
                    })}
                    {privateRoutes.map((route) => {
                        const Page = route.component;
                        switch (route.layout) {
                            case null:
                                return (
                                    <Route
                                        key={route.path}
                                        path={route.path}
                                        element={
                                            <PrivateRoute
                                                accessControl={route.accessControl ? [...route.accessControl] : []}
                                            >
                                                <Page />
                                            </PrivateRoute>
                                        }
                                    />
                                );
                            default:
                                return (
                                    <Route
                                        key={route.path}
                                        path="/"
                                        element={
                                            <PrivateRoute
                                                accessControl={route.accessControl ? [...route.accessControl] : []}
                                            >
                                                <DefaultLayout />
                                            </PrivateRoute>
                                        }
                                    >
                                        <Route path={route.path} element={<Page />} />;
                                    </Route>
                                );
                        }
                    })}
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
