import { FormikConfig, useFormik, FormikValues } from 'formik';

function useFormHandler<Value extends FormikValues>({
    initialValues,
    validationSchema,
    validate,
}: FormikConfig<Value>) {
    const formik = useFormik({
        initialValues,
        validationSchema,
        validate,
        onSubmit: () => {},
    });

    return formik;
}

export default useFormHandler;
