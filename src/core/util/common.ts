import { notification } from 'antd';
export const convertVnd = (money: number) => {
    if (!money) return '0đ';
    return money.toLocaleString('vi-VN', {
        style: 'currency',
        currency: 'VND',
    });
};

type NotificationArgs = {
    type: 'error' | 'success' | 'warning' | 'info';
    title: React.ReactNode;
    description: React.ReactNode;
};

const showMessageNotification = ({ type, title, description }: NotificationArgs) => {
    notification[type]({
        message: title,
        description,
    });
};

export { showMessageNotification };
