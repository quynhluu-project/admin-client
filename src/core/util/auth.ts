import { LOCAL_STORAGE_KEY } from '../common/constants';
import LocalStorage from './storage';
import jwt_decode from 'jwt-decode';
const isAuthenticated = () => {
    const token = LocalStorage.get(LOCAL_STORAGE_KEY.ACCESS_TOKEN);
    let profile = {} as any;
    if (token) {
        const authSession = jwt_decode(token || '');
        LocalStorage.set(LOCAL_STORAGE_KEY.PROFILE, JSON.stringify(authSession));
        profile = authSession;
    }
    return { profile, isLogin: !!token };
};

function generatePassword() {
    const length = 14;
    const uppercaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const lowercaseChars = 'abcdefghijklmnopqrstuvwxyz';
    const numericChars = '0123456789';
    const specialChars = '!@#$%^&*';

    let password = '';

    password += uppercaseChars[Math.floor(Math.random() * uppercaseChars.length)];

    password += numericChars[Math.floor(Math.random() * numericChars.length)];

    password += specialChars[Math.floor(Math.random() * specialChars.length)];

    for (let i = 0; i < length - 3; i++) {
        password += lowercaseChars[Math.floor(Math.random() * lowercaseChars.length)];
    }

    password = shuffleString(password);

    return password;
}

function shuffleString(str: string) {
    let shuffledStr = '';
    const strArray = str.split('');

    while (strArray.length > 0) {
        const randomIndex = Math.floor(Math.random() * strArray.length);
        shuffledStr += strArray.splice(randomIndex, 1);
    }

    return shuffledStr;
}
export { isAuthenticated, generatePassword };
