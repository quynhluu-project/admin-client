export const TEMPLATE_ONE = `<p style="text-align: center;"><strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></p>
<p style="text-align: center;">Độc lập - Tự do- Hạnh ph&uacute;c</p>
<p style="text-align: center;">-------------------</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>HỢP ĐỒNG HỢP T&Aacute;C KINH DOANH</strong></p>
<p>Số: ......./HDHTKD</p>
<p><em>- Căn cứ Bộ Luật d&acirc;n sự nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam năm 2015</em></p>
<p><em>- Căn cứ v&agrave;o khả năng v&agrave; nhu cầu của hai b&ecirc;n.</em></p>
<p><em>- Dựa tr&ecirc;n tinh thần trung thực v&agrave; thiện ch&iacute; hợp t&aacute;c của c&aacute;c b&ecirc;n.</em></p>
<p><em>Ch&uacute;ng t&ocirc;i gồm c&oacute;:</em><br><br></p>
<p><strong>1. C&Ocirc;NG TY TNHH .........................&nbsp;</strong><em>(Sau đ&acirc;y gọi tắt l&agrave; B&ecirc;n A)</em></p>
<p>Trụ sở: ..................................................................................&nbsp;</p>
<p>GCNĐKKD số: ......... do Ph&ograve;ng ĐKKD &ndash; Sở Kế hoạch v&agrave; Đầu tư .....&nbsp; cấp ng&agrave;y: ....... ;</p>
<p>Số t&agrave;i khoản: ..........................................................................&nbsp;</p>
<p>Điện thoại: .............................................................................&nbsp;</p>
<p>Người đại diện: ....................................................................&nbsp;</p>
<p>Chức vụ: Gi&aacute;m đốc</p>
<p><strong>V&agrave;</strong><br><br></p>
<p><strong>2. C&Ocirc;NG TY &hellip;&hellip;&hellip;...............</strong><em>&nbsp;(Sau đ&acirc;y gọi tắt l&agrave; B&ecirc;n B)</em></p>
<p>Trụ sở: ...............................................................................&nbsp;</p>
<p>GCNĐKKD số: ...... do Ph&ograve;ng ĐKKD &ndash; Sở Kế hoạch v&agrave; Đầu tư&nbsp;..&nbsp;</p>
<p>cấp ng&agrave;y: ......................;</p>
<p>Số t&agrave;i khoản: ......................................................................&nbsp;</p>
<p>Điện thoại: ..........................................................................&nbsp;</p>
<p>Người đại diện: ................................................................&nbsp;</p>
<p>Chức vụ: Gi&aacute;m đốc</p>
<p>C&ugrave;ng thoả thuận k&yacute; Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y với c&aacute;c điều khoản v&agrave; điều kiện sau đ&acirc;y:</p>
<p>&nbsp;</p>
<p><strong>Điều 1. Mục ti&ecirc;u v&agrave; phạm vi hợp t&aacute;c kinh doanh</strong></p>
<p>B&ecirc;n A v&agrave; B&ecirc;n B nhất tr&iacute; c&ugrave;ng nhau hợp t&aacute;c ....................&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Điều 2. Thời hạn hợp đồng.</strong></p>
<p>Thời hạn hợp t&aacute;c l&agrave; 05 (năm) năm bắt đầu kể từ ng&agrave;y 28 th&aacute;ng 11 năm ............đến hết ng&agrave;y 28 th&aacute;ng 11 năm ................Thời hạn tr&ecirc;n c&oacute; thể được k&eacute;o d&agrave;i theo sự thoả thuận của hai b&ecirc;n.</p>
<p>&nbsp;</p>
<p><strong>Điều 3. G&oacute;p vốn v&agrave; ph&acirc;n chia kết quả kinh doanh</strong></p>
<p><strong>3.1. G&oacute;p vốn</strong></p>
<p>B&ecirc;n A g&oacute;p vốn bằng to&agrave;n bộ gi&aacute; trị lượng phế liệu nhập khẩu về Việt Nam để t&aacute;i chế ph&ugrave; hợp với khả năng sản xuất của Nh&agrave; m&aacute;y. Gi&aacute; trị tr&ecirc;n bao gồm to&agrave;n bộ c&aacute;c chi ph&iacute; để h&agrave;ng nhập về tới Nh&agrave; m&aacute;y.</p>
<p>B&ecirc;n B g&oacute;p vốn bằng to&agrave;n bộ quyền sử dụng nh&agrave; xưởng, kho b&atilde;i, m&aacute;y m&oacute;c, d&acirc;y chuyền, thiết bị của Nh&agrave; m&aacute;y thuộc quyền sở hữu của m&igrave;nh để phục vụ cho qu&aacute; tr&igrave;nh sản xuất.</p>
<p>&nbsp;</p>
<p><strong>3.2. Ph&acirc;n chia kết quả kinh doanh</strong></p>
<p>3.2.1 Lợi nhuận từ hoạt động ....................&nbsp;</p>
<p>Lợi nhuận sẽ được chia theo tỷ lệ: B&ecirc;n A được hưởng .............%, B&ecirc;n B được hưởng ............% tr&ecirc;n tổng lợi nhuận sau khi đ&atilde; ho&agrave;n th&agrave;nh c&aacute;c nghĩa vụ với Nh&agrave; nước.</p>
<p>Thời điểm chia lợi nhuận v&agrave;o ng&agrave;y cuối c&ugrave;ng của năm t&agrave;i ch&iacute;nh. Năm t&agrave;i ch&iacute;nh được t&iacute;nh bắt đầu kể từ ng&agrave;y: ...../....../...................</p>
<p>3.2.2 Chi ph&iacute; cho hoạt động sản xuất bao gồm:</p>
<p>+ Tiền mua phế liệu;</p>
<p>+ Lương nh&acirc;n vi&ecirc;n;</p>
<p>+ Chi ph&iacute; điện, nước;</p>
<p>+ Khấu hao t&agrave;i sản;</p>
<p>+ Chi ph&iacute; bảo dưỡng m&aacute;y m&oacute;c, thiết bị, nh&agrave; xưởng;</p>
<p>+ Chi ph&iacute; kh&aacute;c...</p>
<p>&nbsp;</p>
<p><strong>Điều 4. C&aacute;c nguy&ecirc;n tắc t&agrave;i ch&iacute;nh</strong></p>
<p>Hai b&ecirc;n phải tu&acirc;n thủ c&aacute;c nguy&ecirc;n tắc t&agrave;i ch&iacute;nh kế to&aacute;n theo qui định của ph&aacute;p luật về kế to&aacute;n của nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam.</p>
<p>Mọi khoản thu chi cho hoạt động kinh doanh đều phải được ghi ch&eacute;p r&otilde; r&agrave;ng, đầy đủ, x&aacute;c thực.</p>
<p>&nbsp;</p>
<p><strong>Điều 5. Ban điều h&agrave;nh hoạt động kinh doanh</strong></p>
<p>Hai b&ecirc;n sẽ th&agrave;nh lập một Ban điều h&agrave;nh hoạt động kinh doanh gồm 03 người trong đ&oacute; B&ecirc;n A sẽ cử 01 (một), B&ecirc;n B sẽ cử 02 (hai) đại diện khi cần phải đưa ra c&aacute;c quyết định li&ecirc;n quan đến nội dung hợp t&aacute;c được quy định tại Hợp đồng n&agrave;y. Mọi quyết định của Ban điều h&agrave;nh sẽ được th&ocirc;ng qua khi c&oacute; &iacute;t nhất hai th&agrave;nh vi&ecirc;n đồng &yacute;.</p>
<p>Đại diện của B&ecirc;n A l&agrave;: B&agrave;&nbsp;<strong>&hellip;&hellip;</strong><strong>&hellip;.</strong>.&nbsp;&ndash; Ph&oacute; gi&aacute;m đốc</p>
<p>Đại diện của B&ecirc;n B l&agrave;: &Ocirc;ng&nbsp;<strong>&hellip;&hellip;&hellip;</strong>&nbsp;&ndash; Tổng gi&aacute;m đốc</p>
<p>B&agrave;&nbsp;<strong>&hellip;&hellip;&hellip;..................................</strong>&nbsp;&ndash; Ph&oacute; gi&aacute;m đốc</p>
<p>Trụ sở của ban điều h&agrave;nh đặt tại: .......................</p>
<p>&nbsp;</p>
<p><strong>Điều 6. Quyền v&agrave; nghĩa vụ của B&ecirc;n A</strong></p>
<p>6.1 Chịu tr&aacute;ch nhiệm nhập khẩu ............................................</p>
<p>6.2 T&igrave;m kiếm, đ&agrave;m ph&aacute;n, k&yacute; kết, thanh to&aacute;n hợp đồng mua phế liệu với c&aacute;c nh&agrave; cung cấp phế liệu trong v&agrave; ngo&agrave;i nước.</p>
<p>6.3 Cung cấp đầy đủ c&aacute;c ho&aacute; đơn, chứng từ li&ecirc;n quan để phục vụ cho c&ocirc;ng t&aacute;c hạch to&aacute;n t&agrave;i ch&iacute;nh qu&aacute; tr&igrave;nh kinh doanh.</p>
<p>6.4 Được hưởng .......................% lợi nhuận sau thuế.</p>
<p>&nbsp;</p>
<p><strong>Điều 7. Quyền v&agrave; nghĩa vụ của b&ecirc;n B</strong></p>
<p>7.1 C&oacute; tr&aacute;ch nhiệm quản l&yacute;, điều h&agrave;nh to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất. Đưa nh&agrave; xưởng, kho b&atilde;i, m&aacute;y m&oacute;c thiết bị thuộc quyền sở hữu của m&igrave;nh v&agrave;o sử dụng. Đảm bảo ph&ocirc;i th&eacute;p được sản xuất ra c&oacute; chất lượng đ&aacute;p ứng ti&ecirc;u chuẩn của ph&aacute;p luật hiện h&agrave;nh.</p>
<p>7.2 Triệt để tu&acirc;n thủ c&aacute;c quy định của ph&aacute;p luật về bảo vệ m&ocirc;i trường v&agrave; c&aacute;c quy định kh&aacute;c của ph&aacute;p luật trong qu&aacute; tr&igrave;nh sản xuất.</p>
<p>7.3 C&oacute; tr&aacute;ch nhiệm triển khai b&aacute;n sản phẩm &ndash; ph&ocirc;i th&eacute;p tr&ecirc;n thị trường Việt Nam.</p>
<p>7.4 Hạch to&aacute;n to&agrave;n bộ thu chi của qu&aacute; tr&igrave;nh sản xuất kinh doanh theo đ&uacute;ng c&aacute;c quy định của ph&aacute;p luật về t&agrave;i ch&iacute;nh kế to&aacute;n của Việt Nam.</p>
<p>7.5 C&oacute; tr&aacute;ch nhiệm k&ecirc; khai, nộp đầy đủ thuế v&agrave; c&aacute;c nghĩa vụ kh&aacute;c với Nh&agrave; nước. Đồng thời quan hệ với cơ quan quản l&yacute; nh&agrave; nước ng&agrave;nh v&agrave; địa phương, cơ quan thuế nơi c&oacute; Nh&agrave; m&aacute;y.</p>
<p>7.6 Được hưởng ............................% lợi nhuận sau thuế.</p>
<p>7.7 Trực tiếp chịu tr&aacute;ch nhiệm tuyển dụng, quản l&yacute;, điều động c&aacute;n bộ, c&ocirc;ng nh&acirc;n tại Nh&agrave; m&aacute;y. L&ecirc;n kế hoạch Trả lương v&agrave; c&aacute;c chế độ kh&aacute;c cho c&ocirc;ng nh&acirc;n, c&aacute;n bộ l&agrave;m việc tại Nh&agrave; m&aacute;y</p>
<p>&nbsp;</p>
<p><strong>Điều 8. Điều khoản chung</strong></p>
<p>8.1. Hợp đồng n&agrave;y được hiểu v&agrave; chịu sự điều chỉnh của Ph&aacute;p luật nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam.</p>
<p>8.2. Hai b&ecirc;n cam kết thực hiện tất cả những điều khoản đ&atilde; cam kết trong hợp đồng. B&ecirc;n n&agrave;o vi phạm hợp đồng g&acirc;y thiệt hại cho b&ecirc;n kia (trừ trong trường hợp bất khả kh&aacute;ng) th&igrave; phải bồi thường to&agrave;n bộ thiệt hại xảy ra v&agrave; chịu phạt vi phạm hợp đồng bằng 10% gi&aacute; trị hợp đồng.</p>
<p>Trong qu&aacute; tr&igrave;nh thực hiện hợp đồng nếu b&ecirc;n n&agrave;o c&oacute; kh&oacute; khăn trở ngại th&igrave; phải b&aacute;o cho b&ecirc;n kia trong v&ograve;ng 1 (một) th&aacute;ng kể từ ng&agrave;y c&oacute; kh&oacute; khăn trở ngại.</p>
<p>8.3. C&aacute;c b&ecirc;n c&oacute; tr&aacute;ch nhiệm th&ocirc;ng tin kịp thời cho nhau tiến độ thực hiện c&ocirc;ng việc. Đảm bảo b&iacute; mật mọi th&ocirc;ng tin li&ecirc;n quan tới qu&aacute; tr&igrave;nh sản xuất kinh doanh.</p>
<p>Mọi sửa đổi, bổ sung hợp đồng n&agrave;y đều phải được l&agrave;m bằng văn bản v&agrave; c&oacute; chữ k&yacute; của hai b&ecirc;n. C&aacute;c phụ lục l&agrave; phần kh&ocirc;ng t&aacute;ch rời của hợp đồng.</p>
<p>8.4 Mọi tranh chấp ph&aacute;t sinh trong qu&aacute; tr&igrave;nh thực hiện hợp đồng được giải quyết trước hết qua thương lượng, ho&agrave; giải, nếu ho&agrave; giải kh&ocirc;ng th&agrave;nh việc tranh chấp sẽ được giải quyết tại To&agrave; &aacute;n c&oacute; thẩm quyền.</p>
<p>&nbsp;</p>
<p><strong>Điều 9. Hiệu lực Hợp đồng</strong></p>
<p>9.1. Hợp đồng chấm dứt khi hết thời hạn hợp đồng theo quy định tại Điều 2 Hợp đồng n&agrave;y hoặc c&aacute;c trường hợp kh&aacute;c theo qui định của ph&aacute;p luật.</p>
<p>Khi kết th&uacute;c Hợp đồng, hai b&ecirc;n sẽ l&agrave;m bi&ecirc;n bản thanh l&yacute; hợp đồng. Nh&agrave; xưởng, nh&agrave; kho, m&aacute;y m&oacute;c, d&acirc;y chuyền thiết bị &hellip;.sẽ&nbsp;được trả lại cho B&ecirc;n B.</p>
<p>9.2. Hợp đồng n&agrave;y gồm 04 (bốn) trang kh&ocirc;ng thể t&aacute;ch rời nhau, được lập th&agrave;nh 02 (hai) bản bằng tiếng Việt, mỗi B&ecirc;n giữ 01 (một) bản c&oacute; gi&aacute; trị ph&aacute;p l&yacute; như nhau v&agrave; c&oacute; hiệu lực kể từ ng&agrave;y k&yacute;.</p>
<table style="height: 59px; width: 86.0192%;" cellspacing="1">
<tbody>
<tr>
<td style="width: 48.4287%;">
<p style="text-align: center;"><strong>Đại diện b&ecirc;n A</strong></p>
</td>
<td style="width: 48.027%;">
<p><strong>Đại diện b&ecirc;n B</strong></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>`;

export const TEMPLATE_TWO = `<p><strong>CỘNG HO&Agrave; X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></p>
<p>Độc lập - Tự do - Hạnh ph&uacute;c</p>
<p>&nbsp;</p>
<p><strong>HỢP ĐỒNG HỢP T&Aacute;C KINH DOANH</strong></p>
<p>Số: &hellip;/HĐHTKD</p>
<p>&nbsp;</p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Căn cứ theo quy định của&nbsp;<u>Bộ luật d&acirc;n sự năm 2015</u>;</p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Căn cứ theo quy định của&nbsp;<u>Luật thương mại năm 2005</u>;</p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Căn cứ v&agrave;o t&igrave;nh h&igrave;nh thực tế của Hai b&ecirc;n;</p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dựa tr&ecirc;n tinh thần trung thực v&agrave; thiện ch&iacute; hợp t&aacute;c của c&aacute;c B&ecirc;n;</p>
<p><strong>Ch&uacute;ng t&ocirc;i gồm c&oacute;:</strong>&nbsp;</p>
<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
<p><strong>(Sau đ&acirc;y gọi tắt l&agrave; B&ecirc;n A)</strong></p>
<p>Địa chỉ&nbsp;:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>Đại diện :&nbsp;&Ocirc;ng &hellip;&hellip;&nbsp; Chức vụ: &hellip;&hellip;&hellip;&hellip;.&hellip;..&nbsp;</p>
<p>Điện thoại :&nbsp;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;................&nbsp;</p>
<p>Số t&agrave;i khoản :&nbsp;&hellip;&hellip;&hellip;&hellip;&nbsp; tại: &hellip;&hellip;&hellip;&hellip;&hellip;.......&nbsp;</p>
<p><strong>V&agrave;</strong></p>
<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p><strong>(Sau đ&acirc;y gọi tắt l&agrave; B&ecirc;n B)</strong></p>
<p>Địa chỉ:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>Đại diện :&nbsp;&Ocirc;ng&hellip;&hellip;&hellip;&hellip; Chức vụ :&hellip;&hellip;...&hellip;&hellip;&nbsp;</p>
<p>Điện thoại :</p>
<p>Số t&agrave;i khoản :&nbsp;tại:</p>
<p>C&ugrave;ng thỏa thuận k&yacute;&nbsp;<u>hợp đồng hợp t&aacute;c kinh doanh</u>&nbsp;với những điều khoản sau đ&acirc;y:</p>
<p>&nbsp;</p>
<p><strong>Điều 1. Mục ti&ecirc;u v&agrave; phạm vi hợp t&aacute;c kinh doanh</strong></p>
<p><strong>1. Mục ti&ecirc;u hợp t&aacute;c kinh doanh</strong></p>
<p>B&ecirc;n A v&agrave; B&ecirc;n B nhất tr&iacute; c&ugrave;ng nhau hợp t&aacute;c kinh doanh, điều h&agrave;nh v&agrave; chia sẻ lợi nhuận c&oacute; được từ việc hợp t&aacute;c kinh doanh.</p>
<p>&nbsp;</p>
<p><strong>2. Phạm vi hợp t&aacute;c kinh doanh</strong></p>
<p>Hai b&ecirc;n c&ugrave;ng nhau hợp t&aacute;c kinh doanh, điều h&agrave;nh, quản l&yacute; hoạt động kinh doanh để c&ugrave;ng ph&aacute;t sinh lợi nhuận</p>
<p>+ Phạm vi Hợp t&aacute;c của B&ecirc;n A</p>
<p>B&ecirc;n A chịu tr&aacute;ch nhiệm quản l&yacute; chung mặt bằng kinh doanh v&agrave; định hướng ph&aacute;t triển kinh doanh</p>
<p>+ Phạm vi Hợp t&aacute;c của B&ecirc;n B</p>
<p>B&ecirc;n B chịu tr&aacute;ch nhi&ecirc;m điều h&agrave;nh to&agrave;n bộ qu&aacute; tr&igrave;nh kinh doanh của c&aacute;c sản phẩm, dịch vụ như:</p>
<p>- T&igrave;m kiếm, đ&agrave;m ph&aacute;n k&yacute; kết, thanh to&aacute;n hợp đồng với c&aacute;c nh&agrave; cung cấp nguy&ecirc;n liệu;</p>
<p>- Tuyển dụng, đ&agrave;o tạo, quản l&yacute; nh&acirc;n sự phục vụ cho&nbsp;<u>hoạt động kinh doanh</u>&nbsp;trong phạm vi hợp t&aacute;c;</p>
<p>- Đầu tư x&uacute;c tiến ph&aacute;t triển hoạt động thương mại trong phạm vi hợp t&aacute;c&hellip;;</p>
<p>&nbsp;</p>
<p><strong>Điều 2. Thời hạn của hợp đồng</strong></p>
<p><strong>Thời hạn của hợp đồng</strong>: l&agrave; &hellip; (&hellip;năm) bắt đầu từ ng&agrave;y &hellip; th&aacute;ng &hellip; năm &hellip; đến ng&agrave;y &hellip; th&aacute;ng &hellip; năm &hellip;;</p>
<p><strong>Gia hạn hợp đồng:</strong>&nbsp;Hết thời hạn tr&ecirc;n hai b&ecirc;n c&oacute; thể thỏa thuận gia hạn th&ecirc;m thời hạn của hợp đồng hoặc thỏa thuận k&yacute; kết hợp đồng mới t&ugrave;y v&agrave;o điều kiện kinh doanh của Hai b&ecirc;n;</p>
<p>&nbsp;</p>
<p><strong>Điều 3. G&oacute;p vốn v&agrave; ph&acirc;n chia kết quả kinh doanh</strong></p>
<p><strong>3.1. G&oacute;p vốn</strong></p>
<p>B&ecirc;n A g&oacute;p vốn bằng: &hellip;&hellip;&hellip;..&nbsp;tương đương với số tiền l&agrave; &hellip;&hellip;&nbsp;</p>
<p>B&ecirc;n B g&oacute;p vốn bằng: B&ecirc;n A g&oacute;p vốn bằng: &hellip;&hellip; tương đương với số tiền l&agrave; &hellip;&hellip;&nbsp;</p>
<p>&nbsp;</p>
<p><strong>3.2. Ph&acirc;n chia kết quả kinh doanh</strong></p>
<p>3.2.1. Tỷ lệ ph&acirc;n chia: Lợi nhuận từ hoạt động kinh doanh được chia như sau B&ecirc;n A được hưởng &hellip;. %, B&ecirc;n B được hưởng &hellip;..&nbsp;% tr&ecirc;n lợi nhuận sau khi đ&atilde; ho&agrave;n th&agrave;nh c&aacute;c nghĩa vụ với Nh&agrave; nước;</p>
<p>3.2.2. Thời điểm chia lợi nhuận: Ng&agrave;y cuối c&ugrave;ng của năm t&agrave;i ch&iacute;nh. Năm t&agrave;i ch&iacute;nh được t&iacute;nh bắt đầu từ ng&agrave;y 1/1 đến ng&agrave;y 31/12 của năm ri&ecirc;ng năm 2018 năm t&agrave;i ch&iacute;nh được hiểu từ thời điểm hợp đồng n&agrave;y c&oacute; hiệu lực đến ng&agrave;y ..../..../20...;</p>
<p>3.2.3. Trường hợp hoạt động kinh doanh ph&aacute;t sinh lỗ: Hai b&ecirc;n phải c&ugrave;ng nhau giải thỏa thuận giải quyết, trường hợp kh&ocirc;ng thỏa thuận được sẽ thực hiện theo việc đ&oacute;ng g&oacute;p như quy định tải Khoản 3.1 Điều 3 của luật n&agrave;y để b&ugrave; đắp chi ph&iacute; v&agrave; tiếp tục hoạt động kinh daonh.</p>
<p>&nbsp;</p>
<p><strong>Điều 4. C&aacute;c nguy&ecirc;n tắc t&agrave;i ch&iacute;nh</strong></p>
<p>4.1. Hai b&ecirc;n phải tu&acirc;n thủ c&aacute;c nguy&ecirc;n tắc t&agrave;i ch&iacute;nh kế to&aacute;n theo qui định của ph&aacute;p luật về kế to&aacute;n của nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam;</p>
<p>4.2. Mọi khoản thu chi cho hoạt động kinh doanh đều phải được ghi ch&eacute;p r&otilde; r&agrave;ng, đầy đủ, x&aacute;c thực.</p>
<p>&nbsp;</p>
<p><strong>Điều 5. Ban điều h&agrave;nh hoạt động kinh doanh</strong></p>
<p><strong>5.1. Th&agrave;nh vi&ecirc;n ban điều h&agrave;nh:&nbsp;</strong>Hai b&ecirc;n sẽ th&agrave;nh lập một Ban điều h&agrave;nh hoạt động kinh doanh gồm &hellip;. người trong đ&oacute; B&ecirc;n A sẽ &hellip;. ,&nbsp;B&ecirc;n B sẽ cử &hellip;. Cụ thể ban điều h&agrave;nh gồm những người c&oacute; t&ecirc;n sau:</p>
<p><strong>-&nbsp;</strong>Đại diện của B&ecirc;n A l&agrave;: &Ocirc;ng/B&agrave; &hellip;&hellip;..&nbsp;&ndash;</p>
<p><strong>-&nbsp;</strong>Đại diện của B&ecirc;n B l&agrave;: &Ocirc;ng/B&agrave; &hellip;&hellip;&hellip;&hellip; -</p>
<p><strong>-&nbsp;</strong>&Ocirc;ng: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
<p>&nbsp;</p>
<p><strong>5.2. H&igrave;nh thức biểu quyết của ban điều h&agrave;nh:</strong></p>
<p>Khi cần phải đưa ra c&aacute;c quyết định li&ecirc;n quan đến nội dung hợp t&aacute;c được quy định tại Hợp đồng n&agrave;y. Mọi quyết định của Ban điều h&agrave;nh sẽ được th&ocirc;ng qua khi c&oacute; &iacute;t nhất hai th&agrave;nh vi&ecirc;n đồng &yacute;;</p>
<p>Việc Biểu quyết phải được lập th&agrave;nh Bi&ecirc;n bản chữ k&yacute; x&aacute;c nhận của c&aacute;c Th&agrave;nh vi&ecirc;n trong Ban điều h&agrave;nh;</p>
<p>&nbsp;</p>
<p><strong>5.3. Trụ sở ban điều h&agrave;nh đặt tại:&nbsp;</strong>&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Điều 6. Quyền v&agrave; nghĩa vụ của B&ecirc;n A</strong></p>
<p><strong>6.1. Quyền của B&ecirc;n A</strong></p>
<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>&hellip;.&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>- Được hưởng&hellip;..% lợi nhuận sau thuế từ hoạt động kinh doanh;</p>
<p>&nbsp;</p>
<p><strong>6.2. Nghĩa vụ của B&ecirc;n A</strong>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Điều 7. Quyền v&agrave; nghĩa vụ của b&ecirc;n B</strong></p>
<p><strong>7.1. Quyền của B&ecirc;n B</strong>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>Được ph&acirc;n chia &hellip;% lợi nhận sau thuế.</p>
<p>&nbsp;</p>
<p><strong>7.2. Nghĩa vụ của B&ecirc;n B</strong></p>
<p><strong>-&nbsp;</strong>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p>&nbsp;</p>
<p>&nbsp;<strong>Điều 8. Điều khoản chung</strong></p>
<p>8.1. Hợp đồng n&agrave;y được hiểu v&agrave; chịu sự điều chỉnh của Ph&aacute;p luật nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam;</p>
<p>8.2. Hai b&ecirc;n cam kết thực hiện tất cả những điều khoản đ&atilde; cam kết trong hợp đồng. B&ecirc;n n&agrave;o vi phạm hợp đồng g&acirc;y thiệt hại cho b&ecirc;n kia (trừ trong trường hợp bất khả kh&aacute;ng) th&igrave; phải bồi thường to&agrave;n bộ thiệt hại xảy ra v&agrave; chịu phạt vi phạm hợp đồng bằng 8% phần gi&aacute; trị hợp đồng bị vi phạm;</p>
<p>8.3. Trong qu&aacute; tr&igrave;nh thực hiện hợp đồng nếu b&ecirc;n n&agrave;o c&oacute; kh&oacute; khăn trở ngại th&igrave; phải b&aacute;o cho b&ecirc;n kia trong v&ograve;ng 1 (một) th&aacute;ng kể từ ng&agrave;y c&oacute; kh&oacute; khăn trở ngại.</p>
<p>8.4. C&aacute;c b&ecirc;n c&oacute; tr&aacute;ch nhiệm th&ocirc;ng tin kịp thời cho nhau tiến độ thực hiện c&ocirc;ng việc. Đảm bảo b&iacute; mật mọi th&ocirc;ng tin li&ecirc;n quan tới qu&aacute; tr&igrave;nh kinh doanh;</p>
<p>8.5. Mọi sửa đổi, bổ sung hợp đồng n&agrave;y đều phải được l&agrave;m bằng văn bản v&agrave; c&oacute; chữ k&yacute; của hai b&ecirc;n. C&aacute;c phụ lục l&agrave; phần kh&ocirc;ng t&aacute;ch rời của hợp đồng;</p>
<p>8.6. Mọi tranh chấp ph&aacute;t sinh trong qu&aacute; tr&igrave;nh thực hiện hợp đồng được giải quyết trước hết qua thương lượng, ho&agrave; giải, nếu ho&agrave; giải kh&ocirc;ng th&agrave;nh việc tranh chấp sẽ được giải quyết tại To&agrave; &aacute;n c&oacute; thẩm quyền.</p>
<p>&nbsp;</p>
<p><strong>Điều 9. Hiệu lực Hợp đồng</strong></p>
<p>9.1. Hợp đồng chấm dứt khi hết thời hạn hợp đồng theo quy định tại Điều 2 Hợp đồng n&agrave;y hoặc c&aacute;c trường hợp kh&aacute;c theo quy định của ph&aacute;p luật. Khi kết th&uacute;c Hợp đồng, hai b&ecirc;n sẽ l&agrave;m bi&ecirc;n bản thanh l&yacute; hợp đồng.</p>
<p>9.2. Hợp đồng n&agrave;y được lập th&agrave;nh 02 (hai) bản bằng tiếng Việt, mỗi B&ecirc;n giữ 01 (một) bản c&oacute; gi&aacute; trị ph&aacute;p l&yacute; như nhau v&agrave; c&oacute; hiệu lực kể từ ng&agrave;y k&yacute;.</p>
<table cellspacing="1">
<tbody>
<tr>
<td>
<p><strong>Đại diện b&ecirc;n A</strong></p>
</td>
<td>
<p><strong>Đại diện b&ecirc;n B</strong></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>`;
export const TEMPLATE_THREE = `<p><strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></p>
<p>Độc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</p>
<p>-------***-------</p>
<p>&nbsp;</p>
<p><strong>HỢP ĐỒNG HỢP T&Aacute;C KINH DOANH</strong></p>
<p>Số: ......./20&hellip;/...-.../HDHTKD</p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Căn cứ qui định tại Bộ luật d&acirc;n sự năm 2015 do Quốc hội nước CHXHCN Việt Nam ban h&agrave;nh;</em></p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Căn cứ luật Luật Thương mại số&nbsp;36/2005/QH11 do Quốc hội nước CHXHCN Việt Nam ban h&agrave;nh ng&agrave;y 14 th&aacute;ng 06 năm 2005;</em></p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Căn cứ hợ đồng cung ứng dịch vụ số&nbsp;</em><em>&hellip;./</em><em>20&hellip;/HĐDV được lập ng&agrave;y&hellip;.th&aacute;ng &hellip; năm 20&hellip;.;</em></p>
<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Căn cứ v&agrave;o khả năng v&agrave; nhu cầu của hai b&ecirc;n v&agrave; dựa tr&ecirc;n tinh thần trung thực v&agrave; thiện ch&iacute; hợp t&aacute;c của c&aacute;c b&ecirc;n.</em></p>
<p>&nbsp;</p>
<p><em>Ch&uacute;ng t&ocirc;i gồm c&oacute;:</em></p>
<p><strong>B&Ecirc;N A:&nbsp;C&Ocirc;NG TY TNHH DỊCH VỤ THƯƠNG MẠI QUỐC TẾ &hellip;&nbsp;</strong></p>
<p>Địa chỉ trụ sở :&nbsp;Số &hellip;.., phố &hellip;., phường &hellip;., quận/huyện...., tỉnh/th&agrave;nh phố.....</p>
<p>Người đại diện :&nbsp;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>M&atilde; số thuế :&nbsp;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>v&agrave;</p>
<p><strong>B&Ecirc;N B: C&Ocirc;NG TY CỔ PHẦN X&Acirc;Y DỰNG &ndash; THƯƠNG MẠI &hellip;&hellip;&nbsp;</strong></p>
<p>Địa chỉ trụ sở :&nbsp;Số &hellip;&hellip; ng&otilde; &hellip;.., đường &hellip;.., quận &hellip;&hellip; , th&agrave;nh phố H&agrave; Nội.</p>
<p>Người đại diện :&nbsp;&hellip;&hellip;&nbsp; Chức danh: Gi&aacute;m đốc<br>M&atilde; số thuế : &hellip;&hellip;&hellip;&hellip;&hellip;&nbsp;</p>
<p>C&ugrave;ng thoả thuận k&yacute; Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y với c&aacute;c điều khoản v&agrave; điều kiện sau đ&acirc;y:</p>
<p>&nbsp;</p>
<p><strong>Điều 1. Mục ti&ecirc;u v&agrave; phạm vi hợp t&aacute;c kinh doanh</strong></p>
<p>B&ecirc;n A v&agrave; B&ecirc;n B nhất tr&iacute; c&ugrave;ng nhau hợp t&aacute;c kinh doanh trong việc khai th&aacute;c địa điểm kinh doanh Số &hellip;&hellip;, đường...., Quận/Huyện...., Tỉnh/th&agrave;nh phố.... thuộc quyền quản l&yacute; của ......................</p>
<p>Mục ti&ecirc;u của hợp đồng hợp t&aacute;c kinh doanh nhằm n&acirc;ng cao năng lực cạnh tranh tr&ecirc;n thị trường v&agrave; ph&aacute;t huy tối đa nguồn lực của mỗi b&ecirc;n nhằm x&acirc;y dựng chuỗi cửa h&agrave;ng b&aacute;n bu&ocirc;n v&agrave; b&aacute;n lẻ c&aacute;c sản phẩm n&ocirc;ng sản.</p>
<p>Phạm vi hợp t&aacute;c kinh doanh: Hai B&ecirc;n hợp t&aacute;c c&ugrave;ng điều tra, nghi&ecirc;n cứu thị trường, nhu cầu, thị hiếu kh&aacute;ch h&agrave;ng, quảng b&aacute; sản phẩm, x&acirc;y dựng thương hiệu v&agrave; t&igrave;m kiếm thị trường ti&ecirc;u thụ sản phẩm n&ocirc;ng sản;</p>
<p>&nbsp;</p>
<p><strong>Điều 2. Thời hạn hợp đồng.</strong></p>
<p>Thời hạn hợp t&aacute;c l&agrave; 05 (năm) năm bắt đầu kể từ ng&agrave;y&hellip;.th&aacute;ng&hellip;..năm 20&hellip;. đến hết ng&agrave;y&hellip;..&nbsp;th&aacute;ng &hellip;..&nbsp;năm 20... Thời hạn tr&ecirc;n c&oacute; thể được k&eacute;o d&agrave;i theo sự thoả thuận của c&aacute;c b&ecirc;n căn cứ v&agrave;o hợp đồng số&nbsp;<strong>&hellip;/20&hellip;/HĐDV</strong>&nbsp;được lập ng&agrave;y&hellip;.th&aacute;ng&nbsp;&hellip; năm 20&hellip;.</p>
<p>&nbsp;</p>
<p><strong>Điều 3. G&oacute;p vốn v&agrave; ph&acirc;n chia kết quả kinh doanh</strong></p>
<p><strong>3.1. G&oacute;p vốn</strong></p>
<p>B&ecirc;n A g&oacute;p vốn bằng to&agrave;n bộ gi&aacute; h&agrave;ng h&oacute;a, chi ph&iacute; thiết kế, in ấn c&aacute;c sản phẩm kinh doanh ph&ugrave; hợp với y&ecirc;u cầu kinh doanh tại địa điểm n&oacute;i tr&ecirc;n (Bảng danh mục h&agrave;ng h&oacute;a v&agrave; định gi&aacute; gi&aacute; trị của từng sản phẩm do B&ecirc;n A g&oacute;p sẽ được hai b&ecirc;n thống nhất lập th&agrave;nh văn bản kh&ocirc;ng t&aacute;ch rời hợp đồng n&agrave;y). Gi&aacute; trị tr&ecirc;n bao gồm to&agrave;n bộ c&aacute;c chi ph&iacute; vận chuyển, lắp đặt tại địa điểm kinh doanh.</p>
<p>B&ecirc;n B g&oacute;p vốn bằng việc trực tiếp cải tạo, thiết kế v&agrave; x&acirc;y dựng lại (nếu c&oacute;) cho to&agrave;n bộ mặt bằng phục vụ hoạt động kinh doanh tại địa điểm n&oacute;i tr&ecirc;n. (Bảng chi ph&iacute; vật tư v&agrave; nh&acirc;n c&ocirc;ng thực hiện do hai b&ecirc;n thống nhất v&agrave; lập th&agrave;nh văn bản kh&ocirc;ng t&aacute;ch rời hợp đồng n&agrave;y).</p>
<p>&nbsp;</p>
<p><strong>3.2. Ph&acirc;n chia kết quả kinh doanh</strong></p>
<p>Lợi nhuận từ hoạt động thực hiện kinh doanh ph&acirc;n phối b&aacute;n bu&ocirc;n v&agrave; b&aacute;n lẻ c&aacute;c sản phẩm n&ocirc;ng sản tại địa điểm kinh doanh n&oacute;i tr&ecirc;n sẽ được ph&acirc;n chia theo căn cứ theo tỷ lệ gi&aacute; trị phần vốn g&oacute;p của mỗi b&ecirc;n t&iacute;nh đến thời điểm ph&acirc;n chia lợi nhuận trừ trường hợp c&aacute;c b&ecirc;n c&oacute; thỏa thuận kh&aacute;c.</p>
<p>Lợi nhuận sẽ được chia theo tỷ lệ: B&ecirc;n A được hưởng .............%, B&ecirc;n B được hưởng ............% tr&ecirc;n tổng lợi nhuận sau khi đ&atilde; ho&agrave;n th&agrave;nh c&aacute;c nghĩa vụ với Nh&agrave; nước.</p>
<p>Thời điểm chia lợi nhuận được x&aacute;c định v&agrave;o ng&agrave;y cuối c&ugrave;ng của mỗi Qu&yacute; của năm t&agrave;i ch&iacute;nh. Năm t&agrave;i ch&iacute;nh được t&iacute;nh bắt đầu kể từ ng&agrave;y: &hellip;../&hellip;../20...</p>
<p>&nbsp;</p>
<p><strong>3.3 Chi ph&iacute; cho hoạt động quản l&yacute; kinh doanh bao gồm:</strong></p>
<p>+ Tiền nhập h&agrave;ng h&oacute;a đầu v&agrave;o (Được x&aacute;c định dựa tr&ecirc;n hợp đồng đ&atilde; k&yacute; v&agrave; bi&ecirc;n bản giao h&agrave;ng của nh&agrave; ph&acirc;n phối đến địa điểm kinh doanh);</p>
<p>+ Lương nh&acirc;n vi&ecirc;n;</p>
<p>+ Chi ph&iacute; điện, nước;</p>
<p>+ Khấu hao t&agrave;i sản;</p>
<p>+ Chi ph&iacute; bảo dưỡng m&aacute;y m&oacute;c, thiết bị, nh&agrave; xưởng;</p>
<p>+ Chi ph&iacute; kh&aacute;c do hai b&ecirc;n thỏa thuận bằng bi&ecirc;n bản.</p>
<p>&nbsp;</p>
<p><strong>Điều 4. C&aacute;c nguy&ecirc;n tắc t&agrave;i ch&iacute;nh</strong></p>
<p>Hai b&ecirc;n phải tu&acirc;n thủ c&aacute;c nguy&ecirc;n tắc t&agrave;i ch&iacute;nh kế to&aacute;n theo qui định của ph&aacute;p luật về kế to&aacute;n của nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam được hoạch to&aacute;n, quyết to&aacute;n v&agrave;o B&ecirc;n A.</p>
<p>Mọi khoản thu chi cho hoạt động kinh doanh đều phải được ghi ch&eacute;p r&otilde; r&agrave;ng, đầy đủ, x&aacute;c thực.</p>
<p>&nbsp;</p>
<p><strong>Điều 5. Ban điều h&agrave;nh hoạt động kinh doanh</strong></p>
<p>Hai b&ecirc;n sẽ th&agrave;nh lập một Ban điều h&agrave;nh hoạt động kinh doanh gồm 03 người trong đ&oacute; B&ecirc;n A sẽ cử 02 (hai), B&ecirc;n B sẽ cử 01 (một) đại diện khi cần phải đưa ra c&aacute;c quyết định li&ecirc;n quan đến nội dung hợp t&aacute;c được quy định tại Hợp đồng n&agrave;y. Mọi quyết định của Ban điều h&agrave;nh sẽ được th&ocirc;ng qua khi c&oacute; &iacute;t nhất hai th&agrave;nh vi&ecirc;n đồng &yacute;.</p>
<p>Đại diện của B&ecirc;n A l&agrave;: &Ocirc;ng&nbsp;<strong>&hellip;&hellip;</strong><strong>&hellip;.</strong>.&nbsp;&ndash; Chức danh: Gi&aacute;m đốc</p>
<p>&Ocirc;ng&nbsp;<strong>&hellip;</strong><strong>&hellip;..</strong><strong>&hellip;</strong>&nbsp;&ndash; Chức danh:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
<p>Đại diện của B&ecirc;n B l&agrave;: &Ocirc;ng&nbsp;<strong>&hellip;</strong><strong>&hellip;..</strong><strong>&hellip;</strong>&nbsp;- Chức danh:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>
<p><strong>Người trực tiếp xử l&yacute; c&aacute;c vấn đề ph&aacute;t sinh trong qu&aacute; tr&igrave;nh hoạt động:</strong></p>
<p>Đại diện B&ecirc;n A: &Ocirc;ng:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
<p>Điện thoại: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; Fax:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
<p>Đại diện B&ecirc;n B: &Ocirc;ng:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</p>
<p>Điện thoại: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; Fax:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</p>
<p>Trụ sở của ban điều h&agrave;nh đặt tại: Số........, đường ..........., Quận/Huyện ............., Tỉnh/th&agrave;nh phố............</p>
<p>Nhiệm vụ của ban điều h&agrave;nh:</p>
<p>- X&acirc;y dựng kế hoạch v&agrave; chương tr&igrave;nh triển khai cụ thể h&agrave;ng th&aacute;ng/qu&yacute; v&agrave; định hướng kế hoạch ph&aacute;t triển h&agrave;ng v&agrave; th&ocirc;ng qua L&atilde;nh đạo Hai B&ecirc;n.</p>
<p>- Phối hợp với c&aacute;c đơn vị kh&aacute;c thực hiện c&aacute;c kế hoạch v&agrave; chương tr&igrave;nh đ&atilde; đề ra.</p>
<p>- Thường xuy&ecirc;n b&aacute;o c&aacute;o v&agrave; xin &yacute; kiến chỉ đạo của L&atilde;nh đạo Hai B&ecirc;n.</p>
<p>- Lập c&aacute;c b&aacute;o c&aacute;o định kỳ về c&aacute;c c&ocirc;ng việc đang thực hiện, t&igrave;nh h&igrave;nh sử dụng kinh ph&iacute; cho c&aacute;c hoạt động đ&oacute;.</p>
<p>- Trực tiếp quản l&yacute; v&agrave; điều h&agrave;nh mọi hoạt động của nh&acirc;n vi&ecirc;n trong trung t&acirc;m ph&acirc;n phối.</p>
<p>&nbsp;</p>
<p><strong>Điều 6. Quyền v&agrave; nghĩa vụ của B&ecirc;n A</strong></p>
<p>- Chịu tr&aacute;ch nhiệm nhập nguồn h&agrave;ng từ c&aacute;c nh&agrave; ph&acirc;n phối;</p>
<p>- T&igrave;m kiếm, đ&agrave;m ph&aacute;n, k&yacute; kết, thanh to&aacute;n hợp đồng đối với c&aacute;c nh&agrave; ph&acirc;n phối cung cấp sản phẩm n&ocirc;ng sản trong v&agrave; ngo&agrave;i nước.</p>
<p>- Cung cấp đầy đủ c&aacute;c ho&aacute; đơn, chứng từ li&ecirc;n quan để phục vụ cho c&ocirc;ng t&aacute;c hạch to&aacute;n t&agrave;i ch&iacute;nh qu&aacute; tr&igrave;nh kinh doanh.</p>
<p>- Quản l&yacute; v&agrave; điều h&agrave;nh hoạt động b&aacute;n lẻ tại địa điểm kinh doanh;</p>
<p>- Trực tiếp chịu tr&aacute;ch nhiệm tuyển dụng, quản l&yacute;, điều động c&aacute;n bộ, nh&acirc;n vi&ecirc;n tại địa điểm kinh doanh. L&ecirc;n kế hoạch Trả lương v&agrave; c&aacute;c chế độ kh&aacute;c cho c&ocirc;ng nh&acirc;n, c&aacute;n bộ l&agrave;m việc tại địa điểm kinh doanh.</p>
<p>- Phối hợp c&ugrave;ng B&ecirc;n B trong việc x&acirc;y dựng kế hoạch kinh doanh chi tiết theo th&aacute;ng/Qu&yacute;/Năm t&agrave;i ch&iacute;nh.</p>
<p>- Được hưởng .......................% lợi nhuận sau thuế.</p>
<p>- Triệt để tu&acirc;n thủ c&aacute;c quy định của ph&aacute;p luật về bảo vệ m&ocirc;i trường v&agrave; c&aacute;c quy định kh&aacute;c của ph&aacute;p luật trong qu&aacute; tr&igrave;nh kinh doanh;</p>
<p>&nbsp;</p>
<p><strong>Điều 7. Quyền v&agrave; nghĩa vụ của b&ecirc;n B</strong></p>
<p>- Thực hiện quyền b&aacute;n bu&ocirc;n c&aacute;c sản phẩm n&ocirc;ng sản;</p>
<p>- T&igrave;m kiếm đối tượng kh&aacute;ch h&agrave;ng ph&ugrave; hợp phục vụ cho hoạt động b&aacute;n bu&ocirc;n n&ocirc;ng sản;</p>
<p>- Trực tiếp quản l&yacute; hoạt động kinh doanh v&agrave; x&acirc;y dựng kế hoạch kinh doanh cho nh&oacute;m kh&aacute;ch h&agrave;ng n&agrave;y;</p>
<p>- C&oacute; tr&aacute;ch nhiệm phối hợp c&ugrave;ng B&ecirc;n A trong hoạt động quản l&yacute;, điều h&agrave;nh qu&aacute; tr&igrave;nh kinh doanh;</p>
<p>- C&oacute; tr&aacute;ch nhiệm triển khai t&igrave;m kiếm v&agrave; b&aacute;n lẻ c&aacute;c sản n&ocirc;ng sản b&aacute;n sản phẩm trong điều kiện cho ph&eacute;p;</p>
<p>- Đưa m&aacute;y m&oacute;c, thiết bị phục vụ hoạt động kinh doanh v&agrave;o hoạt động của trung t&acirc;m;</p>
<p>- Nguồn h&agrave;ng b&aacute;n bu&ocirc;n phải nhập từ nguồn ph&acirc;n phối của trung t&acirc;m v&agrave; hạch to&aacute;n trực tiếp v&agrave;o trung t&acirc;m ph&acirc;n phối;</p>
<p>- Triệt để tu&acirc;n thủ c&aacute;c quy định của ph&aacute;p luật về bảo vệ m&ocirc;i trường v&agrave; c&aacute;c quy định kh&aacute;c của ph&aacute;p luật trong qu&aacute; tr&igrave;nh kinh doanh;</p>
<p>- Hạch to&aacute;n to&agrave;n bộ thu chi của qu&aacute; tr&igrave;nh sản xuất kinh doanh theo đ&uacute;ng c&aacute;c quy định t&agrave;i ch&iacute;nh của trung t&acirc;m ph&acirc;n phối tại địa điểm kinh doanh;</p>
<p>- C&oacute; tr&aacute;ch nhiệm k&ecirc; khai, nộp đầy đủ thuế v&agrave; c&aacute;c nghĩa vụ kh&aacute;c với Nh&agrave; nước. Đồng thời quan hệ với cơ quan quản l&yacute; nh&agrave; nước ng&agrave;nh v&agrave; địa phương, cơ quan thuế nơi đặt địa điểm kinh doanh.</p>
<p>- Phối hợp c&ugrave;ng B&ecirc;n A x&acirc;y dựng kế hoạch kinh doanh chi tiết theo th&aacute;ng/Qu&yacute;/Năm t&agrave;i ch&iacute;nh.</p>
<p>- Được hưởng ............................% lợi nhuận sau thuế.</p>
<p>&nbsp;</p>
<p><strong>Điều 8: Hiệu lực hợp đồng hợp t&aacute;c v&agrave; việc sửa đổi v&agrave; chấm dứt thoả thuận hợp t&aacute;c</strong></p>
<p>- Hợp đồng n&agrave;y c&oacute; hiệu lực kể từ ng&agrave;y hai b&ecirc;n k&yacute; kết;.</p>
<p>- Trong trường hợp một trong Hai B&ecirc;n c&oacute; mong muốn sửa đổi, bổ sung phải th&ocirc;ng b&aacute;o trước cho b&ecirc;n kia bằng văn bản.</p>
<p>- Trong trường hợp một trong Hai B&ecirc;n đơn phương chấm dứt hợp đồng, b&ecirc;n đơn phương c&oacute; nghĩa vụ th&ocirc;ng b&aacute;o trước bằng văn bản cho B&ecirc;n kia trong v&ograve;ng 06 th&aacute;ng trước khi chấm dứt hợp đồng. Quyền v&agrave; nghĩa vụ của c&aacute;c b&ecirc;n tại thời điểm k&iacute; kết sẽ được hai b&ecirc;n trực tiếp thương thảo v&agrave; x&aacute;c lập bằng văn bản.</p>
<p>- Hai B&ecirc;n cam kết thực hiện theo c&aacute;c điều kiện v&agrave; điều khoản tr&ecirc;n v&agrave; nhất tr&iacute; k&yacute; kết thoả thuận hợp t&aacute;c n&agrave;y.</p>
<p>&nbsp;</p>
<p><strong>Điều 9. Điều khoản chung</strong></p>
<p>Hợp đồng n&agrave;y được hiểu v&agrave; chịu sự điều chỉnh của Ph&aacute;p luật nước Cộng ho&agrave; x&atilde; hội chủ nghĩa Việt Nam.</p>
<p>Hai b&ecirc;n cam kết thực hiện tất cả những điều khoản đ&atilde; cam kết trong hợp đồng. B&ecirc;n n&agrave;o vi phạm hợp đồng g&acirc;y thiệt hại cho b&ecirc;n kia (trừ trong trường hợp bất khả kh&aacute;ng) th&igrave; phải bồi thường to&agrave;n bộ thiệt hại xảy ra v&agrave; chịu phạt vi phạm hợp đồng bằng 08% gi&aacute; trị phần nghĩa vụ vi phạm theo hợp đồng.</p>
<p>Trong qu&aacute; tr&igrave;nh thực hiện hợp đồng nếu b&ecirc;n n&agrave;o c&oacute; kh&oacute; khăn trở ngại th&igrave; phải b&aacute;o cho b&ecirc;n kia trong v&ograve;ng 1 (một) th&aacute;ng kể từ ng&agrave;y c&oacute; kh&oacute; khăn trở ngại.</p>
<p>C&aacute;c b&ecirc;n c&oacute; tr&aacute;ch nhiệm th&ocirc;ng tin kịp thời cho nhau tiến độ thực hiện c&ocirc;ng việc. Đảm bảo b&iacute; mật mọi th&ocirc;ng tin li&ecirc;n quan tới qu&aacute; tr&igrave;nh sản xuất kinh doanh.</p>
<p>Mọi sửa đổi, bổ sung hợp đồng n&agrave;y đều phải được l&agrave;m bằng văn bản v&agrave; c&oacute; chữ k&yacute; của hai b&ecirc;n. C&aacute;c phụ lục l&agrave; phần kh&ocirc;ng t&aacute;ch rời của hợp đồng.</p>
<p>Mọi tranh chấp ph&aacute;t sinh trong qu&aacute; tr&igrave;nh thực hiện hợp đồng được giải quyết trước hết qua thương lượng, ho&agrave; giải, nếu ho&agrave; giải kh&ocirc;ng th&agrave;nh việc tranh chấp sẽ được giải quyết tại To&agrave; &aacute;n c&oacute; thẩm quyền.</p>
<p>&nbsp;</p>
<p><strong>Điều 10. Hiệu lực hợp đồng</strong></p>
<p>Hợp đồng hết hiệu lực khi hết thời hạn hợp đồng theo quy định tại Điều 2 Hợp đồng n&agrave;y hoặc c&aacute;c trường hợp kh&aacute;c theo qui định của ph&aacute;p luật.</p>
<p>Khi kết th&uacute;c Hợp đồng, hai b&ecirc;n sẽ l&agrave;m bi&ecirc;n bản thanh l&yacute; hợp đồng. To&agrave;n bộ t&agrave;i sản sản sẽ được kiểm k&ecirc; v&agrave; ph&acirc;n chia theo tỷ lệ vốn g&oacute;p tại thời điểm hợp đồng hết hiệu lực.</p>
<p>Hợp đồng n&agrave;y gồm 04 (bốn) trang kh&ocirc;ng thể t&aacute;ch rời nhau, được lập th&agrave;nh 02 (hai) bản bằng tiếng Việt, mỗi B&ecirc;n giữ 01 (một) bản c&oacute; gi&aacute; trị ph&aacute;p l&yacute; như nhau v&agrave; c&oacute; hiệu lực kể từ ng&agrave;y k&yacute;.</p>
<table cellspacing="1">
<tbody>
<tr>
<td>
<p><strong>Đại diện b&ecirc;n A</strong></p>
</td>
<td>
<p><strong>Đại diện b&ecirc;n B</strong></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>`;
export const TEMPLATE_FOUR = `<p><strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></p>
<p>Độc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</p>
<p>---</p>
<p>&nbsp;</p>
<p><strong>HỢP ĐỒNG HỢP T&Aacute;C KINH DOANH</strong></p>
<p><em>Số:...</em><em>&nbsp;/HĐHTK</em>D</p>
<p>&nbsp;</p>
<p>H&ocirc;m nay, ng&agrave;y&hellip; th&aacute;ng&hellip; năm&hellip;</p>
<p>Tại (địa điểm k&yacute; kết): &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..<br><br>Ch&uacute;ng t&ocirc;i gồm c&oacute;: &hellip;&hellip;&hellip;.<br><br><strong>B&ecirc;n A:</strong></p>
<p>- T&ecirc;n c&aacute; nh&acirc;n (hoặc doanh nghiệp):</p>
<p>- Địa chỉ:</p>
<p>- Điện thoại:</p>
<p>- T&agrave;i khoản số: &hellip;. Mở tại ng&acirc;n h&agrave;ng:</p>
<p>- Đại diện l&agrave; &Ocirc;ng (B&agrave;): &hellip; Chức vụ: &hellip;.</p>
<p>- Giấy ủy quyền số: &hellip;..&nbsp;(nếu c&oacute;).</p>
<p>Viết ng&agrave;y&hellip; th&aacute;ng&hellip; năm&hellip;. Do chức vụ: &hellip; k&yacute; (nếu c&oacute;).<br><br><strong>B&ecirc;n B:</strong></p>
<p>- T&ecirc;n c&aacute; nh&acirc;n (hoặc doanh nghiệp):</p>
<p>- Địa chỉ:</p>
<p>- Điện thoại:</p>
<p>- T&agrave;i khoản số: Mở tại ng&acirc;n h&agrave;ng:</p>
<p>- Đại diện l&agrave; &Ocirc;ng (B&agrave;): Chức vụ:</p>
<p>- Giấy ủy quyền số: (nếu c&oacute;).</p>
<p>Viết ng&agrave;y th&aacute;ng năm Do chức vụ: k&yacute; (nếu c&oacute;).</p>
<p>&nbsp;</p>
<p><br><strong>B&ecirc;n C:</strong></p>
<p>- T&ecirc;n c&aacute; nh&acirc;n (hoặc doanh nghiệp):</p>
<p>- Địa chỉ:</p>
<p>- Điện thoại:</p>
<p>- T&agrave;i khoản số: Mở tại ng&acirc;n h&agrave;ng:</p>
<p>- Đại diện l&agrave; &Ocirc;ng (B&agrave;): Chức vụ:</p>
<p>- Giấy ủy quyền số: (nếu c&oacute;).</p>
<p>Viết ng&agrave;y th&aacute;ng năm Do chức vụ: k&yacute; (nếu c&oacute;).<br><br><strong>B&ecirc;n D:</strong></p>
<p>- T&ecirc;n c&aacute; nh&acirc;n (hoặc doanh nghiệp):</p>
<p>- Địa chỉ:</p>
<p>- Điện thoại:</p>
<p>- T&agrave;i khoản số: Mở tại ng&acirc;n h&agrave;ng:</p>
<p>- Đại diện l&agrave; &Ocirc;ng (B&agrave;): Chức vụ:</p>
<p>- Giấy ủy quyền số: (nếu c&oacute;).</p>
<p>Viết ng&agrave;y th&aacute;ng năm Do chức vụ: k&yacute; (nếu c&oacute;).<br><br>C&aacute;c b&ecirc;n thống nhất thỏa thuận nội dung hợp đồng như sau:<br><br><strong>Điều 1: Nội dung c&aacute;c hoạt động kinh doanh</strong></p>
<p>(C&oacute; thể hợp t&aacute;c trong sản xuất h&agrave;ng ho&aacute;, x&acirc;y dựng một c&ocirc;ng tr&igrave;nh thu mua chế biến một hoặc một số loại sản phẩm, tiến h&agrave;nh một hoạt động dịch vụ v.v&hellip;).<br><br><strong>Điều 2:&nbsp;</strong>Danh mục, số lượng, chất lượng thiết bị, vật tư chủ yếu cần cho hoạt động kinh doanh v&agrave; nguồn cung cấp thiết bị vật tư.</p>
<p>(C&oacute; thể lập bảng chiết t&iacute;nh theo c&aacute;c mục tr&ecirc;n)<br><br><strong>Điều 3:&nbsp;</strong>Quy c&aacute;ch, số lượng, chất lượng sản phẩm v&agrave; thị trường ti&ecirc;u thụ</p>
<p>1. Quy c&aacute;ch sản phẩm</p>
<p>- H&igrave;nh d&aacute;ng k&iacute;ch thước</p>
<p>- M&agrave;u sắc</p>
<p>- Bao b&igrave;</p>
<p>- K&yacute; m&atilde; hiệu</p>
<p>-</p>
<p>-</p>
<p>2. Số lượng sản phẩm</p>
<p>- Số lượng sản phẩm trong năm sẽ sản xuất l&agrave;</p>
<p>- Trong c&aacute;c qu&yacute;</p>
<p>- Trong từng th&aacute;ng của qu&yacute;</p>
<p>3. Chất lượng sản phẩm</p>
<p>Sản phẩm phải đạt ti&ecirc;u chuẩn chất lượng như sau</p>
<p>(Dựa theo ti&ecirc;u chuẩn, theo mẫu, theo h&agrave;m lượng chất chủ yếu, theo t&agrave;i liệu kỹ thuật v.v&hellip;).</p>
<p>4. Thị trường ti&ecirc;u thụ</p>
<p>a/ C&aacute;c thị trường phải cung ứng theo chỉ ti&ecirc;u ph&aacute;p lệnh:</p>
<p>- Địa chỉ Dự kiến số lượng</p>
<p>-</p>
<p>-<br><br>b/ C&aacute;c thị trường kh&aacute;c đ&atilde; c&oacute; đơn đặt h&agrave;ng</p>
<p>- Địa chỉ Dự kiến số lượng</p>
<p>-</p>
<p>-</p>
<p>c/ C&aacute;c thị trường c&oacute; thể b&aacute;n lẻ</p>
<p>- Địa chỉ Dự kiến số lượng<br><br><strong>Điều 4:</strong>&nbsp;Nghĩa vụ v&agrave; quyền lợi của c&aacute;c b&ecirc;n hợp doanh</p>
<p>1. B&ecirc;n A</p>
<p>a/ C&oacute; c&aacute;c nghĩa vụ sau: (Theo tr&aacute;ch nhiệm đ&atilde; ph&acirc;n c&ocirc;ng)</p>
<p>b/ C&aacute;c quyền lợi:</p>
<p>2. B&ecirc;n B: (Ghi r&otilde; quyền v&agrave; nghĩa vụ theo thỏa thuận).</p>
<p>3. B&ecirc;n C:</p>
<p>v.v&hellip;<br><br><strong>Điều 5:</strong>&nbsp;Phương thức x&aacute;c định kết quả kinh doanh v&agrave; ph&acirc;n chia kết quả kinh doanh</p>
<p>1. Phương thức x&aacute;c định kết quả kinh doanh</p>
<p>a. Dựa v&agrave;o lợi nhuận do b&aacute;n sản phẩm (hoặc c&aacute;c c&ocirc;ng tr&igrave;nh ho&agrave;n th&agrave;nh được b&ecirc;n chủ đầu tư thanh to&aacute;n) .</p>
<p>b. dựa v&agrave;o c&aacute;c nguồn thu nhập kh&aacute;c (nếu c&oacute;)</p>
<p>(Thu nhập n&agrave;y c&oacute; thể l&agrave; l&atilde;i, c&oacute; thể l&agrave; lỗ)</p>
<p>2. Phương thức ph&acirc;n chia kết quả kinh doanh</p>
<p>a. C&aacute;c b&ecirc;n được chia lợi nhuận hoặc lỗ v&agrave; rủi ro theo tỷ lệ tương ứng với phần tr&aacute;ch nhiệm trong hợp doanh.</p>
<p>b. Tỷ lệ ph&acirc;n chia cụ thể được thỏa thuận tr&ecirc;n cơ sở phần c&ocirc;ng việc được giao như sau:</p>
<p>- B&ecirc;n A l&agrave; % kết quả</p>
<p>- B&ecirc;n B l&agrave; %</p>
<p>- B&ecirc;n C l&agrave; %</p>
<p>- v.v&hellip;<br><br><strong>Điều 6:&nbsp;</strong>Tr&aacute;ch nhiệm của c&aacute;c b&ecirc;n do&nbsp;<u>vi phạm hợp đồng</u></p>
<p>1. B&ecirc;n n&agrave;o đ&atilde; k&yacute; hợp đồng m&agrave; kh&ocirc;ng thực hiện hoặc đơn phương đ&igrave;nh chỉ hợp đồng kh&ocirc;ng c&oacute; l&yacute; do ch&iacute;nh đ&aacute;ng th&igrave; sẽ bị phạt % tổng trị gi&aacute; vốn m&agrave; b&ecirc;n đ&oacute; c&oacute; tr&aacute;ch nhiệm đ&oacute;ng (c&oacute; thể x&aacute;c định một khoản tiền cụ thể).</p>
<p>2. Ngo&agrave;i tiền phạt vi phạm hợp đồng, b&ecirc;n vi phạm c&ograve;n phải bồi thường những mất m&aacute;t hư hỏng t&agrave;i sản, phải trả những chi ph&iacute; để ngăn chặn, hạn chế thiệt hại do vi phạm hợp đồng g&acirc;y ra, c&aacute;c khoản tiền phạt do vi phạm hợp đồng kh&aacute;c v&agrave; tiền bồi thường thiệt hại m&agrave; c&aacute;c b&ecirc;n bị vi phạm đ&atilde; phải trả cho b&ecirc;n thứ ba (ngo&agrave;i hợp doanh) l&agrave; hậu quả trực tiếp của vi phạm n&agrave;y g&acirc;y ra.</p>
<p>3. C&aacute;c b&ecirc;n vi phạm nghĩa vụ tr&aacute;ch nhiệm đ&atilde; quy định trong Điều 4 sẽ bị buộc phải thực hiện đầy đủ những quy định đ&oacute;, nếu cố t&igrave;nh kh&ocirc;ng thực hiện sẽ bị khấu trừ v&agrave;o lợi nhuận, nếu nghi&ecirc;m trọng c&oacute; thể bị khấu trừ cả v&agrave;o vốn g&oacute;p (T&ugrave;y theo t&iacute;nh chất mức độ vi phạm cụ thể m&agrave; c&aacute;c b&ecirc;n sẽ họp quyết định mức phạt cụ thể v&agrave;o bi&ecirc;n bản).<br><br><strong>Điều 7:&nbsp;</strong>Thủ tục giải quyết c&aacute;c tranh chấp giữa c&aacute;c b&ecirc;n ph&aacute;t sinh từ việc thực hiện hợp đồng</p>
<p>1. Hai b&ecirc;n cần chủ động th&ocirc;ng b&aacute;o cho nhau biết tiến độ thực hiện hợp đồng, nếu c&oacute; vấn đề g&igrave; bất lợi ph&aacute;t sinh, c&aacute;c b&ecirc;n phải kịp thời b&aacute;o cho nhau biết v&agrave; chủ động b&agrave;n bạc giải quyết tr&ecirc;n cơ sở thương lượng đảm bảo hai b&ecirc;n c&ugrave;ng c&oacute; lợi (c&oacute; lập bi&ecirc;n bản ghi to&agrave;n bộ nội dung đ&oacute;).</p>
<p>2. Trường hợp c&oacute; nội dung tranh chấp kh&ocirc;ng tự giải quyết được th&igrave; hai b&ecirc;n thống nhất sẽ khiếu nại tới T&ograve;a &aacute;n l&agrave; cơ quan c&oacute; đủ thẩm quyền giải quyết.3. C&aacute;c chi ph&iacute; về kiểm tra, x&aacute;c minh v&agrave; lệ ph&iacute; T&ograve;a &aacute;n do b&ecirc;n c&oacute; lỗi chịu.<br><br><strong>Điều 8:&nbsp;</strong>Trường hợp cần sửa đổi hoặc chấm dứt hợp đồng trước thời hạn</p>
<p>1. Trường hợp cần sử đổi hợp đồng</p>
<p>- Khi quyền lợi của một b&ecirc;n n&agrave;o đ&oacute; bị thiệt th&ograve;i do ph&acirc;n chia lợi nhuận kh&ocirc;ng ch&iacute;nh x&aacute;c v&agrave; c&ocirc;ng bằng, cần x&aacute;c định lại phương thức ph&acirc;n chia kết quả.</p>
<p>- Ph&acirc;n c&ocirc;ng nghĩa vụ tr&aacute;ch nhiệm chưa s&aacute;t hợp với khả năng thực tế của một trong c&aacute;c b&ecirc;n.</p>
<p>- Khi cần thay đổi quy c&aacute;ch, chất lượng cho ph&ugrave; hợp với nhu cầu người ti&ecirc;u d&ugrave;ng, hoặc thay đổi mẫu m&atilde; h&agrave;ng h&oacute;a, thay đổi mặt h&agrave;ng kinh doanh, v.v&hellip;</p>
<p>- Khi cần thay đổi số lượng vốn g&oacute;p của một trong c&aacute;c b&ecirc;n.</p>
<p>2. Trường hợp cần chấm dứt hợp đồng trước thời hạn</p>
<p>- Khi cơ quan Nh&agrave; nước c&oacute; thẩm quyền ra quyết định đ&igrave;nh chỉ c&aacute;c hoạt động ghi trong hợp đồng n&agrave;y (do hoạt động trong hợp đồng vi phạm ph&aacute;p luật).</p>
<p>- Khi gặp rủi ro (ch&aacute;y, nổ, lụt&hellip;) l&agrave;m cho một hoặc nhiều b&ecirc;n mất khả năng hoat động.</p>
<p>- Khi l&agrave;m ăn thua lỗ trong th&aacute;ng li&ecirc;n tiếp dẫn tới vỡ nợ hoặc mất khả năng thanh to&aacute;n.</p>
<p>-</p>
<p>-</p>
<p>3. C&aacute;c b&ecirc;n phải tổ chức họp v&agrave; lập bi&ecirc;n bản thanh l&yacute; hợp đồng trước thời hạn, x&aacute;c định tr&aacute;ch nhiệm tiếp theo của c&aacute;c b&ecirc;n sau khi chấm dứt hợp đồng v&agrave;o bi&ecirc;n bản v&agrave; phải thực hiện triệt để phần tr&aacute;ch nhiệm của m&igrave;nh.<br><br><strong>Điều 9:</strong>&nbsp;C&aacute;c thỏa thuận kh&aacute;c (nếu cần)<br><br><strong>Điều 10:&nbsp;</strong>Thời hạn c&oacute; hiệu lực của hợp đồng</p>
<p>Hợp đồng n&agrave;y c&oacute; hiệu lực từ ng&agrave;y &hellip; Đến ng&agrave;y &hellip;</p>
<p>C&aacute;c b&ecirc;n sẽ tổ chức họp v&agrave; lập bi&ecirc;n bản thanh l&yacute; hợp đồng n&agrave;y sau khi hết hiệu lực kh&ocirc;ng qu&aacute; 10 ng&agrave;y. B&ecirc;n c&oacute; tr&aacute;ch nhiệm tổ chức v&agrave; chuẩn bị thời gian, địa điểm họp. Nh&agrave; xưởng, nh&agrave; kho, m&aacute;y m&oacute;c, d&acirc;y chuyền thiết bị &hellip;.sẽ&nbsp;được trả lại cho B&ecirc;n &hellip;</p>
<p>Hợp đồng n&agrave;y được l&agrave;m th&agrave;nh &hellip;. bản (c&oacute; hay kh&ocirc;ng c&oacute; c&aacute;c trang t&aacute;ch rời nhau), c&oacute; gi&aacute; trị như nhau, mỗi b&ecirc;n giữ&hellip;. bản.</p>
<table cellspacing="1">
<tbody>
<tr>
<td>
<p><strong>ĐẠI DIỆN B&Ecirc;N A</strong></p>
</td>
<td>
<p><strong>ĐẠI DIỆN B&Ecirc;N B</strong></p>
</td>
</tr>
<tr>
<td>
<p>Chức vụ</p>
<p>(K&yacute; t&ecirc;n v&agrave; đ&oacute;ng dấu)</p>
</td>
<td>
<p>Chức vụ</p>
<p><br>(K&yacute; t&ecirc;n v&agrave; đ&oacute;ng dấu)</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table cellspacing="1">
<tbody>
<tr>
<td>
<p><strong>ĐẠI DIỆN B&Ecirc;N C</strong></p>
</td>
<td>
<p><strong>ĐẠI DIỆN B&Ecirc;N D</strong></p>
</td>
</tr>
<tr>
<td>
<p>Chức vụ</p>
<p>(K&yacute; t&ecirc;n v&agrave; đ&oacute;ng dấu)</p>
</td>
<td>
<p>Chức vụ</p>
<p><br>(K&yacute; t&ecirc;n v&agrave; đ&oacute;ng dấu)</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>`;
export const TEMPLATE_FIRE = `<p><strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong></p>
<p><strong>Độc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</strong></p>
<p><strong>-------o0o-------</strong><br><br></p>
<p><strong>HỢP ĐỒNG HỢP T&Aacute;C KINH DOANH</strong></p>
<p><br>Căn cứ Luật Đầu tư nước ngo&agrave;i tại Việt Nam v&agrave; c&aacute;c văn bản ph&aacute;p l&yacute; kh&aacute;c c&oacute; li&ecirc;n quan, c&aacute;c B&ecirc;n dưới đ&acirc;y mong muốn được tiến h&agrave;nh hoạt động đầu tư tại Cộng ho&agrave; X&atilde; hội chủ nghĩa Việt Nam theo h&igrave;nh thức Hợp đồng hợp t&aacute;c kinh doanh với nội dung v&agrave; phạm vi được quy định trong Hợp đồng n&agrave;y như sau:</p>
<p><strong>A. B&ecirc;n (c&aacute;c B&ecirc;n) Việt Nam:</strong></p>
<p>1. T&ecirc;n c&ocirc;ng ty: ........................................................................</p>
<p>2. Đại diện được uỷ quyền: .....................................................</p>
<p>Chức vụ: ..................................................................................</p>
<p>3. Trụ sở ch&iacute;nh: ........................................................................</p>
<p>Điện thoại: .............................................. Fax: .........................</p>
<p>4. Ng&agrave;nh nghề kinh doanh ch&iacute;nh: ................................................</p>
<p>5. Giấy ph&eacute;p th&agrave;nh lập c&ocirc;ng ty: ...................................................</p>
<p>Đăng k&yacute; tại: .................................. Ng&agrave;y: ..................................</p>
<p>&nbsp;</p>
<p><strong>B. B&ecirc;n (c&aacute;c B&ecirc;n) nước ngo&agrave;i:</strong></p>
<p>1. T&ecirc;n c&ocirc;ng ty hoặc c&aacute; nh&acirc;n: ......................................................</p>
<p>2. Đại diện được uỷ quyền: ..........................................................</p>
<p>Chức vụ: ....................................................................................</p>
<p>Quốc tịch: ...................................................................................</p>
<p>Địa chỉ thường tr&uacute;: ......................................................................</p>
<p>3. Trụ sở ch&iacute;nh: ...........................................................................</p>
<p>Điện thoại: ....................... ....................... Fax: ............................</p>
<p>4. Ng&agrave;nh nghề kinh doanh ch&iacute;nh: ...................................................</p>
<p>5. Giấy ph&eacute;p th&agrave;nh lập c&ocirc;ng ty: ......................................................</p>
<p>Đăng k&yacute; tại: .................................. Ng&agrave;y: .....................................</p>
<p>Ghi ch&uacute;: Nếu một hoặc cả hai B&ecirc;n hợp doanh gồm nhiều th&agrave;nh vi&ecirc;n, th&igrave; từng th&agrave;nh vi&ecirc;n sẽ m&ocirc; tả đầy đủ theo c&aacute;c chi tiết n&ecirc;u tr&ecirc;n; Mỗi B&ecirc;n cần chỉ định đại diện được uỷ quyền của m&igrave;nh.</p>
<p>C&ugrave;ng thoả thuận k&yacute; Hợp đồng hợp t&aacute;c kinh n&agrave;y với c&aacute;c điều khoản v&agrave; điều kiện sau đ&acirc;y:</p>
<p>&nbsp;</p>
<p><strong>Điều 1:</strong></p>
<p>Mục ti&ecirc;u hợp t&aacute;c kinh doanh tr&ecirc;n cơ sở Hợp đồng ..............................</p>
<p>(m&ocirc; tả chi tiết nội dung v&agrave; phạm vi hợp t&aacute;c kinh doanh)</p>
<p>&nbsp;</p>
<p><strong>Điều 2:</strong></p>
<p>1. Địa điểm thực hiện:</p>
<p>2. Năng lực sản xuất: H&agrave;ng ho&aacute;/dịch vụ v&agrave;o năm sản xuất ổn định</p>
<p>(Chia th&agrave;nh sản phẩm ch&iacute;nh v&agrave; phụ - nếu cần thiết)</p>
<p>3. Sản phẩm của Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y sẽ được ti&ecirc;u thụ:</p>
<p>- Tại thị trường Việt Nam: ......% sản phẩm</p>
<p>- Tại thị trường nước ngo&agrave;i: ......% sản phẩm</p>
<p>4. Văn ph&ograve;ng điều h&agrave;nh của B&ecirc;n nước ngo&agrave;i (nếu c&oacute;) .......&nbsp;</p>
<p>Địa chỉ:..................................................................................&nbsp;</p>
<p>Điện thoại: ....................... Fax: ..........................................&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Điều 3:</strong></p>
<p>Tr&aacute;ch nhiệm của c&aacute;c B&ecirc;n hợp doanh trong việc g&oacute;p vốn để thực hiện Hợp đồng:</p>
<p>a) B&ecirc;n (c&aacute;c B&ecirc;n) Việt Nam: g&oacute;p ............. bằng ..............(quyền sử dụng đất, m&aacute;y m&oacute;c thiết bị, nguy&ecirc;n vật liệu, bộ phận rời, linh kiện, tiền mặt, c&aacute;c chi ph&iacute; kh&aacute;c ....)</p>
<p>b) B&ecirc;n (c&aacute;c B&ecirc;n) nước ngo&agrave;i: g&oacute;p .............. bằng ............(m&aacute;y m&oacute;c thiết bị, nguy&ecirc;n vật liệu, bộ phận rời, linh kiện, tiền nước ngo&agrave;i, c&aacute;c chi ph&iacute; kh&aacute;c ....).</p>
<p>Nếu v&agrave;o thời điểm đ&oacute;ng g&oacute;p thực tế, những gi&aacute; trị tr&ecirc;n đ&acirc;y thay đổi so với gi&aacute; trị hiện tại th&igrave; c&aacute;c B&ecirc;n phải thoả thuận về những sửa đổi đ&oacute; v&agrave; b&aacute;o c&aacute;o Cơ quan cấp Giấy ph&eacute;p đầu tư xem x&eacute;t v&agrave; chuẩn y.</p>
<p>Trong trường hợp một B&ecirc;n kh&ocirc;ng c&oacute; khả năng ho&agrave;n th&agrave;nh nghĩa vụ như đ&atilde; thoả thuận th&igrave; B&ecirc;n đ&oacute; phải th&ocirc;ng b&aacute;o cho c&aacute;c B&ecirc;n kia biết l&yacute; do v&agrave; những biện ph&aacute;p xử l&yacute; trước .....&nbsp;ng&agrave;y. Thiệt hại thực tế v&agrave; ch&iacute;nh đ&aacute;ng do sự chậm trễ hay kh&ocirc;ng c&oacute; khả năng thực hiện nghĩa vụ của một B&ecirc;n g&acirc;y ra, sẽ được bồi thường theo thoả thuận của c&aacute;c B&ecirc;n; trong trường hợp kh&ocirc;ng thoả thuận được th&igrave; sẽ do cơ quan x&eacute;t xử hoặc trọng t&agrave;i quy định tại Điều 10 Hợp đồng n&agrave;y quyết định.</p>
<p>&nbsp;</p>
<p><strong>Điều 4:</strong></p>
<p>C&aacute;c tr&aacute;ch nhiệm, nghĩa vụ kh&aacute;c của c&aacute;c B&ecirc;n trong việc thực hiện Hợp đồng:</p>
<p>- B&ecirc;n Việt Nam: ..................................................................</p>
<p>- B&ecirc;n nước ngo&agrave;i: ...............................................................</p>
<p>(Quy định cụ thể B&ecirc;n chịu tr&aacute;ch nhiệm, thời hạn ho&agrave;n th&agrave;nh.... trong từng việc như tổ chức v&agrave; quản l&yacute; sản xuất, quản l&yacute; kỹ thuật v&agrave; c&ocirc;ng nghệ, mua sắm thiết bị, nguy&ecirc;n liệu, ti&ecirc;u thụ sản phẩm .....).</p>
<p>&nbsp;</p>
<p><strong>Điều 5:</strong></p>
<p>Trong qu&aacute; tr&igrave;nh thực hiện Hợp đồng, c&aacute;c B&ecirc;n thoả thuận cơ chế gi&aacute;m s&aacute;t như sau: ......</p>
<p>(Mi&ecirc;u tả cụ thể cơ chế, trong đ&oacute; c&oacute; thể th&agrave;nh lập Ban điều phối, Văn ph&ograve;ng điều h&agrave;nh của B&ecirc;n nước ngo&agrave;i bao gồm phương thức th&agrave;nh lập, hoạt động, quyền hạn, tr&aacute;ch nhiệm ....).</p>
<p>&nbsp;</p>
<p><strong>Điều 6:</strong></p>
<p>Thời hạn Hợp đồng n&agrave;y l&agrave; ...... năm kể từ ng&agrave;y được cấp Giấy ph&eacute;p đầu tư. Bất kỳ sự thay đổi n&agrave;o về thời hạn của Hợp đồng phải được c&aacute;c B&ecirc;n thoả thuận v&agrave; b&aacute;o c&aacute;o cơ quan cấp Giấy ph&eacute;p đầu tư xem x&eacute;t v&agrave; chuẩn y.</p>
<p>Nếu một B&ecirc;n hợp doanh muốn k&eacute;o d&agrave;i thời hạn Hợp đồng ngo&agrave;i thời hạn đ&atilde; được thoả thuận phải th&ocirc;ng b&aacute;o cho (c&aacute;c) B&ecirc;n kia &iacute;t nhất l&agrave; .... th&aacute;ng trước ng&agrave;y Hợp đồng hết hạn. Nếu c&aacute;c B&ecirc;n thoả thuận k&eacute;o d&agrave;i thời hạn Hợp đồng th&igrave; phải b&aacute;o c&aacute;o Cơ quan cấp Giấy ph&eacute;p đầu tư xem x&eacute;t trong thời hạn &iacute;t nhất l&agrave; 6 th&aacute;ng trước khi hết hết hạn Hợp đồng.</p>
<p>&nbsp;</p>
<p><strong>Điều 7:</strong></p>
<p>Kể từ ng&agrave;y được cấp Giấy ph&eacute;p đầu tư, Hợp đồng n&agrave;y được thực hiện theo tiến độ sau:</p>
<p>1. Khởi c&ocirc;ng x&acirc;y dựng: từ th&aacute;ng thứ ......</p>
<p>2. Lắp đặt thiết bị: từ th&aacute;ng thứ......</p>
<p>3. Vận h&agrave;nh thử: từ th&aacute;ng thứ ......</p>
<p>4. Sản xuất ch&iacute;nh thức: từ th&aacute;ng thứ ......</p>
<p>&nbsp;</p>
<p><strong>Điều 8:</strong></p>
<p>1. (C&aacute;c) B&ecirc;n nước ngo&agrave;i thực hiện đầy đủ nghĩa vụ thuế v&agrave; c&aacute;c nghĩa vụ t&agrave;i ch&iacute;nh kh&aacute;c theo quy định tại Giấy ph&eacute;p đầu tư. (ghi r&otilde; phương thức nộp thuế thu nhập doanh nghiệp v&agrave; thuế chuyển lợi nhuận ra nước ngo&agrave;i của B&ecirc;n nước ngo&agrave;i: nộp trực tiếp, B&ecirc;n Việt Nam nộp hộ v.v....).</p>
<p>2. (C&aacute;c) B&ecirc;n Việt Nam thực hiện đầy đủ nghĩa vụ thuế v&agrave; nghĩa vụ t&agrave;i ch&iacute;nh kh&aacute;c theo quy định của c&aacute;c văn bản quy phạm ph&aacute;p luật &aacute;p dụng đối với Doanh nghiệp trong nước v&agrave; c&aacute;c quy định tại Giấy ph&eacute;p đầu tư.</p>
<p>Lưu &yacute;: cần x&aacute;c định r&otilde; B&ecirc;n chịu tr&aacute;ch nhiệm nộp c&aacute;c loại thuế chung li&ecirc;n quan đến thực hiện Hợp đồng như thuế xuất khẩu, thuế nhập khẩu, thuế gi&aacute; trị gia tăng ....</p>
<p>&nbsp;</p>
<p><strong>Điều 9:</strong></p>
<p>C&aacute;c B&ecirc;n tham gia Hợp đồng thoả thuận chia sản phẩm v&agrave; / hoặc lợi nhuận, c&aacute;c khoản lỗ th&ocirc;ng qua việc thực hiện Hợp đồng như sau:</p>
<p>- (c&aacute;c) B&ecirc;n Việt Nam (ghi r&otilde; từng B&ecirc;n,nếu&nbsp;cần thiết) .....................</p>
<p>- (c&aacute;c) B&ecirc;n Nước ngo&agrave;i (ghi r&otilde; từng B&ecirc;n,nếu&nbsp;cần thiết) ..................</p>
<p>&nbsp;</p>
<p><strong>Điều 10:</strong></p>
<p>Tranh chấp giữa c&aacute;c B&ecirc;n c&oacute; li&ecirc;n quan, hoặc ph&aacute;t sinh từ Hợp đồng trước hết phải được giải quyết th&ocirc;ng qua thương lượng v&agrave; ho&agrave; giải. Trong trường hợp c&aacute;c B&ecirc;n tranh chấp vẫn kh&ocirc;ng thoả thuận được với nhau th&igrave; vụ tranh chấp sẽ được đưa ra... (ghi r&otilde; t&ecirc;n v&agrave; địa chỉ To&agrave; &aacute;n hoặc tổ chức trọng t&agrave;i).</p>
<p>Quyết định của.....(tổ chức tr&ecirc;n) l&agrave; chung thẩm v&agrave; c&aacute;c B&ecirc;n phải tu&acirc;n theo.</p>
<p>&nbsp;</p>
<p><strong>Điều 11:</strong></p>
<p>C&aacute;c B&ecirc;n c&oacute; quyền chuyển nhượng gi&aacute; trị phần vốn của m&igrave;nh trong Hợp đồng, theo c&aacute;c qui định tại Điều 34 Luật Đầu tư nước ngo&agrave;i v&agrave; c&aacute;c điều khoản li&ecirc;n quan của Nghị định ..../20..../NĐ-CP của Ch&iacute;nh phủ.</p>
<p>&nbsp;</p>
<p><strong>Điều 12:</strong></p>
<p>Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y c&oacute; thể chấm dứt hoạt động trước thời hạn v&agrave; / hoặc kết th&uacute;c trong c&aacute;c trường hợp sau: .......</p>
<p>(m&ocirc; tả chi tiết c&aacute;c trường hợp, ph&ugrave; hợp với c&aacute;c quy định tại Điều 52 Luật Đầu tư nước ngo&agrave;i v&agrave; c&aacute;c điều khoản li&ecirc;n quan của Nghị định ..../20..../NĐ-CP của Ch&iacute;nh phủ).</p>
<p>&nbsp;</p>
<p><strong>Điều 13:</strong></p>
<p>Khi Hợp đồng hết hạn, c&aacute;c B&ecirc;n thoả thuận việc thanh l&yacute; c&aacute;c t&agrave;i sản li&ecirc;n quan đến quyền lợi v&agrave; nghĩa vụ của c&aacute;c B&ecirc;n trong Hợp đồng hợp t&aacute;c kinh doanh như sau: .......</p>
<p>(M&ocirc; tả chi tiết c&aacute;c điều kiện c&oacute; li&ecirc;n quan đến quyền hạn, nghĩa vụ, t&agrave;i sản .....&nbsp;ph&ugrave; hợp với c&aacute;c quy định tại Điều 53 Luật Đầu tư nước ngo&agrave;i v&agrave; c&aacute;c điều khoản li&ecirc;n quan của Nghị định ..../20..../NĐ-CP)</p>
<p>&nbsp;</p>
<p><strong>Điều 14:</strong></p>
<p>Mọi điều khoản kh&aacute;c c&oacute; li&ecirc;n quan kh&ocirc;ng được quy định cụ thể tại Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y sẽ được c&aacute;c B&ecirc;n thực hiện theo quy định của ph&aacute;p luật Việt Nam v&agrave; của Giấy ph&eacute;p đầu tư.</p>
<p>&nbsp;</p>
<p><strong>Điều 15:</strong></p>
<p>Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y c&oacute; thể được sửa đổi, bổ sung sau khi c&oacute; thoả thuận bằng văn bản giữa c&aacute;c B&ecirc;n v&agrave; phải được cơ quan cấp Giấy ph&eacute;p đầu tư chuẩn y trước khi thực hiện.</p>
<p>&nbsp;</p>
<p><strong>Điều 16:</strong></p>
<p>Hợp đồng n&agrave;y c&oacute; hiệu lực kể từ ng&agrave;y được cấp Giấy ph&eacute;p đầu tư.</p>
<p>&nbsp;</p>
<p><strong>Điều 17:</strong></p>
<p>Hợp đồng hợp t&aacute;c kinh doanh n&agrave;y được k&yacute; ng&agrave;y ........, tại........, gồm .....&nbsp;bản gốc bằng tiếng Việt Nam v&agrave; bằng tiếng .....&nbsp;(tiếng nước ngo&agrave;i th&ocirc;ng dụng). Cả hai bản tiếng Việt Nam v&agrave; tiếng ...... đều c&oacute; gi&aacute; trị ph&aacute;p l&yacute; như nhau.</p>
<table cellspacing="0.6666667">
<tbody>
<tr>
<td>
<p><strong>Đại diện B&ecirc;n (c&aacute;c B&ecirc;n) nước ngo&agrave;i&nbsp;</strong></p>
</td>
<td>
<p><strong>&nbsp;Đại diện&nbsp;B&ecirc;n (c&aacute;c B&ecirc;n) Việt Nam</strong></p>
</td>
</tr>
<tr>
<td>
<p>( K&yacute; t&ecirc;n, chức vụ v&agrave; dấu)</p>
</td>
<td>
<p>&nbsp;(K&yacute; t&ecirc;n, chức vụ v&agrave; dấu)</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><em>Lưu &yacute;:&nbsp;</em><em>Trường hợp c&oacute; nhiều B&ecirc;n: từng b&ecirc;n sẽ k&yacute;, ghi r&otilde; chức vụ người đại diện k&yacute;, đ&oacute;ng dấu.</em></p>
<p>&nbsp;</p>`;
