import { initializeApp } from 'firebase/app';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
    apiKey: 'AIzaSyDP1OgyWbkaZVt3cVKxZwMm8TYahwqvMlQ',
    authDomain: 'gasquynhl.firebaseapp.com',
    projectId: 'gasquynhl',
    storageBucket: 'gasquynhl.appspot.com',
    messagingSenderId: '306691626843',
    appId: '1:306691626843:web:d167071f09400d52746c7e',
    measurementId: 'G-X0B0ELEQ5E',
};

const app = initializeApp(firebaseConfig);
const storage = getStorage(app);

function base64ToBlob(base64Data: string, contentType: string) {
    const byteCharacters = atob(base64Data.split(',')[1]);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += 512) {
        const slice = byteCharacters.slice(offset, offset + 512);
        const byteNumbers = new Array(slice.length);

        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
}

export { storage, base64ToBlob };
