import { io } from 'socket.io-client';
import { COMPANY_EVENTS } from '../common/constants';

const socket = io('http://localhost:7001');

socket.on('connect', () => {
    socket.on(COMPANY_EVENTS.USER_CREATED_COMPANY, (data) => {
        console.log(data);
    });
});

export { socket };
