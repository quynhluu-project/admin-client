import React from 'react';
import { Popover, Select } from 'antd';
import { ErrorMessage } from 'formik';
import './select-group.scss';
import AsteriskIcon from '../../icons/asterisk';
type TypeProps = {
    label?: string;
    nameField: string;
    isRequire?: boolean;
    tutorial?: React.ReactNode;
    placeholder?: string;
    labelClass?: string;
    inputClass?: string;
    setFieldValue?: any;
    options?: any[];
    mode?: 'multiple' | 'tags' | undefined;
    [key: string]: any;
};
function SelectGroup({
    label,
    tutorial,
    placeholder,
    nameField,
    isRequire = false,
    labelClass,
    inputClass,
    type,
    defaultValue,
    options,
    setFieldValue,
    mode,
    ...props
}: TypeProps) {
    const handleChoseItem = (value: any) => {
        setFieldValue(nameField, value);
    };

    return (
        <div className="flex flex-col w-full">
            <div className="flex justify-between items-end py-1">
                {label && (
                    <div className="relative">
                        <label htmlFor="" className={labelClass}>
                            {label}
                        </label>
                        {isRequire && (
                            <div className="absolute top-0 right-[-10px]">
                                <AsteriskIcon width={8} height={8} />
                            </div>
                        )}
                    </div>
                )}

                {tutorial && (
                    <Popover placement="topRight" content={<div className="w-[300px]">{tutorial}</div>} trigger="click">
                        <div className="text-xs text-[#26de81] cursor-pointer">Hướng dẫn</div>
                    </Popover>
                )}
            </div>
            <div className={` rounded-md ${inputClass}`}>
                <Select
                    mode={mode}
                    onChange={handleChoseItem}
                    maxTagCount="responsive"
                    placeholder={placeholder}
                    options={options}
                    {...props}
                />
            </div>
            {isRequire && (
                <div className="text-red-500">
                    <ErrorMessage name={nameField} />
                </div>
            )}
        </div>
    );
}

export default SelectGroup;
