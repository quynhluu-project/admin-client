import { Pagination } from 'antd';
import React from 'react';
import './style.scss';
type PaginationProps = {
    total?: number;
    limit?: number;
    onChange?: (page: number, pageSize: number) => any;
    [key: string]: any;
};

function PaginationApp({ total, limit, onChange, ...props }: PaginationProps) {
    return (
        <div className="pagination__app">
            <Pagination
                total={total}
                defaultPageSize={limit}
                onChange={onChange}
                pageSizeOptions={[16, 32, 64, 128]}
                {...props}
            />
        </div>
    );
}

export default PaginationApp;
