import { ArrowIcon } from '../icons/arrow';

export const Arrow = (props: any) => {
    const { type, onClick } = props;
    return (
        <div
            className={`cursor-pointer z-10 absolute top-1/2 ${type === 'NEXT' ? 'rotate-180 right-3' : 'left-3'}`}
            onClick={onClick}
        >
            <div className="w-[30px] h-[30px] bg-[#A75D5D] flex justify-center items-center rounded-full transform">
                <ArrowIcon fill="#fff" />
            </div>
        </div>
    );
};
