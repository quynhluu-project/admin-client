import React from 'react';
import { DatePicker, Popover } from 'antd';
import { Dayjs } from 'dayjs';
import './date-picker.scss';

type TypeProps = {
    picker: any;
    format?: string;
    label?: React.ReactNode;
    tutorial?: React.ReactNode;
    labelClass?: string;
    disabledDate?: ((date: Dayjs) => boolean) | undefined;
    [key: string]: any;
};
function AppDatePicker({ format, picker, label, tutorial, labelClass, disabledDate, ...props }: TypeProps) {
    if (!format) {
        format = 'DD-MM-YYYY';
    }
    return (
        <div className="flex flex-col">
            <div className="flex justify-between items-end py-1">
                <label htmlFor="" className={labelClass}>
                    {label && label}
                </label>

                {tutorial && (
                    <Popover placement="topRight" content={<div className="w-[300px]">{tutorial}</div>} trigger="click">
                        <div className="text-xs text-[#26de81] cursor-pointer">Hướng dẫn</div>
                    </Popover>
                )}
            </div>
            <DatePicker picker={picker} format={format} disabledDate={disabledDate} {...props} />
        </div>
    );
}

export default AppDatePicker;
