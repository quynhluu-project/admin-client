import React, { useCallback } from 'react';
import './style.scss';
type ButtonApplyProps = {
    isLoading?: boolean;
    label?: React.ReactNode;
    onClick?: () => any;
    isDisabled?: boolean;
    icon?: React.ReactNode;
};

function ButtonApply({ isDisabled, isLoading, label = 'Button', onClick, icon }: ButtonApplyProps) {
    const handleClick = useCallback(() => {
        !isDisabled && !isLoading && onClick && onClick();
    }, [isDisabled, isLoading, onClick]);

    return (
        <button className="cssbuttons-io-button" disabled={isDisabled} onClick={handleClick}>
            {label}
            <div className="icon">
                {isLoading ? (
                    <div className="dot-spinner">
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                        <div className="dot-spinner__dot"></div>
                    </div>
                ) : (
                    icon
                )}
            </div>
        </button>
    );
}

export default ButtonApply;
