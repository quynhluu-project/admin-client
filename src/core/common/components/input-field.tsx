import React from 'react';
import { Popover } from 'antd';
import { ErrorMessage, Field } from 'formik';
import { Input } from 'antd';
import AsteriskIcon from '../icons/asterisk';
export enum TypeInput {
    TEXT = 'text',
    TEXTAREA = 'textarea',
}
type TypeProps = {
    label?: string;
    nameField: string;
    isRequire?: boolean;
    tutorial?: React.ReactNode;
    placeholder?: string;
    labelClass?: string;
    inputClass?: string;
    type?: any;
    [key: string]: any;
};
function InputFiled({
    label,
    tutorial,
    placeholder,
    nameField,
    isRequire = false,
    labelClass,
    inputClass,
    type,
    ...props
}: TypeProps) {
    return (
        <div className="flex flex-col w-full">
            <div className="flex justify-between items-end py-1">
                {label && (
                    <div className="relative">
                        <label htmlFor="" className={labelClass}>
                            {label}
                        </label>
                        {isRequire && (
                            <div className="absolute top-0 right-[-10px]">
                                <AsteriskIcon width={8} height={8} />
                            </div>
                        )}
                    </div>
                )}

                {tutorial && (
                    <Popover placement="topRight" content={<div className="w-[300px]">{tutorial}</div>} trigger="click">
                        <div className="text-xs text-[#26de81] cursor-pointer">Hướng dẫn</div>
                    </Popover>
                )}
            </div>
            <div className={`bg-gray-200 p-2 rounded-md border ${inputClass}`}>
                <Field
                    as={type}
                    name={nameField}
                    placeholder={placeholder}
                    className={'bg-transparent outline-none border-none w-full h-full placeholder:italic'}
                    type={type}
                    {...props}
                />
            </div>
            {isRequire && (
                <div className="text-red-500">
                    <ErrorMessage name={nameField} />
                </div>
            )}
        </div>
    );
}

export function InputFiledOutSite({
    label,
    tutorial,
    placeholder,
    nameField,
    isRequire = false,
    labelClass,
    inputClass,
    type,
    ...props
}: TypeProps) {
    return (
        <div className="flex flex-col w-full">
            <div className="flex justify-between items-end py-1">
                <label htmlFor="" className={labelClass}>
                    {label && label}
                </label>

                {tutorial && (
                    <Popover placement="topRight" content={<div className="w-[300px]">{tutorial}</div>} trigger="click">
                        <div className="text-xs text-[#26de81] cursor-pointer">Hướng dẫn</div>
                    </Popover>
                )}
            </div>
            <div className={`bg-gray-200 p-2 rounded-md border ${inputClass}`}>
                <Input
                    name={nameField}
                    placeholder={placeholder}
                    className={'bg-transparent outline-none border-none w-full h-full'}
                    {...props}
                />
            </div>
            {isRequire && (
                <div className="text-red-500">
                    <ErrorMessage name={nameField} />
                </div>
            )}
        </div>
    );
}

export default InputFiled;
