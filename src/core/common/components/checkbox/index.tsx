import React from 'react';
import './style.scss';
import { Popover } from 'antd';
function CheckboxApp({ label, labelClass, tutorial, onChange, ...props }: any) {
    return (
        <div className="flex flex-col">
            <div className="flex justify-between items-end py-1">
                {label && (
                    <label htmlFor="" className={labelClass}>
                        {label}
                    </label>
                )}

                {tutorial && (
                    <Popover placement="topRight" content={<div className="w-[300px]">{tutorial}</div>} trigger="click">
                        <div className="text-xs text-[#26de81] cursor-pointer">Hướng dẫn</div>
                    </Popover>
                )}
            </div>
            <div className="h-[40px] flex items-center">
                <div className="checkbox-wrapper-19">
                    <input type="checkbox" id="cbtest-19" onChange={onChange} {...props} />
                    <label htmlFor="cbtest-19" className="check-box" />
                </div>
            </div>
        </div>
    );
}

export default CheckboxApp;
