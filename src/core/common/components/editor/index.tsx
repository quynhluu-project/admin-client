import { Editor } from '@tinymce/tinymce-react';
import { Popover } from 'antd';
import { useRef } from 'react';
import AsteriskIcon from '../../icons/asterisk';

type TinyEditorType = {
    onSetContent: (data: any) => any;
    height?: number;
};

export default function TinyEditor({ onSetContent, height = 300 }: TinyEditorType) {
    const editorRef = useRef<any>();
    const log = () => {
        if (editorRef.current) {
            onSetContent(editorRef.current?.getContent());
        }
    };
    return (
        <>
            <Editor
                onChange={(data: any) => console.log(data)}
                apiKey="enirabnbigbksd2a6rhyzkp2oh1fc2umrp5cmht82wnqz22k"
                onInit={(evt, editor) => (editorRef.current = editor)}
                // initialValue="<p>This is the initial content of the editor.</p>"
                init={{
                    plugins: 'table image link code autolink fullscreen lists',
                    toolbar:
                        'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
                    tinycomments_mode: 'embedded',
                    tinycomments_author: 'Author name',
                    mergetags_list: [
                        { value: 'First.Name', title: 'First Name' },
                        { value: 'Email', title: 'Email' },
                    ],
                    ai_request: (request: any, respondWith: any) =>
                        respondWith.string(() => Promise.reject('See docs to implement AI Assistant')),
                    height,
                }}
                onEditorChange={log}
            />
        </>
    );
}

type EditorAppProps = {
    onSetContent: (data: string) => any;
    isRequire?: boolean;
    tutorial?: React.ReactNode;
    placeholder?: string;
    labelClass?: string;
    inputClass?: string;
    label?: string;
    error?: string;
    height?: number;
    [key: string]: any;
};

export const EditorApp = ({
    onSetContent,
    label,
    labelClass,
    tutorial,
    isRequire,
    error,
    height = 300,
    ...props
}: EditorAppProps) => {
    const { initialValue } = props;
    const editorRef = useRef<any>();
    const log = () => {
        if (editorRef.current) {
            onSetContent(editorRef.current?.getContent());
        }
    };
    return (
        <div className="flex flex-col border rounded-[16px] my-2">
            <div className="flex justify-between items-end py-1 mx-2">
                {label && (
                    <div className="relative">
                        <label htmlFor="" className={labelClass}>
                            {label}
                        </label>
                        {isRequire && (
                            <div className="absolute top-0 right-[-12px]">
                                <AsteriskIcon width={8} height={8} />
                            </div>
                        )}
                    </div>
                )}

                {tutorial && (
                    <Popover placement="topRight" content={<div className="w-[300px]">{tutorial}</div>} trigger="click">
                        <div className="text-xs text-[#26de81] cursor-pointer">Hướng dẫn</div>
                    </Popover>
                )}
            </div>
            <div>
                <Editor
                    apiKey="enirabnbigbksd2a6rhyzkp2oh1fc2umrp5cmht82wnqz22k"
                    init={{
                        plugins:
                            'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                        toolbar:
                            'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                    }}
                    onInit={(evt, editor) => (editorRef.current = editor)}
                    onEditorChange={log}
                    value={initialValue}
                />
            </div>
            {isRequire && <div className="text-red-500">{error}</div>}
        </div>
    );
};
