import React from 'react';

function AsteriskIcon({ width = 20, height = 20 }) {
    return (
        <svg
            width={width}
            height={height}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
        >
            <rect width="24" height="24" fill="url(#pattern0)" />
            <defs>
                <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                    <use xlinkHref="#image0_439_140" transform="scale(0.0416667)" />
                </pattern>
                <image
                    id="image0_439_140"
                    width="24"
                    height="24"
                    xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYEAYAAACw5+G7AAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAGYktHRAAAAAAAAPlDu38AAAAJcEhZcwAAAGAAAABgAPBrQs8AAAAHdElNRQfnCxwNOS5wAEuKAAABe0lEQVRYw9VXvdKCQAxcHLB1rH0l9SEUG8cXcHxc6YRhBMHKO4t84SMMP4cgYJotPJLdJF5yQM+mNQBst4Sel6ECgM2m73g9E7cswiAg1Fqi7/O54YgpAJjPCW27/txiUU48h3/n6v04DmMTv1m9I9eFBQD3O2EYEpHdrvdECb9hyHEzHq0zrgHg+SzPpFKEx2PXCrAf6bf4XZoyL0MBjkMfxnE9oX8hbQWYEWeMoqYWriypWQD+/XxuFCDOmfrt0LKk/HAgR6+XGcGuqBTFPZ0+Jl4vpClzIxCX9/hyWY+Xy3cEsN+m+Lk5Iidn1QCaGgYBT3aLRz3JWa1667lBzPNm3Z2MaVqDSyF3lbFbpAl9n3iv13kt+Mk/sXGxpn6Njkv8C0J+dpWQ+/jjMY1lLo6Nlzm5TqfpNNbpJDF94BSEuK4UEkVVJe36IpMty2t8khDu98bEy4lxSw31pLTt1hnvanKOlAxEBQC328f3+GBCxGS/XjMsTs6e7A3JqnMg0ooEdwAAAABJRU5ErkJggg=="
                />
            </defs>
        </svg>
    );
}

export default AsteriskIcon;
