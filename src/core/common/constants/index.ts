/**
 * REGEX
 */
export const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]+$/;

/**
 * VARIABLE
 */

export enum AccessControl {
    OWNER = 'OWNER',
    COMPANY = 'COMPANY',
}

export const COMPANY_EVENTS = {
    USER_CREATED_COMPANY: 'USER_CREATED_COMPANY',
};
export const CONTRACT_EVENTS = {
    CREATED_CONTRACT: 'CREATED_CONTRACT',
};

export const COMPANY_ROOMS = {
    MANAGER: 'MANAGER',
    OWNER: 'OWNER',
};

export const LOCAL_STORAGE_KEY = {
    ACCESS_TOKEN: 'ACCESS_TOKEN',
    REFRESH_TOKEN: 'REFRESH_TOKEN',
    PROFILE: 'PROFILE',
};

export const APP_ENV = {
    PRODUCTION: 'production',
    DEV: 'study',
    LOCAL: 'local',
};

export const TYPE_DRAWER = {
    CREATE_BRANCH: 'CREATE_BRANCH',
    CREATE_DEPARTMENT: 'CREATE_DEPARTMENT',
};

export const APP_ROLE = {
    ROOT_MANAGER: 'ROOT_MANAGER',
    MANAGER: 'MANAGER',
    EMPLOYEE: 'EMPLOYEE',
};

export const APP_ROLE_OPTIONS = [
    { label: 'Tổng giám đốc điều hành', value: 'ROOT_MANAGER' },
    { label: 'Quản lí điều hành', value: 'MANAGER' },
    { label: 'Nhân viên', value: 'EMPLOYEE' },
];

export const APP_ACCESS_CONTROL = [
    { label: 'Cấp 1 (Full quyền)', value: 'ROOT_MANAGER' },
    { label: 'Cấp 2 (1 vài quyền bị giới hạn)', value: 'MANAGER' },
    { label: 'Nhân viên (1 vài quyền)', value: 'EMPLOYEE' },
];

export const TYPE_NOTIFICATION = {
    CREATED_CONTRACT: 'CREATED_CONTRACT',
};
