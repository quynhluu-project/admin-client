import { Outlet } from 'react-router-dom';
import HeaderDashboard from './components/header';
import Sidebar from './components/sidebar';
import { useState } from 'react';

const DefaultLayout = () => {
    const [collapsed, setCollapsed] = useState(false);

    const toggleCollapsed = () => {
        setCollapsed(!collapsed);
    };
    return (
        <>
            <HeaderDashboard />
            <div className="relative flex">
                <div
                    className={`h-[calc(100vh-50px)] fixed top-[50px] z-50 bg-white ${
                        collapsed ? 'w-[80px]' : 'w-[256px]'
                    }`}
                >
                    <Sidebar collapsed={collapsed} toggleCollapsed={toggleCollapsed} />
                </div>
                <div
                    className={`bg-gray-100 absolute transition-all z-40 top-[50px] ${
                        collapsed ? 'left-[80px] w-[calc(100%-80px)]' : 'left-[256px] w-[calc(100%-256px)]'
                    }`}
                >
                    <Outlet></Outlet>
                </div>
            </div>
        </>
    );
};

export default DefaultLayout;
