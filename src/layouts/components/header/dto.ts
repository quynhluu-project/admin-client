export interface NotificationAttributes {
    id?: number;
    type?: string;
    from?: string;
    to?: string;
    content?: string;
    metadata?: string;
    isReading?: boolean;
    createdAt?: string;
    updatedAt?: string;
}
