import { FromNow } from '~/core/util/date';

type TypeProps = {
    createdAt: string;
};

export const DescriptionCartItem = ({ createdAt }: TypeProps) => {
    return (
        <div className="flex items-center">
            <div className="text-[10px] text-[#FFC3A1] font-semibold">{FromNow(createdAt)}</div>
        </div>
    );
};
