import { ReceiveMoneyIcon } from '~/core/common/icons/receive-money';

export const AvatarCartItem = () => {
    return (
        <div className="w-[30px] h-[30px] p-[6px] flex items-center justify-center rounded-full bg-[#3498db]">
            <ReceiveMoneyIcon fill="#fff" />
        </div>
    );
};
