import { Link } from 'react-router-dom';
import { TrashIcon } from '~/core/common/icons/trash';

type TypeProps = {
    title: string;
    href: string;
    onClick?: () => any;
};

export const TitleCartItem = ({ title, href, onClick }: TypeProps) => {
    return (
        <div className="flex justify-between" onClick={onClick}>
            <Link to={href} className="hover:text-[#A75D5D]">
                <div className="text-sm line-clamp-2 min-h-[40px]"> {title}</div>
            </Link>
        </div>
    );
};
