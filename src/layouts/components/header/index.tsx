import React, { useState, memo, useEffect, useCallback, useMemo, useRef } from 'react';
import { SearchIcon } from '~/core/common/icons/search';
import { Badge, Popover, Tabs, List } from 'antd';
import BellIcon from '~/core/common/icons/bell';
import { AvatarCartItem } from './components/avatar-cart-item';
import { TitleCartItem } from './components/title-cart-item';
import { DescriptionCartItem } from './components/description-cart-item';
import { Link, useNavigate } from 'react-router-dom';
import ProfileIcon from '~/core/common/icons/profile';
import SettingIcon from '~/core/common/icons/setting';
import { LogoutIcon } from '~/core/common/icons/logout';
import LocalStorage from '~/core/util/storage';
import { CONTRACT_EVENTS, LOCAL_STORAGE_KEY, TYPE_NOTIFICATION } from '~/core/common/constants';
import './header.scss';
import { socket } from '~/core/socket';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';
import { HeaderService } from './service';
import { NotificationAttributes } from './dto';
function HeaderDashboard() {
    const navigate = useNavigate();

    const audioRef = useRef<any>();
    const [notifications, setNotifications] = useState<NotificationAttributes[]>([]);
    const [notificationNoRead, setNotificationNoRead] = useState(0);
    const { request: getNotificationsOfUser } = usePrepareExecuteApi<string, any>({
        apiWillExecute: HeaderService.getNotificationsOfUser,
        isUseServerMes: true,
    });
    const { name, avatar, email } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);

    const handleLogout = () => {
        LocalStorage.clear();
        navigate('/login');
    };

    const account = (
        <div className="flex flex-col">
            <Link to={'/profile'} className="py-1 flex items-center gap-2 border-b-[1px]">
                <div className="w-[20px] h-[20px] text-center flex items-center justify-center">
                    <ProfileIcon width={15} height={15} />
                </div>
                <span>Hồ sơ cá nhân</span>
            </Link>
            <Link to={'/profile'} className="py-1 flex items-center gap-2 border-b-[1px]">
                <div className="w-[20px] h-[20px] text-center flex items-center justify-center">
                    <SettingIcon />
                </div>
                <span>Thiết lập cá nhân</span>
            </Link>
            <div className="cursor-pointer py-1 flex items-center gap-2" onClick={handleLogout}>
                <div className="w-[20px] h-[20px] text-center flex items-center justify-center">
                    <LogoutIcon width={20} height={20} />
                </div>
                <span>Đăng xuất</span>
            </div>
        </div>
    );

    const onChange = (key: string) => {
        console.log(key);
    };

    const emailOrg = useMemo(() => {
        const wordsToReplace = ['root_manager_', 'manager_'];
        const regex = new RegExp(wordsToReplace.join('|'), 'g');
        return email.replace(regex, '');
    }, [email]);

    const handleGetNotificationOfUser = useCallback(async () => {
        const data = await getNotificationsOfUser(emailOrg);
        setNotifications(data?.report?.rows || []);
        setNotificationNoRead(data?.report?.count || 0);
    }, [email]);

    const reportCount = useMemo(() => {
        const noti = notifications.reduce((total, value) => {
            if (!value.isReading) {
                return total + 1;
            }
            return total;
        }, 0);
        return { noti, total: noti };
    }, [notifications]);

    const renderNotification = useCallback(() => {
        return [
            {
                key: '1',
                label: `Thông Báo(${reportCount.noti})`,
                children: (
                    <div className="">
                        <div className="max-h-[400px] overflow-y-auto">
                            <List
                                className="list-item-cart"
                                itemLayout="horizontal"
                                dataSource={notifications}
                                renderItem={(item: NotificationAttributes) => {
                                    if (item.type === TYPE_NOTIFICATION.CREATED_CONTRACT) {
                                        const { contractId } = JSON.parse(item.metadata || '');
                                        return (
                                            <List.Item>
                                                <List.Item.Meta
                                                    avatar={<AvatarCartItem />}
                                                    title={
                                                        <TitleCartItem
                                                            title={item.content || ''}
                                                            href={`/contract/${contractId?.toLowerCase()}`}
                                                            onClick={() => {}}
                                                        />
                                                    }
                                                    description={
                                                        <DescriptionCartItem createdAt={item.createdAt || ''} />
                                                    }
                                                />
                                            </List.Item>
                                        );
                                    }
                                }}
                            />
                        </div>
                    </div>
                ),
            },
            {
                key: '2',
                label: `Tin Nhắn`,
                children: (
                    <div className="">
                        <div className="max-h-[400px] overflow-y-auto">
                            <List
                                className="list-item-cart"
                                itemLayout="horizontal"
                                dataSource={[]}
                                renderItem={(item: NotificationAttributes) => {
                                    if (item.type === TYPE_NOTIFICATION.CREATED_CONTRACT) {
                                        const { companyId } = JSON.parse(item.metadata || '');
                                        return (
                                            <List.Item>
                                                <List.Item.Meta
                                                    avatar={<AvatarCartItem />}
                                                    title={
                                                        <TitleCartItem
                                                            title={item.content || ''}
                                                            href={`/company/detail/${companyId}`}
                                                            onClick={() => {}}
                                                        />
                                                    }
                                                    description={
                                                        <DescriptionCartItem createdAt={item.createdAt || ''} />
                                                    }
                                                />
                                            </List.Item>
                                        );
                                    }
                                }}
                            />
                        </div>
                    </div>
                ),
            },
            {
                key: '3',
                label: `Việc Cần Làm`,
                children: (
                    <div className="">
                        <div className="max-h-[400px] overflow-y-auto">
                            <List
                                className="list-item-cart"
                                itemLayout="horizontal"
                                dataSource={[]}
                                renderItem={(item: NotificationAttributes) => {
                                    if (item.type === TYPE_NOTIFICATION.CREATED_CONTRACT) {
                                        const { companyId } = JSON.parse(item.metadata || '');
                                        return (
                                            <List.Item>
                                                <List.Item.Meta
                                                    avatar={<AvatarCartItem />}
                                                    title={
                                                        <TitleCartItem
                                                            title={item.content || ''}
                                                            href={`/company/detail/${companyId}`}
                                                            onClick={() => {}}
                                                        />
                                                    }
                                                    description={
                                                        <DescriptionCartItem createdAt={item.createdAt || ''} />
                                                    }
                                                />
                                            </List.Item>
                                        );
                                    }
                                }}
                            />
                        </div>
                    </div>
                ),
            },
        ];
    }, [notifications, notificationNoRead, reportCount]);

    useEffect(() => {
        const reCallData = (data: any) => {
            if (data.to === emailOrg) {
                handleGetNotificationOfUser();
                audioRef.current?.play();
            }
        };

        socket.on(CONTRACT_EVENTS.CREATED_CONTRACT, reCallData);

        return () => {
            socket.off(CONTRACT_EVENTS.CREATED_CONTRACT, reCallData);
        };
    }, []);

    useEffect(() => {
        handleGetNotificationOfUser();
    }, [email]);

    return (
        <header className="fixed w-full z-50">
            <div className="bg-[#a75d5d]">
                <div className="mx-auto container">
                    <div className="flex items-center justify-between">
                        <div className="logo flex items-center h-[50px]">
                            <div className="w-[40px] h-[40px]">
                                <img src="/images/favicon.png" alt="" className="w-full h-full object-cover" />
                            </div>
                            <div className="relative top-[6px] ml-2">
                                <span className="font-bold uppercase text-lg text-white">Quỳnh Lưu</span>
                                <div className="absolute top-[-14px] right-0 transform translate-x-[100%] text-white uppercase">
                                    admin
                                </div>
                            </div>
                        </div>
                        <div className="action flex items-center">
                            <div className="mr-[30px]">
                                <div className="bg-white lg:w-[300px] xl:w-[350px] rounded-full hidden flex-1 lg:flex">
                                    <input
                                        type="text"
                                        placeholder="Tên sản phẩm..."
                                        className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                                    />
                                    <button className=" rounded-full px-2 py-1">
                                        <SearchIcon fill="#7286D3" />
                                    </button>
                                </div>
                            </div>
                            <div className="px-2 cursor-pointer text-white">
                                <Popover
                                    content={
                                        <Tabs defaultActiveKey="1" items={renderNotification()} onChange={onChange} />
                                    }
                                    placement="bottomRight"
                                    trigger="click"
                                >
                                    <Badge count={reportCount.total}>
                                        <div className="h-[15px] w-[30px]">
                                            <BellIcon fill="#fff" />
                                        </div>
                                    </Badge>
                                </Popover>
                            </div>
                            <div className="px-2 cursor-pointer">
                                <Popover content={account} placement="bottomRight" trigger="click">
                                    <div className="flex items-center gap-2">
                                        <div className="w-[30px] h-[30px] rounded-full overflow-hidden">
                                            <img src={avatar} alt="" className="w-full h-full object-cover" />
                                        </div>
                                        <div className="text-white text-sm">{name}</div>
                                    </div>
                                </Popover>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <audio controls className="hidden" ref={audioRef}>
                <source src="/audio/noti-message.mp3" type="audio/mpeg" />
                Your browser does not support the audio element.
            </audio>
        </header>
    );
}

export default memo(HeaderDashboard);
