import { ConfigEnv } from '~/configs';
import AxiosInstance from '~/core/util/axios';

export class HeaderService {
    constructor(readonly HTTPClient: typeof AxiosInstance) {}

    static async getNotificationsOfUser(to?: string) {
        const result = await AxiosInstance.get(`${ConfigEnv.NOTIFICATION_DOMAIN}/api-v1/notifications-of-owner/${to}`);
        return result.data; //
    }
}
