import React, { useState } from 'react';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import MenuSidebarIcon from '~/core/common/icons/menu-sidebar';
import DashboardIcon from '~/core/common/icons/dashboard';
import ProductIcon from '~/core/common/icons/product';
import ProductListIcon from '~/core/common/icons/product-list';
import AnalysisIcon from '~/core/common/icons/analysis';
import CreateIcon from '~/core/common/icons/create';
import BookIcon from '~/core/common/icons/book';
import HumanIcon from '~/core/common/icons/human';
import OrganizationIcon from '~/core/common/icons/organization';
import DefaultSettingIcon from '~/core/common/icons/default-setting';
import { useNavigate, useLocation } from 'react-router-dom';
import CompanyIcon from '~/core/common/icons/company';
import LocalStorage from '~/core/util/storage';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import './sidebar.scss';
type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
    disabled?: boolean,
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
        disabled,
    } as MenuItem;
}
type TypeProps = {
    collapsed: boolean;
    toggleCollapsed: () => any;
};
function Sidebar({ collapsed, toggleCollapsed }: TypeProps) {
    const navigate = useNavigate();
    const location = useLocation();
    const currentUrl = location.pathname;
    const { accessControl } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);
    const [openSidebar] = useState({
        parentPath: currentUrl.split('/')[1],
        childrenPath: currentUrl.split('/').pop(),
    });

    const onClick: MenuProps['onClick'] = (e) => {
        const { keyPath } = e;
        navigate(keyPath.reverse().join(''));
    };

    const items: MenuProps['items'] = [
        getItem('Bảng điều khiển', '/dashboard', <DashboardIcon fill="#2c3e50" width={25} height={25} />, [
            getItem('Phân tích', '/analysis', <AnalysisIcon fill="#2c3e50" width={25} height={25} />),
        ]),
        { type: 'divider' },
        getItem(
            'Sản phẩm',
            '/product',
            <ProductIcon fill="#2c3e50" width={25} height={25} />,
            accessControl === 'SUPER_OWNER'
                ? [
                      getItem('Danh sách sản phẩm', '/list', <ProductListIcon fill="#2c3e50" width={25} height={25} />),
                      getItem('Tạo mới', '/create', <CreateIcon fill="#2c3e50" width={25} height={25} />),
                      getItem(
                          'Thêm sản phẩm khuyên mãi',
                          '/create-discount',
                          <CreateIcon fill="#2c3e50" width={25} height={25} />,
                          undefined,
                          undefined,
                          false,
                      ),
                  ]
                : [
                      getItem('Danh sách sản phẩm', '/list', <ProductListIcon fill="#2c3e50" width={25} height={25} />),
                      getItem('Tạo mới', '/create', <CreateIcon fill="#2c3e50" width={25} height={25} />),
                  ],
            undefined,
            false, // access control
        ),
        getItem(
            'Tin Tức',
            '/news',
            <ProductIcon fill="#2c3e50" width={25} height={25} />,
            accessControl === 'SUPER_OWNER'
                ? [
                      getItem(
                          'Danh sách tin tức',
                          '/list-news',
                          <ProductListIcon fill="#2c3e50" width={25} height={25} />,
                      ),
                      getItem('Tạo mới', '/create-news', <CreateIcon fill="#2c3e50" width={25} height={25} />),
                  ]
                : [
                      getItem('Thị trường', '/market', <ProductListIcon fill="#2c3e50" width={25} height={25} />),
                      getItem(
                          'Danh sách tin tức',
                          '/list-news',
                          <ProductListIcon fill="#2c3e50" width={25} height={25} />,
                      ),
                  ],
            undefined,
            false, // access control
        ),
        { type: 'divider' },
        getItem('Sổ sách', '/book', <BookIcon fill="#2c3e50" width={25} height={25} />, [
            getItem('Ghi nợ', '/debit'),
            getItem('Hợp đồng nhập hàng', '/contract-import'),
            getItem('Hợp đồng xuất hàng', '/contract-export'),
            // getItem('Chấm công', '/time-keep-ping'),
            // getItem('Nhập hàng', '/import'),
            // getItem('Bán hàng', '/sell'),
        ]),
        getItem('Nhân sự', '/people', <HumanIcon fill="#2c3e50" width={25} height={25} />, [
            getItem('Quản lí', '/manager'),
            getItem('Nhân viên', '/employee'),
            getItem('CTV', '/collaborators'),
            getItem('Khác', '/orther'),
        ]),
        getItem('Công ty', '/company', <CompanyIcon fill="#2c3e50" width={25} height={25} />, [
            getItem('Danh sách công ty', '/list-company'),
        ]),
        getItem('Cơ cấu tổ chức', '/organization', <OrganizationIcon fill="#2c3e50" width={25} height={25} />),
        getItem('Mặc định', 'setting', <DefaultSettingIcon fill="#2c3e50" width={25} height={25} />),
    ];
    return (
        <div className="flex flex-col justify-between">
            <div className="overflow-y-auto h-[calc(100vh-112px)] shadow-custom-1">
                <Menu
                    onClick={onClick}
                    defaultSelectedKeys={[`/${openSidebar.childrenPath}`]}
                    defaultOpenKeys={[`/${openSidebar.parentPath}`]}
                    mode="inline"
                    items={items}
                    inlineCollapsed={collapsed}
                />
            </div>
            <div className="p-4 absolute bottom-0 w-full border-t-[1px] bg-white z-30 shadow-custom-1">
                <div className="cursor-pointer w-[40px]" onClick={toggleCollapsed}>
                    <MenuSidebarIcon width={30} fill="#2c3e50" height={30} />
                </div>
            </div>
        </div>
    );
}

export default Sidebar;
