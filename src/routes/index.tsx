import { AccessControl } from '~/core/common/constants';
import DetailContract from '~/modules/book/contract/detail';
import ListContract from '~/modules/book/contract/list';
import BookDebit from '~/modules/book/debit';
import DetailCompany from '~/modules/company/detail';
import ListCompany from '~/modules/company/list';
import Dashboard from '~/modules/dasboard';
import ForgotPassword from '~/modules/forgot-password';
import Login from '~/modules/login';
// import HumanManager from '~/modules/manager';
import ViewCreateNews from '~/modules/news/create';
import ListNews from '~/modules/news/list';
import Organization from '~/modules/organization';
import Manager from '~/modules/personnel/manager';
import ViewCreateProduct from '~/modules/products/create';
import CreateDiscount from '~/modules/products/create-discount';
import ViewListProduct from '~/modules/products/list';
import ProductDetail from '~/modules/products/product-detail';

const publicRoutes: Array<{
    path: string;
    component: any;
    layout?: string | null;
}> = [
    { path: '/login', component: Login, layout: null },
    { path: '/forgot-password', component: ForgotPassword, layout: null },
];

const privateRoutes: Array<{
    path: string;
    component: any;
    layout?: string;
    accessControl?: AccessControl[];
}> = [
    { path: '/', component: Dashboard },
    { path: '/product/create', component: ViewCreateProduct, accessControl: [AccessControl.OWNER] },
    { path: '/product/list', component: ViewListProduct, accessControl: [AccessControl.OWNER] },
    { path: '/product/detail/:slug', component: ProductDetail, accessControl: [AccessControl.OWNER] },
    { path: '/product/create-discount', component: CreateDiscount, accessControl: [AccessControl.OWNER] },
    { path: '/company/list-company', component: ListCompany, accessControl: [AccessControl.OWNER] },
    { path: '/company/detail/:companyId', component: DetailCompany, accessControl: [AccessControl.OWNER] },
    { path: '/news/create-news', component: ViewCreateNews, accessControl: [AccessControl.OWNER] },
    { path: '/news/list-news', component: ListNews, accessControl: [AccessControl.OWNER] },
    { path: '/book/debit', component: BookDebit, accessControl: [AccessControl.OWNER] },
    { path: '/organization', component: Organization, accessControl: [AccessControl.OWNER] },
    { path: '/people/manager', component: Manager, accessControl: [AccessControl.OWNER] },
    { path: '/contract/', component: ListContract, accessControl: [AccessControl.OWNER] },
    { path: '/contract/:contractId', component: DetailContract, accessControl: [AccessControl.OWNER] },
];

export { publicRoutes, privateRoutes };
