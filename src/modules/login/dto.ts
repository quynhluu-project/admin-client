export interface LoginAttributes {
    email: string;
    password: string;
}

export interface AuthSuccessResponse {
    token: {
        accessToken: string;
        refreshTokenId: string;
    };
}
