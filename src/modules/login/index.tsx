import { useLayoutEffect } from 'react';
import { LoginLogicProvider } from './contextLogic';
import FormLogin from './components/form';
import { isAuthenticated } from '~/core/util/auth';
import { useNavigate } from 'react-router-dom';

function Login() {
    const navigate = useNavigate();
    useLayoutEffect(() => {
        const { isLogin } = isAuthenticated();
        if (isLogin) {
            navigate('/');
        }
    }, []);

    return (
        <LoginLogicProvider>
            <FormLogin />
        </LoginLogicProvider>
    );
}

export default Login;
