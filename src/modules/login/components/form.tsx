import React, { useContext, memo } from 'react';
import { ErrorMessage, Field, FormikProvider, useFormik } from 'formik';
import { Link } from 'react-router-dom';
import { LoginLogicContext } from '../contextLogic';
import { validateLogin } from '../schema';
import { Spin } from 'antd';

function FormLogin() {
    const { handleLogin, loading } = useContext(LoginLogicContext);
    const formHandler = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: validateLogin,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    return (
        <div className="flex justify-center items-center w-screen h-screen">
            <div className="w-[500px] rounded-md md:shadow-custom-4 md:px-8 py-4">
                <div className="">
                    <img src="/images/logo.png" alt="" className="w-[100px] h-[100px] mx-auto" />
                </div>
                <div className="mb-16 mt-4">
                    <h2 className="mx-auto text-3xl font-bold text-center uppercase">Đăng Nhập Admin</h2>
                </div>
                <div>
                    <FormikProvider value={formHandler}>
                        <form onSubmit={(e) => e.preventDefault()}>
                            <div className="flex flex-col px-6">
                                <div className="flex flex-col">
                                    <div className={`bg-gray-200 py-2 px-6 rounded-full border h-[50px]`}>
                                        <Field
                                            type="email"
                                            name={'email'}
                                            placeholder={'Nhập tài khoản Email'}
                                            className={'bg-transparent outline-none border-none w-full h-full'}
                                        />
                                    </div>
                                    <div className="text-red-500 p-2">
                                        <ErrorMessage name={'email'} />
                                    </div>
                                </div>
                                <div className="flex flex-col">
                                    <div className={`bg-gray-200 py-2 px-6 rounded-full border h-[50px]`}>
                                        <Field
                                            type="password"
                                            name={'password'}
                                            placeholder={'Nhập mật khẩu'}
                                            className={'bg-transparent outline-none border-none w-full h-full'}
                                        />
                                    </div>
                                    <div className="text-red-500 p-2">
                                        <ErrorMessage name={'password'} />
                                    </div>
                                </div>
                                <div className="mt-8">
                                    <button
                                        disabled={!formHandler.dirty || !formHandler.isValid}
                                        onClick={() => handleLogin(formHandler.values)}
                                        className={` ${
                                            !formHandler.dirty || !formHandler.isValid ? 'disabled' : ''
                                        } disabled:opacity-75 disabled:cursor-not-allowed outline-none border-none w-full h-full rounded-full  bg-gradient-to-r from-[#A75D5D] to-[#FFC3A1] text-2xl text-white p-3 font-medium`}
                                    >
                                        <div className="flex items-center justify-center gap-2">
                                            {loading && <Spin></Spin>} Đăng Nhập
                                        </div>
                                    </button>
                                </div>
                                <div className="text-center mt-2">
                                    <Link to={'/forgot-password'} className="text-[#A75D5D] font-medium">
                                        Quên mật khẩu?
                                    </Link>
                                </div>
                            </div>
                        </form>
                    </FormikProvider>
                </div>
                <div className="mt-16 flex items-center justify-center">
                    <p className="text-xs text-center">
                        Việc bạn tiếp tục sử dụng trang web này đồng nghĩa bạn đồng ý với <br />
                        <Link to={'/terms'} className="text-xs underline px-1">
                            Điều khoản sử dụng
                        </Link>
                        của chúng tôi
                    </p>
                </div>
            </div>
        </div>
    );
}

export default memo(FormLogin);
