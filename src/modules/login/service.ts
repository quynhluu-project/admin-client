import AxiosInstance from '~/core/util/axios';
import { LoginAttributes, AuthSuccessResponse } from './dto';
import { ConfigEnv } from '~/configs';
import { showMessageNotification } from '~/core/util/common';
class LoginDashboardService {
    constructor(readonly HTTPClient: typeof AxiosInstance) {}

    async login(payload: LoginAttributes) {
        try {
            const result = await this.HTTPClient.post(`${ConfigEnv.ADMIN_DOMAIN}/api-v1/auth/login`, payload);
            return result.data; //
        } catch (error: any) {
            showMessageNotification({
                type: 'error',
                title: `Đăng nhập thất bại`,
                description: error?.message,
            });
        }
    }

    async managerAccessDashboard(payload: LoginAttributes): Promise<AuthSuccessResponse> {
        const result = await this.HTTPClient.post(
            `${ConfigEnv.ADMIN_DOMAIN}/api-v1/auth/manager-access-dashboard`,
            payload,
        );
        return result.data; //
    }

    async ownerAccessDashboard(payload: LoginAttributes) {
        try {
            const result = await this.HTTPClient.post(
                `${ConfigEnv.ADMIN_DOMAIN}/api-v1/auth/owner-access-dashboard`,
                payload,
            );
            return result.data; //
        } catch (error: any) {
            showMessageNotification({
                type: 'error',
                title: `Đăng nhập thất bại`,
                description: error?.message,
            });
        }
    }
}

const loginDashboardService = new LoginDashboardService(AxiosInstance);

export default loginDashboardService;
