import React, { createContext, useCallback, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LoginAttributes } from './dto';
import loginDashboardService from './service';
import LocalStorage from '~/core/util/storage';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import jwt_decode from 'jwt-decode';

const LoginLogicContext = createContext({} as LoginLogicContextType);

export interface LoginLogicContextType {
    loading: boolean;
    navigate: ReturnType<typeof useNavigate>;
    handleLogin: (data: LoginAttributes) => void;
}

function LoginLogicProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const handleLogin = useCallback(async (data: LoginAttributes) => {
        try {
            setLoading(true);
            const { email } = data;
            let result;
            if (email.includes('manager') || email.includes('employee')) {
                result = await loginDashboardService.managerAccessDashboard(data);
            } else if (email.includes('owner')) {
                result = await loginDashboardService.ownerAccessDashboard(data);
            } else {
                result = await loginDashboardService.login(data);
            }
            if (result) {
                LocalStorage.set(LOCAL_STORAGE_KEY.ACCESS_TOKEN, result.token.accessToken);
                LocalStorage.set(LOCAL_STORAGE_KEY.REFRESH_TOKEN, result.token.refreshTokenId);
                const profile = jwt_decode(result.token.accessToken);
                navigate('/');
            }
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }, []);

    const valuesContext: LoginLogicContextType = useMemo(
        () => ({
            loading,
            navigate,
            handleLogin,
        }),
        [loading],
    );

    return <LoginLogicContext.Provider value={valuesContext}>{children}</LoginLogicContext.Provider>;
}

export { LoginLogicContext, LoginLogicProvider };
