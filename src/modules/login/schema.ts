import * as yup from 'yup';
import { emailRegex, passwordRegex } from '~/core/common/constants';
export const validateLogin = yup.object().shape({
    email: yup.string().matches(emailRegex, 'Tài khoản email không hợp lệ').required('Vui lòng nhập tài khoản email'),
    password: yup
        .string()
        .matches(passwordRegex, 'Mật khẩu phải chứa ít nhất 1 kí tự IN HOA, $%#.., 098... ')
        .required('Vui lòng nhập mật khẩu'),
});
