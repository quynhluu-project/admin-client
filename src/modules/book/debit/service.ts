import AxiosInstance from '~/core/util/axios';
import { ConfigEnv } from '~/configs';
import { DebitAttributesPayload } from './dto';
import { showMessageNotification } from '~/core/util/common';

class DebitService {
    static async createDebit(payload: DebitAttributesPayload): Promise<any> {
        try {
            const result = await AxiosInstance.post(`${ConfigEnv.BOOK_DOMAIN}/api-v1/debit/`, payload);
            return result.data;
        } catch (error: any) {
            showMessageNotification({
                type: 'error',
                title: `Tạo ghi nợ thất bại`,
                description: error.message,
            });
            return;
        }
    }

    static async getAllDebit(params?: any): Promise<any> {
        try {
            const result = await AxiosInstance.get(`${ConfigEnv.BOOK_DOMAIN}/api-v1/debit/`, { params });
            return result.data;
        } catch (error: any) {
            showMessageNotification({
                type: 'error',
                title: `Lấy ghi nợ thất bại`,
                description: error.message,
            });
            return {
                debits: {
                    rows: [],
                    count: 0,
                },
            };
        }
    }
}
export default DebitService;
