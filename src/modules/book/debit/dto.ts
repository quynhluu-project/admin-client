export interface DebitAttributes {
    id?: number;
    debitId: string;
    userName: string;
    addressDebit: string;
    sellerId: string;
    products: string;
    totalDebit: number;
    evaluate: EnumEvaluate;
    status: EnumDebitStatus;
    note: string;
    phone: string;
    debitDate: Date;
    paymentAppointmentDate: Date;
    createdBy: string;
    updatedBy: string;
    createdAt?: string;
    updatedAt?: string;
}

export enum EnumEvaluate {
    DANGER = 'DANGER',
    WARNING = 'WARNING',
    SUCCESS = 'SUCCESS',
}

export enum EnumDebitStatus {
    DEBITING = 'DEBITING',
    PAYED = 'PAYED',
    DESTROY = 'DESTROY',
}

export interface DebitAttributesPayload {
    userName: string;
    addressDebit: string;
    sellerId: string;
    products: string;
    totalDebit: number;
    evaluate: EnumEvaluate | string;
    note: string;
    phone: string;
    debitDate: string;
    paymentAppointmentDate: string;
}
