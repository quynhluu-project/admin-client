import React, { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import LocalStorage from '~/core/util/storage';
import { ProductAttributes } from '~/modules/products/dto';
import productService from '~/modules/products/service';
import { DebitAttributes } from './dto';
import DebitService from './service';

const BookDebitLogicContext = createContext({} as BookDebitLogicContextType);

export interface BookDebitLogicContextType {
    debits: DebitAttributes[];
    setDebits: React.Dispatch<React.SetStateAction<DebitAttributes[]>>;
    products: ProductAttributes[];
    setReCall: React.Dispatch<React.SetStateAction<boolean>>;
    navigate: ReturnType<typeof useNavigate>;
}

function BookDebitProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const { companyId } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);
    const [products, setProducts] = useState<ProductAttributes[]>([]);
    const [debits, setDebits] = useState<DebitAttributes[]>([]);
    const [reCall, setReCall] = useState(false);
    const handleGetAllProductOfOwner = useCallback(async () => {
        const {
            products: { rows },
        } = await productService.getAllProductOfCompany({ companyId });
        setProducts(rows);
    }, [companyId]);

    useEffect(() => {
        handleGetAllProductOfOwner();
    }, []);

    useEffect(() => {
        const getAllDebit = async () => {
            const {
                debits: { rows },
            } = await DebitService.getAllDebit();
            setDebits(rows);
            setReCall(false);
        };
        reCall && getAllDebit();
    }, [reCall]);

    const valuesContext: BookDebitLogicContextType = useMemo(
        () => ({
            debits,
            setDebits,
            products,
            setReCall,
            navigate,
        }),
        [debits, products, setDebits, navigate, setReCall],
    );

    return <BookDebitLogicContext.Provider value={valuesContext}>{children}</BookDebitLogicContext.Provider>;
}

export { BookDebitLogicContext, BookDebitProvider };
