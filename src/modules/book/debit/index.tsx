import React from 'react';
import { BookDebitProvider } from './contextLogic';
import ViewBookDebit from './components';

function BookDebit() {
    return (
        <BookDebitProvider>
            <ViewBookDebit />
        </BookDebitProvider>
    );
}

export default BookDebit;
