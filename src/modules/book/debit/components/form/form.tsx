/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useMemo, useState, useCallback } from 'react';
import { FormikProvider, FormikContextType } from 'formik';
import InputFiled, { InputFiledOutSite } from '~/core/common/components/input-field';
import './form.scss';
import SelectGroup from '~/core/common/components/select-group';
import { RangePickerProps } from 'antd/es/date-picker';
import dayjs from 'dayjs';
import AppDatePicker from '~/core/common/components/date-picker';
import { BookDebitLogicContext } from '../../contextLogic';
import { Card, Col, Row } from 'antd';
import Meta from 'antd/es/card/Meta';
import { TitleCartItem } from '~/layouts/components/header/components/title-cart-item';
import ProductPrice from '~/modules/products/create-discount/components/product-price';
import { DebitAttributesPayload } from '../../dto';
import DebitService from '../../service';
import { showMessageNotification } from '~/core/util/common';

type ProductCountType = {
    productId: string;
    count: string;
};

function FormCreateDebit({ formik }: { formik: FormikContextType<DebitAttributesPayload> }) {
    const { products, setReCall } = useContext(BookDebitLogicContext);
    const [productCount, setProductCount] = useState<ProductCountType[]>([]);

    const productsDebit = useMemo(() => {
        return products?.filter((product) => formik.values?.products?.includes(product.productId)) || [];
    }, [products, formik.values]);

    const productOptions = useMemo(
        () => products?.map((product) => ({ label: product.productName, value: product.productId })) || [],
        [products],
    );

    const totalDebit = useMemo(() => {
        return productsDebit.reduce((total, product, i) => {
            return (
                total +
                (product.price - (product.price / 100) * product.discount) * (Number(productCount?.[i]?.count) || 1)
            );
        }, 0);
    }, [productsDebit, productCount]);

    const disabledDate: RangePickerProps['disabledDate'] = (current) => {
        return current && current < dayjs().endOf('day');
    };

    const handleCountProductSelectChange = (e: any, productId: string) => {
        setProductCount((prev) => {
            return prev.map((item) => {
                if (productId === item.productId) {
                    return {
                        ...item,
                        count: e.target.value,
                    };
                }
                return item;
            });
        });
    };

    const handleSaveDebit = useCallback(
        async (data: DebitAttributesPayload) => {
            const products = productsDebit.map((item, i) => {
                return {
                    ...item,
                    count: productCount[i].count,
                };
            });
            data.products = JSON.stringify(products);
            data.debitDate = dayjs().toDate().toISOString();
            data.totalDebit = totalDebit;
            data.paymentAppointmentDate = dayjs(data.paymentAppointmentDate, 'DD-MM-YYYY').toDate().toISOString();

            const result = await DebitService.createDebit(data);
            setReCall(true);
            result &&
                showMessageNotification({
                    type: 'success',
                    title: 'Tạo ghi nơ thành công',
                    description: 'Ghi nợ đã được thêm vào hệ thống lưu trữ',
                });
        },
        [productsDebit, productCount, totalDebit, setReCall],
    );

    const handleSetDateAppointment = (_: any, date: string) => {
        formik.setFieldValue('paymentAppointmentDate', date);
    };
    useEffect(() => {
        setProductCount(
            productsDebit.map((product, i) => {
                const oldCount = productCount.find((ele) => ele.productId === product.productId);
                return {
                    productId: product.productId,
                    count: oldCount?.count || '1',
                };
            }),
        );
    }, [productsDebit]);

    return (
        <FormikProvider value={formik}>
            <form>
                <h2 className="text-center p-4 text-2xl font-bold uppercase"> Sổ Ghi Nợ </h2>
                <Row gutter={[16, 16]}>
                    {productsDebit?.map((product, i) => {
                        return (
                            <Col xs={24} sm={12} md={12} lg={12} xl={6} key={product.productId}>
                                <Card
                                    hoverable
                                    cover={
                                        <div className="flex justify-center items-center ">
                                            <img alt="example" src={product.imageMain} className="mx-auto" />
                                        </div>
                                    }
                                >
                                    <Meta
                                        className="mb-2"
                                        description={
                                            <>
                                                <TitleCartItem
                                                    title={product.productName}
                                                    href={`/product/${product.slug}`}
                                                />
                                                <ProductPrice price={product.price} discount={product.discount} />
                                                <InputFiledOutSite
                                                    nameField={`productCount[${i}]`}
                                                    isRequire
                                                    placeholder="Nhập số lượng"
                                                    onChange={(e: any) =>
                                                        handleCountProductSelectChange(e, product.productId)
                                                    }
                                                    value={productCount[i]?.count}
                                                />
                                            </>
                                        }
                                    />
                                </Card>
                            </Col>
                        );
                    })}
                </Row>
                <div className="flex items-center justify-start gap-4 mb-4">
                    <div className="w-full">
                        <SelectGroup
                            nameField="products"
                            isRequire
                            label="Chọn sản phẩm"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Vui lòng chọn cho sản phẩm"
                            options={productOptions}
                            mode="multiple"
                            value={formik.values?.products || []}
                        />
                    </div>
                </div>
                <div className="flex items-center justify-start gap-4 mb-4">
                    <div className="w-1/3">
                        <SelectGroup
                            nameField="evaluate"
                            isRequire
                            label="Đánh giá nợ"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Đánh giá nợ"
                            options={[
                                { label: 'Nợ Lâu', value: 'DANGER' },
                                { label: 'Nợ Mới', value: 'WARNING' },
                                { label: 'Khách Quen', value: 'SUCCESS' },
                            ]}
                            value={formik.values?.evaluate || null}
                        />
                    </div>
                    <div className="w-1/3 ">
                        <div className="flex justify-between">
                            <div className="">
                                <InputFiled
                                    nameField="phone"
                                    isRequire
                                    placeholder="Số ĐT khách hàng nợ"
                                    label="Số ĐT khách hàng"
                                />
                            </div>
                            <div className="flex items-center justify-center">
                                <AppDatePicker
                                    picker="date"
                                    format="DD-MM-YYYY"
                                    disabledDate={disabledDate}
                                    label="Thời Gian Hẹn trả"
                                    onChange={handleSetDateAppointment}
                                    value={dayjs(formik.values.paymentAppointmentDate || dayjs(), 'DD-MM-YYYY')}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="w-1/3">
                        <SelectGroup
                            nameField="sellerId"
                            isRequire
                            label="Chọn nhân viên bán"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Vui lòng chọn cho nhân viên bán"
                            options={[]}
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="userName"
                            isRequire
                            placeholder="Tên khách hàng nợ"
                            label="Tên khách hàng"
                            tutorial={<div>meo meo</div>}
                        />
                    </div>
                    <div className="flex-1">
                        <InputFiled
                            nameField="totalDebit"
                            isRequire
                            placeholder="0"
                            label="Tổng tiền nợ"
                            readOnly
                            value={totalDebit}
                        />
                    </div>
                    <div className="flex-1">
                        <InputFiled nameField="note" isRequire placeholder="Ghi chú" label="Lưu ý" />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="addressDebit"
                            isRequire
                            placeholder="Địa chỉ khách hàng"
                            label="Địa chỉ khách hàng"
                            tutorial={<div>meo meo</div>}
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1 h-[300px]"></div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <button
                        type="button"
                        className="text-white p-2 px-6 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                        onClick={() => {
                            handleSaveDebit(formik.values);
                            // formik.resetForm();
                        }}
                    >
                        Thêm ghi nợ
                    </button>
                </div>
            </form>
        </FormikProvider>
    );
}

export default FormCreateDebit;
