import { Breadcrumb } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import FormCreateDebit from './form/form';
import { useFormik } from 'formik';
import ListDebit from './list-debit';
import { DebitAttributesPayload } from '../dto';

function ViewBookDebit() {
    const formik = useFormik<DebitAttributesPayload>({
        initialValues: {
            userName: '',
            addressDebit: '',
            sellerId: '',
            products: '',
            debitDate: '',
            totalDebit: 0,
            paymentAppointmentDate: '',
            evaluate: '',
            note: '',
            phone: '',
        },
        enableReinitialize: true,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/product/create'}>Product create</Link>,
                        },
                    ]}
                />
            </div>
            <div className="bg-gray-100 p-4 mb-20">
                <div className="bg-white shadow-sm p-2">
                    <FormCreateDebit formik={formik} />
                </div>
            </div>
            <div className="bg-gray-100 p-4 mb-20">
                <div className="bg-white shadow-sm p-2">
                    <ListDebit />
                </div>
            </div>
        </section>
    );
}

export default ViewBookDebit;
