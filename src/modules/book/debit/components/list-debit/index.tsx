import React, { useContext, useMemo, useState } from 'react';
import { Badge, Card, Col, Row, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { ProductAttributes } from '~/modules/products/dto';
import Meta from 'antd/es/card/Meta';
import { TitleCartItem } from '~/layouts/components/header/components/title-cart-item';
import ProductPrice from '~/modules/products/create-discount/components/product-price';
import { BookDebitLogicContext } from '../../contextLogic';
import { convertVnd } from '~/core/util/common';
import { DebitAttributes, EnumDebitStatus } from '../../dto';
import { SearchIcon } from '~/core/common/icons/search';
import useDebounce from '~/core/hook/useDebounce';
import DebitService from '../../service';

const statusType: any = {
    DEBITING: 'warning',
    PAYED: 'success',
    DESTROY: 'error',
};
const statusText: any = {
    DEBITING: 'Đang nợ ',
    PAYED: 'Đã thanh toán',
    DESTROY: 'Đã hủy',
};

function ListDebit() {
    const { debits, setDebits } = useContext(BookDebitLogicContext);

    const [search, setSearch] = useState();
    const [count, setCount] = useState(0);
    useDebounce(search, 500, async () => {
        const params = {
            search,
        };
        const {
            debits: { rows, count },
        } = await DebitService.getAllDebit(params);
        setDebits(rows);
        setCount(count);
        return rows;
    });

    const columns: ColumnsType<DebitAttributes> = [
        {
            title: 'Tên khách hàng',
            dataIndex: 'userName',
            key: 'userName',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Tổng tiền nợ',
            key: 'totalDebit',
            render: (_, record) => {
                return <div>{convertVnd(record.totalDebit)}</div>;
            },
        },
        {
            title: 'Địa chỉ',
            key: 'addressDebit',
            render: (_, record) => {
                return <div>{record.addressDebit}</div>;
            },
        },
        {
            title: 'Trạng thái',
            key: 'status',
            render: (_, record) => {
                return <Badge status={statusType[record.status]} text={statusText[record.status]} />;
            },
        },

        {
            title: 'Action',
            key: 'action',
            render: (_, record) => {
                return record.status !== EnumDebitStatus.DESTROY ? (
                    <Space size="middle">
                        <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                            Đã Thanh Toán
                        </button>
                        <button className="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded">
                            Đã Hủy
                        </button>
                    </Space>
                ) : (
                    ''
                );
            },
        },
    ];

    const debitsData = useMemo(() => {
        return debits?.map((item) => ({ ...item, key: item.debitId })) || [];
    }, [debits]);

    return (
        <div>
            <div>
                <div className="flex flex-col justify-center items-center ">
                    <h2 className="text-2xl font-medium pb-4 text-red-500 uppercase">Danh sách khách hàng nợ</h2>
                    <div className="rounded-full border-[2px] p-2">
                        <div className="bg-white lg:w-[400px] xl:w-[450px] rounded-full hidden flex-1 lg:flex">
                            <input
                                type="text"
                                placeholder="Tên sản phẩm, tên khách hàng, tổng tiền, địa chỉ..."
                                className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                                value={search}
                                onChange={(e: any) => setSearch(e.target.value)}
                            />
                            <button className=" rounded-full px-2 py-1">
                                <SearchIcon fill="#7286D3" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table
                columns={columns}
                dataSource={debitsData}
                pagination={{
                    defaultCurrent: 1,
                    total: count,
                }}
                expandable={{
                    expandedRowRender: (record) => (
                        <Row gutter={[16, 16]}>
                            {record.products &&
                                JSON.parse(record.products).map((product: ProductAttributes) => {
                                    return (
                                        <Col xs={24} sm={12} md={12} lg={12} xl={6} key={product.productId}>
                                            <Card
                                                hoverable
                                                cover={
                                                    <div className="flex justify-center items-center ">
                                                        <img
                                                            alt="example"
                                                            src={product.imageMain}
                                                            className="mx-auto"
                                                        />
                                                    </div>
                                                }
                                            >
                                                <Meta
                                                    className="mb-2"
                                                    description={
                                                        <>
                                                            <TitleCartItem
                                                                title={product.productName}
                                                                href={`/product/${product.slug}`}
                                                            />
                                                            <div>
                                                                <span className="text-sm">
                                                                    Số lượng: {product?.count}
                                                                </span>
                                                            </div>
                                                            <div className="flex gap-2">
                                                                <span className="text-sm">Giá niêm yết:</span>
                                                                <span>
                                                                    {convertVnd(
                                                                        product.price - (product.price / 100) * 0,
                                                                    )}
                                                                </span>
                                                            </div>
                                                            <div className="flex gap-2">
                                                                <span className="text-sm">Giá mới:</span>
                                                                <span>
                                                                    {convertVnd(
                                                                        product.price -
                                                                            (product.price / 100) * product.discount,
                                                                    )}
                                                                </span>
                                                            </div>
                                                            <div className="flex gap-2">
                                                                <span className="text-sm">Total:</span>
                                                                <span>
                                                                    {convertVnd(
                                                                        (product.price -
                                                                            (product.price / 100) * product.discount) *
                                                                            Number(product?.count || 1),
                                                                    )}
                                                                </span>
                                                            </div>
                                                        </>
                                                    }
                                                />
                                            </Card>
                                        </Col>
                                    );
                                })}
                        </Row>
                    ),
                    rowExpandable: (record) => record.products.length > 0,
                }}
            />
        </div>
    );
}

export default ListDebit;
