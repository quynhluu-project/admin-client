import AxiosInstance from '~/core/util/axios';
import { ConfigEnv } from '~/configs';
import { ParamsSearchDTO } from './dto';

class ContractService {
    static async createContract(payload: any) {
        const data = await AxiosInstance.post(`${ConfigEnv.BOOK_DOMAIN}/api-v1/contract/`, payload);
        return data.data;
    }

    static async getDetailContract(contractId: string) {
        const data = await AxiosInstance.get(`${ConfigEnv.BOOK_DOMAIN}/api-v1/contract/`, {
            params: {
                contractId,
            },
        });
        return data.data;
    }

    static async getAllContract(params?: ParamsSearchDTO) {
        const data = await AxiosInstance.get(`${ConfigEnv.BOOK_DOMAIN}/api-v1/contract/search`, {
            params: {
                ...params,
            },
        });
        return data.data;
    }
}
export default ContractService;
