export interface ParamsSearchDTO {
    limit?: number;
    companyId?: string;
    pageIndex?: number;
    contractType?: 'IMPORT' | 'EXPORT';
    search?: string;
    sortType?: string;
    sortColumn?: string;
}

export interface ContractAttributes {
    id?: number;
    contractId?: string;
    companyIdRequest?: string;
    companyIdResponse?: string;
    period?: string;
    detailInfor?: string;
    signatureRequest?: string;
    signatureResponse?: string;
    statusRequest?: boolean;
    statusResponse?: boolean;
    createdBy?: string;
    updatedBy?: string;
    emailLegalRepresentativeRequest?: string;
    emailLegalRepresentativeResponse?: string;
    statusActive?: EnumContractStatus;
    fileUrl?: string;
    createdAt?: string;
    updatedAt?: string;
}

export enum EnumContractStatus {
    ACTIVE = 'ACTIVE',
    PENDING = 'PENDING',
    WAITING_APPROVAL = 'WAITING_APPROVAL',
}
