import { Col, Row } from 'antd';
import React, { useContext } from 'react';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { ListContractLogicContext } from '../../contextLogic';
import PaginationApp from '~/core/common/components/pagination';
import { ContractAttributes } from '../../../dto';

const columns: ColumnsType<ContractAttributes> = [
    { title: 'Người tạo', dataIndex: 'emailLegalRepresentativeRequest', key: 'name' },
    { title: 'Age', dataIndex: 'age', key: 'age' },
    { title: 'Address', dataIndex: 'address', key: 'address' },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: () => <a>Delete</a>,
    },
];

function ListContractImport() {
    const { contracts, total, setFilterParams, filterParams } = useContext(ListContractLogicContext);
    return (
        <div className="bg-white p-4">
            <Table
                columns={columns}
                expandable={{
                    expandedRowRender: (record) => (
                        <p style={{ margin: 0 }}>{record.emailLegalRepresentativeResponse}</p>
                    ),
                    // rowExpandable: (record) => record.name !== 'Not Expandable',
                }}
                dataSource={contracts || []}
                pagination={false}
            />
            <div className="my-2 flex items-center justify-end">
                <PaginationApp
                    limit={filterParams.limit}
                    total={total}
                    onChange={(pageIndex, page) => setFilterParams((prev) => ({ ...prev, pageIndex, limit: page }))}
                />
            </div>
        </div>
    );
}

export default ListContractImport;
