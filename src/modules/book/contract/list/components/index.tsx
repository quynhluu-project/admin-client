import { Breadcrumb, Tabs } from 'antd';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import ListContractImport from './import';
import ListContractExport from './export';
import { ListCompanyLogicContext } from '~/modules/company/list/contextLogic';
import { ListContractLogicContext } from '../contextLogic';

function ViewListContract() {
    const { setTabActive, tabActive } = useContext(ListContractLogicContext);
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/contract'}>Hợp đồng (danh sách)</Link>,
                        },
                    ]}
                />
            </div>
            <div className="bg-gray-100 p-4">
                <Tabs
                    activeKey={tabActive}
                    type="card"
                    onChange={(active) => setTabActive(active)}
                    items={[
                        {
                            label: `Hợp Đồng Nhập Hàng`,
                            key: 'IMPORT',
                            children: <ListContractImport />,
                        },
                        {
                            label: `Hợp Đồng Xuất Hàng`,
                            key: 'EXPORT',
                            children: <ListContractExport />,
                        },
                    ]}
                />
            </div>
        </section>
    );
}

export default ViewListContract;
