import React, { createContext, useMemo, useState, useCallback, useEffect } from 'react';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';
import ContractService from '../service';
import { ContractAttributes, ParamsSearchDTO } from '../dto';
import LocalStorage from '~/core/util/storage';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import { isNil, omitBy } from 'lodash';

const ListContractLogicContext = createContext({} as ListContractLogicContextType);

export interface ListContractLogicContextType {
    total: number;
    setTotal: React.Dispatch<React.SetStateAction<number>>;
    contracts: ContractAttributes[];
    setContracts: React.Dispatch<React.SetStateAction<ContractAttributes[]>>;
    filterParams: ParamsSearchDTO;
    setFilterParams: React.Dispatch<React.SetStateAction<ParamsSearchDTO>>;
    lodingGetContract: boolean;
    tabActive: string;
    setTabActive: React.Dispatch<React.SetStateAction<string>>;
}

function ListContractProvider({ children }: { children: React.ReactNode }) {
    const { request: getAllContract, isLoading: lodingGetContract } = usePrepareExecuteApi({
        apiWillExecute: ContractService.getAllContract,
        isUseServerMes: true,
    });
    const { branchId } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);

    const [total, setTotal] = useState<number>(0);
    const [tabActive, setTabActive] = useState<any>('IMPORT');
    const [contracts, setContracts] = useState<ContractAttributes[]>([]);
    const [filterParams, setFilterParams] = useState<ParamsSearchDTO>({
        limit: 20,
        pageIndex: 1,
        sortColumn: 'createdAt',
        sortType: 'DESC',
        companyId: branchId,
        contractType: tabActive,
        search: undefined,
    } as ParamsSearchDTO);

    useEffect(() => {
        const fetchAllContract = async () => {
            const prepareParams = omitBy({ ...filterParams }, isNil);
            const data = await getAllContract(prepareParams);
            setContracts(data?.contracts?.rows || []);
            setTotal(data?.contracts?.count);
        };
        fetchAllContract();
    }, [filterParams]);

    useEffect(() => {
        setFilterParams((prev) => ({ ...prev, contractType: tabActive }));
    }, [tabActive]);

    const valuesContext: ListContractLogicContextType = useMemo(
        () => ({
            total,
            setTotal,
            tabActive,
            setTabActive,
            contracts,
            setContracts,
            filterParams,
            setFilterParams,
            lodingGetContract,
        }),
        [
            total,
            setTotal,
            tabActive,
            setTabActive,
            contracts,
            setContracts,
            filterParams,
            setFilterParams,
            lodingGetContract,
        ],
    );

    return <ListContractLogicContext.Provider value={valuesContext}>{children}</ListContractLogicContext.Provider>;
}

export { ListContractLogicContext, ListContractProvider };
