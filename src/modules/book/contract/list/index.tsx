import React from 'react';
import { ListContractProvider } from './contextLogic';
import ViewListContract from './components';
import './style.scss';

function ListContract() {
    return (
        <ListContractProvider>
            <ViewListContract />
        </ListContractProvider>
    );
}

export default ListContract;
