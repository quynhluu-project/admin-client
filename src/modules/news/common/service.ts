import AxiosInstance from '~/core/util/axios';

import { showMessageNotification } from '~/core/util/common';
import { ConfigEnv } from '~/configs';
import { NewsAttributes } from './dto';

class NewsService {
    constructor(readonly HTTPClient: typeof AxiosInstance) {}

    async createNews(payload: any): Promise<{ isUpdated: boolean }> {
        const result = await AxiosInstance.post(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/news`, payload);
        return result.data;
    }

    async updateNews(payload: any) {
        const result = await AxiosInstance.put(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/news`, payload);
        return result.data;
    }

    async getAllNews(params: any): Promise<{ news: { rows: NewsAttributes[]; count: number } }> {
        const result = await AxiosInstance.get(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/news/all`, { params });
        return result.data;
    }

    async getNewsBySlug(slug: string) {
        try {
            const result = await this.HTTPClient.get(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/news/slug/${slug}`);
            return {
                news: result?.data?.news,
            };
        } catch (error) {
            return {
                news: {} as NewsAttributes,
            };
        }
    }
}

const newsService = new NewsService(AxiosInstance);

export default newsService;
