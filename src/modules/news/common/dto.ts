export interface PayloadNewsAttribute {
    title: string;
    newsCategory: string;
    descriptionMain: string;
    subDescription: SubDescription[];
    bannerMain: string;
    isOfMonth: boolean;
}

export type SubDescription = {
    title: string;
    metadata: string;
    alert: string;
    alertType: string;
};

export interface NewsAttributes {
    id: number;
    newsId: string;
    title: string;
    slug: string;
    newsCategory: string;
    view: number;
    bannerMain: string;
    descriptionMain: string;
    subDescription: string;
    isOfMonth: boolean;
    createdBy: string;
    updatedBy: string;
    createdAt: string;
    updatedAt: string;
}
export type NewsSortBy = 'newsId' | 'updatedAt' | 'createdAt';
export type NewsSortType = 'DESC' | 'ASC';
export interface ParamsSearchDTO {
    limit?: number;
    newsId?: string;
    category?: string;
    pageIndex?: number;
    search?: string;
    sortBy?: string;
    sortType?: string;
}
