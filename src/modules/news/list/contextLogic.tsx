import React, { createContext, useMemo, useState, useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { NewsAttributes, ParamsSearchDTO } from '../common/dto';
import newsService from '../common/service';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';

const ListNewsLogicContext = createContext({} as ListNewsLogicContextType);

export interface ListNewsLogicContextType {
    news: NewsAttributes[];
    setNews: React.Dispatch<React.SetStateAction<NewsAttributes[]>>;
    navigate: ReturnType<typeof useNavigate>;
    totalNews: number;
    filterParams: ParamsSearchDTO;
    setFilterParams: React.Dispatch<React.SetStateAction<ParamsSearchDTO>>;
    loadingGetNews: boolean;
}

function ListNewsProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const [news, setNews] = useState<NewsAttributes[]>([]);
    const [totalNews, setTotalNews] = useState<number>(0);
    const [filterParams, setFilterParams] = useState<ParamsSearchDTO>({
        limit: 16,
        pageIndex: 1,
        sortBy: 'createdAt',
        sortType: 'DESC',
    } as ParamsSearchDTO);

    const { request: getAllNews, isLoading: loadingGetNews } = usePrepareExecuteApi<any, any>({
        apiWillExecute: newsService.getAllNews,
    });
    const fetchAllNews = useCallback(async () => {
        const data = await getAllNews({ ...filterParams });
        setNews(data?.news?.rows || []);
        setTotalNews(data?.news?.count || 0);
    }, [filterParams]);

    useEffect(() => {
        fetchAllNews();
    }, [filterParams, fetchAllNews]);

    const valuesContext: ListNewsLogicContextType = useMemo(
        () => ({
            news,
            setNews,
            navigate,
            totalNews,
            filterParams,
            setFilterParams,
            loadingGetNews,
        }),
        [news, setNews, navigate, totalNews, filterParams, setFilterParams, loadingGetNews],
    );

    return <ListNewsLogicContext.Provider value={valuesContext}>{children}</ListNewsLogicContext.Provider>;
}

export { ListNewsLogicContext, ListNewsProvider };
