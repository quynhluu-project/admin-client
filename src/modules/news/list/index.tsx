import React, { useState } from 'react';

import './list.scss';
import { ListNewsProvider } from './contextLogic';
import ViewListNews from './components/list';
function ListNews() {
    return (
        <ListNewsProvider>
            <ViewListNews />
        </ListNewsProvider>
    );
}

export default ListNews;
