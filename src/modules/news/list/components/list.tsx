import React, { useContext, useState } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import { SearchIcon } from '~/core/common/icons/search';
import { Card, Col, Row, Divider, Space, Select } from 'antd';
import { ListNewsLogicContext } from '../contextLogic';
import { FromNow } from '~/core/util/date';
import useDebounce from '~/core/hook/useDebounce';
import PaginationApp from '~/core/common/components/pagination';
import AppLoading from '~/core/common/components/app-loading';
function ViewListNews() {
    const { Meta } = Card;
    const { news, setFilterParams, totalNews, filterParams, loadingGetNews } = useContext(ListNewsLogicContext);
    const [search, setSearch] = useState<string>();
    useDebounce(search, 500, () => {
        if (search !== undefined) {
            setFilterParams({ ...filterParams, search: search });
        }
    });
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/product/list'}>News (list)</Link>,
                        },
                    ]}
                />
                <div className="pt-4">
                    <h2 className="text-xl font-medium pb-4">Tìm kiếm bài viết: </h2>
                    <div className="flex flex-col justify-center items-center">
                        <div className="rounded-full border-[2px] p-2">
                            <div className="bg-white lg:w-[400px] xl:w-[450px] rounded-full hidden flex-1 lg:flex">
                                <input
                                    type="text"
                                    placeholder="Tên bài viết..."
                                    className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                                    onChange={(e) => setSearch(e.target.value)}
                                />
                                <button className=" rounded-full px-2 py-1">
                                    <SearchIcon fill="#7286D3" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <div className="bg-white shadow-sm p-2">
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="flex items-center mt-2 mx-2 text-sm">
                            <div className="mr-4">Sắp xếp Theo: </div>
                            <div>
                                <Select
                                    defaultValue="createdAt"
                                    style={{ width: 160 }}
                                    onChange={(data) => {
                                        setFilterParams((prev) => ({ ...prev, sortBy: data }));
                                    }}
                                    options={[
                                        { value: 'newsId', label: 'Mã định danh' },
                                        { value: 'createdAt', label: 'Thời gian tạo' },
                                        { value: 'updatedAt', label: 'Thời gian chỉnh sửa' },
                                    ]}
                                />
                            </div>
                        </div>
                        <div className="flex items-center mt-2 mx-2 text-sm">
                            <div className="mr-4">Kiểu sắp xếp: </div>
                            <div>
                                <Select
                                    defaultValue="DESC"
                                    style={{ width: 160 }}
                                    onChange={(data) => {
                                        setFilterParams((prev) => ({ ...prev, sortType: data }));
                                    }}
                                    options={[
                                        { value: 'ASC', label: 'Tăng dần' },
                                        { value: 'DESC', label: 'Giảm dần' },
                                    ]}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="p-4 flex items-center justify-center">
                <PaginationApp
                    limit={filterParams.limit}
                    total={totalNews}
                    defaultCurrent={filterParams.pageIndex}
                    onChange={(page, pageSize) => {
                        setFilterParams({
                            ...filterParams,
                            limit: pageSize,
                            pageIndex: page,
                        });
                    }}
                />
            </div>
            <div className="bg-gray-100 p-4">
                {loadingGetNews ? (
                    <AppLoading />
                ) : (
                    <Row gutter={[16, 16]}>
                        {news?.map((newsItem) => {
                            return (
                                <Col span={6} key={newsItem.newsId}>
                                    <Card
                                        hoverable
                                        cover={
                                            <Link to={`/news/create-news?slug=${newsItem.slug}`}>
                                                <div className="flex justify-center items-center">
                                                    <img alt="example" src={newsItem.bannerMain} />
                                                </div>
                                            </Link>
                                        }
                                    >
                                        <Meta
                                            className="mb-2"
                                            title={newsItem?.title}
                                            description={
                                                <div>
                                                    <p
                                                        className="text-gray-800 font-normal text-base line-clamp-3"
                                                        dangerouslySetInnerHTML={{ __html: newsItem.descriptionMain }}
                                                    ></p>
                                                    <div className="flex justify-between items-center mt-2">
                                                        <div className=""> {FromNow(newsItem.createdAt)}</div>
                                                    </div>
                                                </div>
                                            }
                                        />
                                    </Card>
                                </Col>
                            );
                        })}
                    </Row>
                )}
            </div>
        </section>
    );
}

export default ViewListNews;
