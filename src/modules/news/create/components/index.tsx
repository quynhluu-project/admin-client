import React, { useContext } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import FormCreateNews from './form/form';
import { NewsLogicContext } from '../contextLogic';
import { useFormik } from 'formik';
import { PayloadNewsAttribute } from '../../common/dto';
import ButtonApply from '~/core/common/components/button/apply';

function CreateNewsComponent() {
    const {
        handleCreateNews,
        handleUpdateNews,
        loadingUpdateNews,
        detailNews,
        loadingCreateNews,
        searchQuery,
        navigate,
    } = useContext(NewsLogicContext);
    const formik = useFormik<PayloadNewsAttribute>({
        initialValues: {
            title: detailNews?.title || '',
            newsCategory: detailNews?.newsCategory || '',
            bannerMain: detailNews?.bannerMain || '',
            descriptionMain: detailNews?.descriptionMain || '',
            subDescription: JSON.parse(detailNews?.subDescription || '[]'),
            isOfMonth: detailNews?.isOfMonth || false,
        },
        enableReinitialize: true,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/news/create-news'}>News create</Link>,
                        },
                    ]}
                />
                <div className="pt-4">
                    <h2 className="text-xl font-medium pb-4">Biểu mẫu thêm tin tức vào hệ thống</h2>
                </div>
            </div>
            <div className="bg-gray-100 p-4 mb-20">
                <div className="bg-white shadow-sm p-2">
                    <FormCreateNews formik={formik} />
                </div>
            </div>
            <div className="bg-white fixed w-[calc(100%-256px)] bottom-0 z-[100] shadow-custom-4">
                <div className="p-[7px] flex items-end justify-end gap-4">
                    <ButtonApply
                        onClick={() => {
                            !!searchQuery.get('slug')
                                ? handleUpdateNews(formik.values, () => {})
                                : handleCreateNews(formik.values, (isSuccess: any) => {
                                      !!isSuccess && formik.resetForm();
                                  });
                        }}
                        isDisabled={!formik.dirty}
                        isLoading={(!searchQuery.get('slug') && loadingCreateNews) || loadingUpdateNews}
                        icon={
                            <svg
                                width="24px"
                                height="24px"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M13 3H7C5.89543 3 5 3.89543 5 5V10M13 3L19 9M13 3V8C13 8.55228 13.4477 9 14 9H19M19 9V19C19 20.1046 18.1046 21 17 21H10C7.79086 21 6 19.2091 6 17V17C6 14.7909 7.79086 13 10 13H13M13 13L10 10M13 13L10 16"
                                    stroke="#000000"
                                    strokeWidth="2"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                        }
                        label={!!searchQuery.get('slug') ? 'Cập nhật tin tức' : 'Thêm tin tức'}
                    />
                    {!!searchQuery.get('slug') && (
                        <ButtonApply
                            onClick={() => {
                                handleCreateNews(formik.values, (data: any) => {
                                    navigate(`/news/create-news?slug=${data?.news?.slug}`);
                                });
                            }}
                            isDisabled={!formik.dirty}
                            isLoading={loadingCreateNews}
                            icon={
                                <svg
                                    fill="#000000"
                                    width="32px"
                                    height="32px"
                                    viewBox="0 0 32 32"
                                    version="1.1"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <title>copy</title>
                                    <path d="M30.731 8.16c-0.005-0.035-0.011-0.066-0.019-0.095l0.001 0.005c-0.031-0.134-0.094-0.249-0.182-0.342l0 0-6.297-6.296c-0.093-0.087-0.208-0.15-0.336-0.181l-0.005-0.001c-0.026-0.008-0.058-0.014-0.091-0.019l-0.004-0c-0.026-0.007-0.059-0.014-0.092-0.018l-0.004-0h-9.445c-0.414 0-0.75 0.336-0.75 0.75v0 22.038c0 0.414 0.336 0.75 0.75 0.75h15.742c0.414-0 0.75-0.336 0.75-0.75v0-15.742c-0.005-0.038-0.012-0.071-0.020-0.103l0.001 0.005zM24.453 3.773l3.736 3.735h-3.736zM15.008 23.25v-20.538h7.945v5.546c0 0.414 0.336 0.75 0.75 0.75h5.547v14.242zM17.742 27.25c-0.414 0-0.75 0.336-0.75 0.75v0 1.27h-14.242v-20.539h7.25c0.414 0 0.75-0.336 0.75-0.75s-0.336-0.75-0.75-0.75v0h-8c-0.414 0-0.75 0.336-0.75 0.75v0 22.039c0 0.414 0.336 0.75 0.75 0.75h15.742c0.414-0 0.75-0.336 0.75-0.75v0-2.020c-0-0.414-0.336-0.75-0.75-0.75v0z"></path>
                                </svg>
                            }
                            label={'Sao chép tin tức'}
                        />
                    )}
                </div>
            </div>
        </section>
    );
}

export default CreateNewsComponent;
