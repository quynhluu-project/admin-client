import React, { useContext } from 'react';
import { FormikProvider, FieldArray, FormikContextType } from 'formik';
import InputFiled, { TypeInput } from '~/core/common/components/input-field';
import './form.scss';
import SelectGroup from '~/core/common/components/select-group';
import { NewsLogicContext } from '../../contextLogic';
import { PayloadNewsAttribute, SubDescription } from '../../../common/dto';
import TinyEditor, { EditorApp } from '~/core/common/components/editor';
import CheckboxApp from '~/core/common/components/checkbox';

function FormCreateNews({ formik }: { formik: FormikContextType<PayloadNewsAttribute> }) {
    const { optionCategories } = useContext(NewsLogicContext);

    return (
        <FormikProvider value={formik}>
            <form>
                <div className="flex items-center justify-start gap-4 mb-4">
                    <div className="w-1/3">
                        <SelectGroup
                            nameField="newsCategory"
                            isRequire
                            label="Chọn danh mục tin tức"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Vui lòng chọn danh mục cho tin tức"
                            options={optionCategories}
                            value={formik.values.newsCategory}
                        />
                    </div>
                    {formik.values.newsCategory === 'gia-gas-moi-nhat-hom-nay' && (
                        <div className="w-1/3">
                            <CheckboxApp
                                label={'Bài viết của tháng'}
                                checked={formik.values.isOfMonth || false}
                                onChange={(e: any) => formik.setFieldValue('isOfMonth', e.target.checked)}
                            />
                        </div>
                    )}
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="title"
                            isRequire
                            placeholder="Vui lòng nhập tiêu đề tin tức!"
                            label="Tiêu đề"
                            tutorial={<div>meo meo</div>}
                            value={formik.values.title}
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="bannerMain"
                            isRequire
                            placeholder="Vui lòng nhập đường dẫn ảnh tin tức!"
                            label="Link ảnh tin tức (ảnh chính)"
                            tutorial={<div>Nhập đường dẫn đến ảnh mặc định của bạn</div>}
                            type={TypeInput.TEXTAREA}
                        />
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <div className="w-full">
                            <img
                                src={formik.values.bannerMain.trim() || '/images/default-img.jpg'}
                                alt="Ảnh sản phẩm"
                                className="w-full h-[400px] object-contain"
                            />
                        </div>
                    </div>
                </div>
                {formik.values.newsCategory && (
                    <EditorApp
                        onSetContent={(data) => formik.setFieldValue(`descriptionMain`, data)}
                        label="Mô tả chi tiết tiêu đề"
                        isRequire
                        height={300}
                        initialValue={formik.values.descriptionMain}
                    ></EditorApp>
                )}
                <div className="flex items-start justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <div className="w-full">
                            <FieldArray name="subDescription">
                                {(arrayHelpers: any) => (
                                    <div className="w-full">
                                        {formik.values.subDescription.map((item: SubDescription, index: number) => (
                                            <div
                                                key={index}
                                                className="flex items-start gap-4 p-1 border rounded-md mt-4"
                                            >
                                                <div className="w-full">
                                                    <div className="flex gap-4 items-center relative z-30">
                                                        <EditorApp
                                                            onSetContent={(data) =>
                                                                formik.setFieldValue(
                                                                    `subDescription.${index}.title`,
                                                                    data,
                                                                )
                                                            }
                                                            height={220}
                                                            initialValue={formik.values.subDescription[index]?.title}
                                                        />
                                                        <div className="w-[160px] flex items-center justify-center">
                                                            <button
                                                                type="button"
                                                                className="text-white p-2 bg-red-500 hover:bg-red-600 active:bg-red-700 focus:outline-none focus:ring focus:ring-red-300 rounded-md"
                                                                onClick={() => arrayHelpers.remove(index)}
                                                            >
                                                                Xóa tin tức
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div className="flex gap-4 py-2 flex-col border rounded-md my-2 relative z-20">
                                                        <label htmlFor="" className="ml-2">
                                                            Chi tiết mô tả tin tức {index}
                                                        </label>
                                                        <EditorApp
                                                            onSetContent={(data) =>
                                                                formik.setFieldValue(
                                                                    `subDescription.${index}.metadata`,
                                                                    data,
                                                                )
                                                            }
                                                            height={320}
                                                            initialValue={formik.values.subDescription[index]?.metadata}
                                                        />
                                                    </div>
                                                    <div className="flex gap-4  py-2 flex-col border rounded-md relative z-10">
                                                        <label htmlFor="" className="ml-2">
                                                            Chi tiết thông báo {index}
                                                        </label>
                                                        <EditorApp
                                                            onSetContent={(data) =>
                                                                formik.setFieldValue(
                                                                    `subDescription.${index}.alert`,
                                                                    data,
                                                                )
                                                            }
                                                            height={320}
                                                            initialValue={formik.values.subDescription[index]?.alert}
                                                        />
                                                    </div>
                                                    <SelectGroup
                                                        nameField={`subDescription.${index}.alertType`}
                                                        isRequire
                                                        label="Chọn kiểu thông báo"
                                                        tutorial={<div>meo meo</div>}
                                                        className="w-full"
                                                        setFieldValue={formik.setFieldValue}
                                                        placeholder="Vui lòng chọn danh mục cho tin tức"
                                                        value={formik.values.subDescription[index]?.alertType || null}
                                                        options={[
                                                            { label: 'Cảnh báo', value: 'warning' },
                                                            {
                                                                label: 'Thành công',
                                                                value: 'success',
                                                            },
                                                            { label: 'Thông tin', value: 'infor' },
                                                            { label: 'Nghiêm Trọng', value: 'error' },
                                                        ]}
                                                    />
                                                </div>
                                            </div>
                                        ))}
                                        <button
                                            type="button"
                                            className="text-white p-2 mt-4 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                                            onClick={() =>
                                                arrayHelpers.push({ title: '', metadata: '', alert: '', alertType: '' })
                                            }
                                        >
                                            Thêm mô tả tin tức
                                        </button>
                                    </div>
                                )}
                            </FieldArray>
                        </div>
                    </div>
                </div>
            </form>
        </FormikProvider>
    );
}

export default FormCreateNews;
