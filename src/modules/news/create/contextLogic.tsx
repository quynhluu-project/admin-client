import React, { createContext, useCallback, useEffect, useMemo, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { NewsAttributes, PayloadNewsAttribute } from '../common/dto';
import newsService from '../common/service';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';

export interface NewsContextType {
    navigate: any;
    optionCategories: any;
    handleCreateNews: (payload: PayloadNewsAttribute, callback: (data: any) => any) => void;
    handleUpdateNews: (payload: PayloadNewsAttribute, callback: (data: any) => any) => void;
    loadingUpdateNews: boolean;
    detailNews: NewsAttributes;
    setDetailNews: React.Dispatch<React.SetStateAction<NewsAttributes>>;
    loadingCreateNews: boolean;
    searchQuery: URLSearchParams;
}

const NewsLogicContext = createContext({} as NewsContextType);

function NewsLogicProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const location = useLocation();
    const searchQuery = new URLSearchParams(location.search);
    const slug = searchQuery.get('slug');

    const [detailNews, setDetailNews] = useState<NewsAttributes>({} as NewsAttributes);
    const { request: createNews, isLoading: loadingCreateNews } = usePrepareExecuteApi({
        apiWillExecute: newsService.createNews,
        failMes: 'Tạo mới tin tức thất bại!',
        successMes: 'Tạo mới tin tức thành công',
    });

    const { request: updateNews, isLoading: loadingUpdateNews } = usePrepareExecuteApi({
        apiWillExecute: newsService.updateNews,
        failMes: 'Cập nhật tin tức thất bại!',
        successMes: 'Cập nhật tin tức thành công',
    });

    const optionCategories = useMemo(
        () => [
            { label: 'Cẩm Nang An Toàn', value: 'cam-nang-an-toan' },
            { label: 'Giá Gas Mới Nhất Hôm Nay', value: 'gia-gas-moi-nhat-hom-nay' },
            { label: 'Hệ Thống Cửa hàng', value: 'he-thong-cua-hang' },
            { label: 'Hỗ Trợ Khách Hàng', value: 'ho-tro-khach-hang' },
            { label: 'Món Ngon Mỗi Ngày', value: 'mon-ngon-moi-ngay' },
            { label: 'Phong Thủy Nhà Bếp', value: 'phong-thuy-nha-bep' },
            { label: 'Tin Tức Khuyến Mãi', value: 'tin-tuc-khuyen-mai' },
            { label: 'Tin Tức Mẹo Hay', value: 'tin-tuc-meo-hay' },
            { label: 'Tin Tức Thị Trường Gas', value: 'tin-tuc-thi-truong-gas' },
            { label: 'Tuyển dụng', value: 'tuyen-dung' },
        ],
        [],
    );

    const handleCreateNews = useCallback(async (payload: PayloadNewsAttribute, callback: (data: any) => any) => {
        const preparePayload = {
            ...payload,
            subDescription: JSON.stringify(payload.subDescription),
        };
        const data = await createNews(preparePayload);
        callback(data);
    }, []);

    const handleUpdateNews = useCallback(
        async (payload: PayloadNewsAttribute, callback: (data: any) => any) => {
            const preparePayload = {
                ...payload,
                subDescription: JSON.stringify(payload.subDescription),
                slug: slug,
            };
            const data = await updateNews(preparePayload);
            callback(data);
        },
        [slug],
    );

    useEffect(() => {
        const getDetailNews = async () => {
            if (slug) {
                const { news } = await newsService.getNewsBySlug(slug || '');
                setDetailNews(news?.detail);
            }
        };
        getDetailNews();
    }, [setDetailNews, slug]);

    const valuesContext: NewsContextType = useMemo(
        () => ({
            optionCategories,
            detailNews,
            setDetailNews,
            navigate,
            handleCreateNews,
            loadingCreateNews,
            searchQuery,
            handleUpdateNews,
            loadingUpdateNews,
        }),
        [
            optionCategories,
            handleCreateNews,
            detailNews,
            setDetailNews,
            navigate,
            loadingCreateNews,
            searchQuery,
            handleUpdateNews,
            loadingUpdateNews,
        ],
    );

    return <NewsLogicContext.Provider value={valuesContext}>{children}</NewsLogicContext.Provider>;
}

export { NewsLogicContext, NewsLogicProvider };
