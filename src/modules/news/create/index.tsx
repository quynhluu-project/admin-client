import React from 'react';

import { NewsLogicProvider } from './contextLogic';
import CreateNewsComponent from './components';

function ViewCreateNews() {
    return (
        <NewsLogicProvider>
            <CreateNewsComponent />
        </NewsLogicProvider>
    );
}

export default ViewCreateNews;
