import React, { useContext, useState } from 'react';
import { OrganizationLogicContext } from '../contextLogic';
import { Popover, Drawer } from 'antd';
import dayjs from 'dayjs';
import { TYPE_DRAWER } from '~/core/common/constants';
import DrawerCreateBranch from './drawer/create-branch';
import DrawerCreateDepartment from './drawer/create-department';

function ViewOrganization() {
    const { organizationChart, setOpenDrawer, openDrawer, setDataCreateDepartment, dataCreateDepartment } =
        useContext(OrganizationLogicContext);

    const organizations = {
        level: 0,
        name: null,
        label: null,
        organizations: organizationChart?.organizations || [],
    };

    const renderNodePopover = (level = 0, data: any): JSX.Element => {
        if (level === 1 || level === 2) {
            return (
                <div className="p-2">
                    {!data.organizationId && (
                        <>
                            <div className="flex items-center gap-4">
                                <span className="font-medium">Địa Chỉ:</span>{' '}
                                <span className="text-sm">{data.address}</span>
                            </div>
                            <div className="flex items-center gap-4">
                                <span className="font-medium">Khu vực kinh doanh:</span>
                                <span className="text-sm">{data.businessArea}</span>
                            </div>
                            <div className="flex items-center gap-4">
                                <span className="font-medium">Tên công ty:</span>
                                <span className="text-sm">{data.companyName}</span>
                            </div>
                            <div className="flex items-center gap-4">
                                <span className="font-medium">Người đại diện:</span>
                                <span className="text-sm">{data.managerRootName}</span>
                            </div>
                            <div className="flex items-center gap-4">
                                <span className="font-medium">Địa chỉ Email:</span>
                                <span className="text-sm">{data.emailRoot}</span>
                            </div>
                            <div className="flex items-center gap-4">
                                <span className="font-medium">Thời gian thành lập:</span>
                                <span className="text-sm">
                                    {dayjs(new Date(data.establishmentTime as string)).format('DD/MM/YYYY')}
                                </span>
                            </div>
                        </>
                    )}
                    <div className="h-[1px] bg-slate-200"></div>
                    <div className="flex items-end justify-end mt-2">
                        {level === 1 ? (
                            <div className="flex gap-4">
                                <button
                                    onClick={() =>
                                        setOpenDrawer({
                                            ...openDrawer,
                                            isOpen: true,
                                            type: TYPE_DRAWER.CREATE_BRANCH,
                                            drawerName: 'Biểu mẫu tạo chi nhánh mới',
                                        })
                                    }
                                    className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
                                >
                                    Tạo chi nhánh
                                </button>
                                {/* <button
                                    onClick={() => {
                                        setOpenDrawer({
                                            ...openDrawer,
                                            isOpen: true,
                                            type: TYPE_DRAWER.CREATE_DEPARTMENT,
                                            drawerName: 'Biểu mẫu tạo bộ phận',
                                        });
                                        setDataCreateDepartment({
                                            companyId: data.companyId || '',
                                            level: level + 1,
                                            organizationParentId: data.organizationId,
                                        });
                                    }}
                                    className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
                                >
                                    Tạo phòng ban
                                </button> */}
                            </div>
                        ) : (
                            <button
                                onClick={() => {
                                    setOpenDrawer({
                                        ...openDrawer,
                                        isOpen: true,
                                        type: TYPE_DRAWER.CREATE_DEPARTMENT,
                                        drawerName: 'Biểu mẫu tạo bộ phận',
                                    });
                                    setDataCreateDepartment({
                                        companyId: data.companyId || '',
                                        level: level + 1,
                                        organizationParentId: data.organizationId,
                                    });
                                }}
                                className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
                            >
                                Tạo phòng ban
                            </button>
                        )}
                    </div>
                </div>
            );
        }

        if (level === 3 && data.organizationId) {
            return (
                <div>
                    <button
                        onClick={() => {
                            setOpenDrawer({
                                ...openDrawer,
                                isOpen: true,
                                type: TYPE_DRAWER.CREATE_DEPARTMENT,
                                drawerName: 'Biểu mẫu tạo bộ phận',
                            });
                            setDataCreateDepartment({
                                companyId: data.companyId || '',
                                level: level + 1,
                                organizationParentId: data.organizationId,
                            });
                        }}
                        className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
                    >
                        Tạo phòng ban
                    </button>
                </div>
            );
        }

        return <></>;
    };

    const Organization = ({ organization }: any) => {
        return (
            <>
                {organization.label && organization.name && (
                    <div className="relative mb-5 p-4 border bg-slate-300 rounded-md text-center w-[180px] only:after:w-0 before:content-[''] after:content-['] before:top-[-10px] after:bottom-[-10px]  before:w-[2px] after:w-[2px] before:h-[10px] after:h-[10px] before:absolute after:absolute after:left-1/2  before:left-1/2 before:bg-slate-400 after:bg-slate-400  ">
                        <h5 className="font-semibold text-lg py-2"> {organization.label}</h5>
                        <Popover
                            placement="top"
                            title={false}
                            content={renderNodePopover(organization.level, organization)}
                        >
                            <div className="flex justify-center items-center bg-slate-100 rounded-md  cursor-pointer">
                                <span className="font-bold text-xl p-2 truncate">{organization.name}</span>
                            </div>
                        </Popover>
                    </div>
                )}
                {organization.organizations?.length > 0 && (
                    <ul className="flex justify-center">
                        {organization.organizations.map((subOrganization: any) => (
                            <li className="flex flex-col items-center mx-2 relative only:before:w-0 first:before:w-[calc(50%+16px)] first:before:left-1/2 last:before:w-[calc(50%+16px)] last:before:left-[-16px]  before:content-['] before:absolute before:top-[-10px] before:w-[calc(100%+16px)] before:h-[2px] before:bg-slate-400">
                                <Organization key={subOrganization.name} organization={subOrganization} />
                            </li>
                        ))}
                    </ul>
                )}
            </>
        );
    };

    return (
        <section className="bg-white">
            <div className="p-4">
                <div className="flex items-center overflow-x-auto ">
                    <Organization organization={organizations} />
                </div>
            </div>
            <Drawer
                title={openDrawer.drawerName}
                placement="right"
                onClose={() => setOpenDrawer({ ...openDrawer, isOpen: false })}
                open={openDrawer.isOpen}
                width={openDrawer.type === TYPE_DRAWER.CREATE_BRANCH ? 820 : 500}
            >
                {openDrawer.type === TYPE_DRAWER.CREATE_BRANCH && <DrawerCreateBranch />}
                {openDrawer.type === TYPE_DRAWER.CREATE_DEPARTMENT && <DrawerCreateDepartment />}
            </Drawer>
        </section>
    );
}

export default ViewOrganization;
