import { Spin } from 'antd';
import { FormikProvider, useFormik } from 'formik';
import React, { useContext } from 'react';
import InputFiled from '~/core/common/components/input-field';
import { OrganizationLogicContext } from '../../contextLogic';

import { validateOrganization } from '../../schema';

function DrawerCreateDepartment() {
    const { handleCreateOrganization, loadingCreateOrganization } = useContext(OrganizationLogicContext);
    const formHandler = useFormik({
        initialValues: {
            type: 'DEPARTMENT',
            name: '',
            label: '',
            status: 'ACTIVE',
        },
        validationSchema: validateOrganization,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (
        <div>
            <FormikProvider value={formHandler}>
                <form onSubmit={(e) => e.preventDefault()}>
                    <div className="flex flex-col ">
                        <div className="flex flex-col px-1 flex-1">
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="label"
                                    isRequire
                                    placeholder="Nhập nhãn của bộ phận"
                                    label="Tên nhãn bộ phận"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                    tutorial={<>vd: Phòng ban, tổ chức, nhóm </>}
                                />
                            </div>
                        </div>
                        <div className="flex flex-col px-1 flex-1">
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="name"
                                    isRequire
                                    placeholder="Nhập tên bộ phận!"
                                    label="Tên bộ phận"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                    tutorial={<>vd: Kế hoạc tài chính, Đào tạo, Hành chính, Nhân sự</>}
                                />
                            </div>
                        </div>
                    </div>

                    <div className="w-1/2 mx-auto">
                        <div className="mt-8">
                            <button
                                disabled={!formHandler?.dirty || !formHandler?.isValid || loadingCreateOrganization}
                                onClick={() => handleCreateOrganization(formHandler.values, formHandler.resetForm)}
                                className={` ${
                                    !formHandler?.dirty || !formHandler?.isValid ? 'disabled' : ''
                                } disabled:opacity-75 disabled:cursor-not-allowed disabled:from-slate-400 disabled:to-slate-500 outline-none border-none w-full h-full rounded-md  bg-gradient-to-r from-[#A75D5D] to-[#FFC3A1] text-2xl text-white p-3 font-medium`}
                            >
                                {loadingCreateOrganization && <Spin />} Tạo phòng ban
                            </button>
                        </div>
                    </div>
                </form>
            </FormikProvider>
        </div>
    );
}

export default DrawerCreateDepartment;
