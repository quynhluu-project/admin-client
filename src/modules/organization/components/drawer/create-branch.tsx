import { Spin } from 'antd';
import { FormikProvider, useFormik } from 'formik';
import React, { useContext } from 'react';
import InputFiled, { TypeInput } from '~/core/common/components/input-field';
import { OrganizationLogicContext } from '../../contextLogic';
import { CompanyAttributes, EnumTypeOrganization } from '~/modules/company/detail/dto';
import { validateCompany } from '../../schema';

function DrawerCreateBranch() {
    const { handlerCreateBranch, loadingCreateBranch } = useContext(OrganizationLogicContext);
    const formHandler = useFormik<CompanyAttributes>({
        initialValues: {
            managerRootName: '',
            emailRoot: '',
            companyName: '',
            address: '',
            businessArea: '',
            businessCode: '',
            portalCode: 0,
            shortCompanyName: '',
            establishmentTime: '',
            websiteUrl: '',
            introduction: '',
            phone: '',
            imageCompany: '',
            parentCompanyId: '',
            typeOrganization: EnumTypeOrganization.BRANCH,
        },
        validationSchema: validateCompany,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (
        <div>
            <FormikProvider value={formHandler}>
                <form onSubmit={(e) => e.preventDefault()}>
                    <div className="flex ">
                        <div className="flex flex-col px-1 flex-1">
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="managerRootName"
                                    isRequire
                                    placeholder="Nhập người đại diện pháp luật!"
                                    label="Tên chủ tịch"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="emailRoot"
                                    isRequire
                                    placeholder="Nhập email công ty!"
                                    label="Đại chỉ email"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>

                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="companyName"
                                    isRequire
                                    placeholder="Nhập tên công ty!"
                                    label="Tên công ty"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="address"
                                    isRequire
                                    placeholder="Nhập địa chỉ công ty!"
                                    label="Địa chỉ công ty"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="businessArea"
                                    isRequire
                                    placeholder="Nhập khu vực kinh doanh!"
                                    label="Khu vực kinh doanh"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex gap-4">
                                <div className="flex flex-col">
                                    <InputFiled
                                        nameField="businessCode"
                                        isRequire
                                        placeholder="Nhập mã số kinh doanh!"
                                        label="Mã số kinh doanh"
                                        inputClass="rounded-full px-[18px] py-[12px]"
                                        labelClass={'font-bold'}
                                    />
                                </div>
                                <div className="flex flex-col">
                                    <InputFiled
                                        nameField="portalCode"
                                        isRequire
                                        placeholder="Nhập mã số bưu chính!"
                                        label="Mã số bưu chính"
                                        inputClass="rounded-full px-[18px] py-[12px]"
                                        labelClass={'font-bold'}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-col px-1 flex-1">
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="shortCompanyName"
                                    isRequire
                                    placeholder="Nhập tên công ty rút gọn"
                                    label="Tên rút gọn"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="establishmentTime"
                                    isRequire
                                    label="Thời gian thành tập công ty"
                                    placeholder="DD/MM/YYYY"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="websiteUrl"
                                    isRequire
                                    placeholder="Nhập địa chỉ website công ty!"
                                    label="Tên website url"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="introduction"
                                    isRequire
                                    placeholder="Nhập thông tin giới thiệu công ty!"
                                    label="Thông tin giới thiệu"
                                    inputClass="rounded-md px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                    type={TypeInput.TEXTAREA}
                                    rows={5}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="phone"
                                    isRequire
                                    placeholder="Nhập số điện thoại!"
                                    label="Số điện thoại"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="flex flex-col mb-4">
                            <InputFiled
                                nameField="imageCompany"
                                isRequire
                                placeholder="Nhập hình ảnh chi nhánh"
                                label="Hình ảnh chi nhánh"
                                inputClass="rounded-full px-[18px] py-[12px]"
                                labelClass={'font-bold'}
                            />
                        </div>
                        <div className="flex items-center justify-center">
                            <img
                                src={formHandler.values.imageCompany || '/images/default-img.jpg'}
                                alt="Hình ảnh công ty"
                                className="text-center"
                            />
                        </div>
                    </div>
                    <div className="w-1/2 mx-auto">
                        <div className="mt-8">
                            <button
                                disabled={!formHandler?.dirty || !formHandler?.isValid || loadingCreateBranch}
                                onClick={() => handlerCreateBranch(formHandler.values, formHandler.resetForm)}
                                className={` ${
                                    !formHandler?.dirty || !formHandler?.isValid ? 'disabled' : ''
                                } disabled:opacity-75 disabled:cursor-not-allowed disabled:from-slate-400 disabled:to-slate-500 outline-none border-none w-full h-full rounded-md  bg-gradient-to-r from-[#A75D5D] to-[#FFC3A1] text-2xl text-white p-3 font-medium`}
                            >
                                {loadingCreateBranch && <Spin />} Yêu cầu tổ chức công ty
                            </button>
                        </div>
                    </div>
                </form>
            </FormikProvider>
        </div>
    );
}

export default DrawerCreateBranch;
