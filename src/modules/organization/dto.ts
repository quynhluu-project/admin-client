export interface OrganizationAttributes {
    id?: number;
    organizationId: string;
    companyId: string;
    organizationParentId: string;
    type: EnumTypeOrganization;
    name: string;
    label: string;
    status: EnumStatusOrganization;
    level: number;
    createdBy: string;
    updatedBy: string;
    createdAt?: string;
    updatedAt?: string;
}

export enum EnumTypeOrganization {
    BRANCH = 'BRANCH',
    DEPARTMENT = 'DEPARTMENT',
}

export enum EnumStatusOrganization {
    ACTIVE = 'ACTIVE',
    CLOSE = 'CLOSE',
}
export interface CreateOrganizationPayload {
    organizationId: string;
    companyId: string;
    organizationParentId: string;
    type: EnumTypeOrganization;
    name: string;
    label: string;
    level: number;
}
