import React from 'react';
import { OrganizationProvider } from './contextLogic';
import ViewOrganization from './components/view';

function Organization() {
    return (
        <OrganizationProvider>
            <ViewOrganization />
        </OrganizationProvider>
    );
}

export default Organization;
