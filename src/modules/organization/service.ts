import AxiosInstance from '~/core/util/axios';

import { ConfigEnv } from '~/configs';
import { CompanyAttributes } from '../company/detail/dto';
import { CreateOrganizationPayload } from './dto';

class OrganizationService {
    static async getOrganizationOfCompany(companyId?: string) {
        const result = await AxiosInstance.get(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/organization/`, {
            params: { companyId },
        });

        return result.data;
    }
    static async createBranchOfCompany(payload?: CompanyAttributes) {
        const result = await AxiosInstance.post(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/companies`, payload);
        return result.data;
    }

    static async createOrganization(payload?: CreateOrganizationPayload) {
        const result = await AxiosInstance.post(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/organization/`, payload);
        return result.data;
    }
}

export default OrganizationService;
