import React, { createContext, useMemo, useState, useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';
import OrganizationService from './service';
import LocalStorage from '~/core/util/storage';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import { CompanyAttributes } from '../company/detail/dto';
import { CreateOrganizationPayload } from './dto';

const OrganizationLogicContext = createContext({} as OrganizationLogicContextType);

type DrawerType = {
    drawerName: string;
    isOpen: boolean;
    type: string;
};

type PayloadCreateDepartment = {
    companyId: string;
    organizationParentId: string;
    level: number;
};
export interface OrganizationLogicContextType {
    navigate: ReturnType<typeof useNavigate>;
    organizationChart: any;
    openDrawer: DrawerType;
    setOpenDrawer: React.Dispatch<React.SetStateAction<DrawerType>>;
    loadingCreateBranch: boolean;
    handlerCreateBranch: (payload: CompanyAttributes, callback: any) => void;
    organizationParentId: string;
    setOrganizationParentId: React.Dispatch<React.SetStateAction<string>>;
    dataCreateDepartment: PayloadCreateDepartment;
    setDataCreateDepartment: React.Dispatch<React.SetStateAction<PayloadCreateDepartment>>;
    handleCreateOrganization: (payload: any, callback: any) => void;
    loadingCreateOrganization: boolean;
}

function OrganizationProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const [organizationChart, setOrganizationChart] = useState<any>({});
    const [organizationParentId, setOrganizationParentId] = useState<string>('');
    const [dataCreateDepartment, setDataCreateDepartment] = useState<PayloadCreateDepartment>(
        {} as PayloadCreateDepartment,
    );
    const [openDrawer, setOpenDrawer] = useState({
        drawerName: '',
        isOpen: false,
        type: '',
    });
    const { companyId } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);

    const { request } = usePrepareExecuteApi<string, any>({
        apiWillExecute: OrganizationService.getOrganizationOfCompany,
    });

    const { request: createBranchOfCompany, isLoading: loadingCreateBranch } = usePrepareExecuteApi<
        CompanyAttributes,
        any
    >({
        apiWillExecute: OrganizationService.createBranchOfCompany,
        successMes: 'Tạo chi nhánh thành công.',
        isUseServerMes: true,
    });

    const { request: createOrganization, isLoading: loadingCreateOrganization } = usePrepareExecuteApi({
        apiWillExecute: OrganizationService.createOrganization,
        successMes: 'Tạo phòng ban thành công',
        isUseServerMes: true,
    });
    const resetDrawer = () => setOpenDrawer({ drawerName: '', isOpen: false, type: '' });

    const fetchOrganization = useCallback(async () => {
        const data = await request(companyId);
        setOrganizationChart(data);
    }, [request]);

    const handlerCreateBranch = useCallback(async (value: CompanyAttributes, clearFc: any) => {
        const preparePayload = { ...value, parentCompanyId: companyId };
        const data = await createBranchOfCompany(preparePayload);
        if (data) {
            fetchOrganization();
            resetDrawer();
            clearFc();
        }
    }, []);

    const handleCreateOrganization = useCallback(
        async (value: any, clearFc: any) => {
            const preparePayload = { ...value, ...dataCreateDepartment };
            const data = await createOrganization(preparePayload);
            if (data) {
                fetchOrganization();
                resetDrawer();
                clearFc();
            }
        },
        [dataCreateDepartment],
    );

    useEffect(() => {
        companyId && fetchOrganization();
    }, []);

    const valuesContext: OrganizationLogicContextType = useMemo(
        () => ({
            navigate,
            organizationChart,
            openDrawer,
            setOpenDrawer,
            handlerCreateBranch,
            loadingCreateBranch,
            organizationParentId,
            setOrganizationParentId,
            dataCreateDepartment,
            setDataCreateDepartment,
            handleCreateOrganization,
            loadingCreateOrganization,
        }),
        [
            navigate,
            organizationChart,
            openDrawer,
            setOpenDrawer,
            handlerCreateBranch,
            loadingCreateBranch,
            organizationParentId,
            setOrganizationParentId,
            dataCreateDepartment,
            setDataCreateDepartment,
            handleCreateOrganization,
            loadingCreateOrganization,
        ],
    );

    return <OrganizationLogicContext.Provider value={valuesContext}>{children}</OrganizationLogicContext.Provider>;
}

export { OrganizationLogicContext, OrganizationProvider };
