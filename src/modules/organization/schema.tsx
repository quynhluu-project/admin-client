import * as yup from 'yup';
import { emailRegex } from '~/core/common/constants/index';
export const validateCompany = yup.object().shape({
    managerRootName: yup.string().required('Tên người đại diện không thể trống'),
    companyName: yup.string().required('Tên công ty không thể trống'),
    address: yup.string().required('Địa chỉ công ty không thể trống'),
    businessArea: yup.string().required('Khu vực kinh doanh không thể trống'),
    businessCode: yup.string().required('Mã số kinh doanh không thể trống'),
    portalCode: yup.string().required('Mã số khu vực không thể trống'),
    emailRoot: yup
        .string()
        .matches(emailRegex, 'Tài khoản email không hợp lệ')
        .required('Vui lòng nhập tài khoản email'),
});
export const validateOrganization = yup.object().shape({
    name: yup.string().required('Tên người đại diện không thể trống'),
    label: yup.string().required('Tên công ty không thể trống'),
});
