import React from 'react';
import { ManagerProvider } from './contextLogic';
import ViewManager from './components';

function Manager() {
    return (
        <ManagerProvider>
            <ViewManager />
        </ManagerProvider>
    );
}

export default Manager;
