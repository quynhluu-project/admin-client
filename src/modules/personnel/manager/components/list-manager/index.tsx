import React, { useContext, useMemo, useState } from 'react';
import { Badge, Card, Col, Row, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { ProductAttributes } from '~/modules/products/dto';
import Meta from 'antd/es/card/Meta';
import { TitleCartItem } from '~/layouts/components/header/components/title-cart-item';
import ProductPrice from '~/modules/products/create-discount/components/product-price';
import { ManagerLogicContext } from '../../contextLogic';
import { convertVnd } from '~/core/util/common';
import { DebitAttributes, EnumDebitStatus, ManagerAttributes } from '../../dto';
import { SearchIcon } from '~/core/common/icons/search';
import useDebounce from '~/core/hook/useDebounce';
import DebitService from '../../service';

const statusType: any = {
    true: 'success',
    false: 'error',
};
const statusText: any = {
    true: 'Đang hoạt động',
    false: 'Đã dừng hoạt động',
};

function ListManager() {
    const { managers, handleGetManagers } = useContext(ManagerLogicContext);

    const [search, setSearch] = useState();
    const [count, setCount] = useState(0);
    useDebounce(search, 500, async () => {
        handleGetManagers({ search });
    });

    const columns: ColumnsType<ManagerAttributes> = [
        {
            title: 'Tên nhân viên',
            dataIndex: 'name',
            key: 'name',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Ảnh nhân viên',
            key: 'totalDebit',
            render: (_, record) => {
                return (
                    <div>
                        <img
                            src={record.avatar || '/images/default-img.jpg'}
                            alt=""
                            className="w-20 h-20 rounded-md object-contain shadow-custom-4 p-1"
                        />
                    </div>
                );
            },
        },
        {
            title: 'Email',
            key: 'email',
            render: (_, record) => {
                return <div>{record.email}</div>;
            },
        },
        {
            title: 'Trạng thái',
            key: 'status',
            render: (_, record) => {
                return (
                    <Badge status={statusType[String(record.isActive)]} text={statusText[String(record.isActive)]} />
                );
            },
        },

        {
            title: 'Action',
            key: 'action',
            render: (_, record) => {
                return true ? (
                    <Space size="middle">
                        <button
                            disabled={record.isActive}
                            className="bg-transparent disabled:bg-gray-400 disabled:cursor-default disabled:text-gray-700 hover:bg-blue-500 text-blue-700 font-semibold hover:text-white disabled:border-gray-500 py-2 px-4 border border-blue-500 hover:border-transparent rounded"
                        >
                            Hoạt động
                        </button>
                        <button
                            disabled={!record.isActive}
                            className="bg-transparent disabled:bg-gray-400 disabled:cursor-default disabled:text-gray-700 hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border disabled:border-gray-500 border-red-500 hover:border-transparent rounded"
                        >
                            Dừng hoạt động
                        </button>
                    </Space>
                ) : (
                    ''
                );
            },
        },
    ];

    return (
        <div>
            <div>
                <div className="flex flex-col justify-center items-center ">
                    <h2 className="text-2xl font-medium pb-4 text-red-500 uppercase">Danh sách quản lí</h2>
                    <div className="rounded-full border-[2px] p-2">
                        <div className="bg-white lg:w-[400px] xl:w-[450px] rounded-full hidden flex-1 lg:flex">
                            <input
                                type="text"
                                placeholder="Tên quản lí, email, branchId, managerId..."
                                className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                                value={search}
                                onChange={(e: any) => setSearch(e.target.value)}
                            />
                            <button className=" rounded-full px-2 py-1">
                                <SearchIcon fill="#7286D3" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table
                columns={columns}
                dataSource={managers}
                pagination={{
                    defaultCurrent: 1,
                    total: count,
                }}
            />
        </div>
    );
}

export default ListManager;
