import { Breadcrumb } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import FormCreateManager from './form/form';
import { useFormik } from 'formik';
import ListManager from './list-manager';
import { ManagerAttributesPayload } from '../dto';

function ViewManager() {
    const formik = useFormik<ManagerAttributesPayload>({
        initialValues: {
            name: '',
            email: '',
            branchId: '',
            role: '',
            password: '',
            avatar: '',
            accessControl: '',
            createdBy: '',
            updatedBy: '',
            companyId: '',
        },
        enableReinitialize: true,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/product/create'}>Manager</Link>,
                        },
                    ]}
                />
            </div>
            <div className="bg-gray-100 p-4 mb-20">
                <div className="bg-white shadow-sm p-2">
                    <FormCreateManager formik={formik} />
                </div>
            </div>
            <div className="bg-gray-100 p-4 mb-20">
                <div className="bg-white shadow-sm p-2">
                    <ListManager />
                </div>
            </div>
        </section>
    );
}

export default ViewManager;
