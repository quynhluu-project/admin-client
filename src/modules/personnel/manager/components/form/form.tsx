/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useMemo, useState, useCallback } from 'react';
import { FormikProvider, FormikContextType } from 'formik';
import InputFiled from '~/core/common/components/input-field';
import './form.scss';
import SelectGroup from '~/core/common/components/select-group';

import { ManagerAttributesPayload } from '../../dto';
import { generatePassword } from '~/core/util/auth';
import { ManagerLogicContext } from '../../contextLogic';
import { APP_ACCESS_CONTROL, APP_ROLE_OPTIONS } from '~/core/common/constants';

function FormCreateDebit({ formik }: { formik: FormikContextType<ManagerAttributesPayload> }) {
    const { branchOptions, handleCreateManager } = useContext(ManagerLogicContext);

    const accessControl = useMemo(() => {
        if (formik.values.role === APP_ROLE_OPTIONS[0].value) {
            return APP_ACCESS_CONTROL;
        }
        if (formik.values.role === APP_ROLE_OPTIONS[1].value) {
            return APP_ACCESS_CONTROL.filter((control) => control.value !== APP_ACCESS_CONTROL[0].value);
        }
        if (formik.values.role === APP_ROLE_OPTIONS[2].value) {
            return APP_ACCESS_CONTROL.filter(
                (control) =>
                    control.value !== APP_ACCESS_CONTROL[0].value && control.value !== APP_ACCESS_CONTROL[1].value,
            );
        }
    }, [formik.values.role]);
    return (
        <FormikProvider value={formik}>
            <form>
                <h2 className="text-center p-4 text-2xl font-bold uppercase"> Biễu Mẫu Thêm Quản lí Mới </h2>

                <div className="flex items-center justify-start gap-4 mb-4">
                    <div className="w-full">
                        <InputFiled nameField="name" isRequire placeholder="Nguyễn Văn A" label="Tên quản lí" />
                    </div>
                </div>
                <div className="flex items-center justify-start gap-4 mb-4">
                    <div className="w-1/2">
                        <InputFiled
                            nameField="email"
                            isRequire
                            placeholder="nguyenvana@gmail.com"
                            label="Địa chỉ Email"
                        />
                    </div>
                    <div className="w-1/2 flex gap-4 items-end ">
                        <div className="w-[70%]">
                            <InputFiled nameField="password" isRequire placeholder="gas$#%9999" label="Mật Khẩu" />
                        </div>
                        <button
                            type="button"
                            className="text-white p-2 px-6 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                            onClick={() => {
                                formik.setFieldValue('password', generatePassword());
                            }}
                        >
                            Random mật khẩu
                        </button>
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="w-1/2">
                        <SelectGroup
                            nameField="branchId"
                            isRequire
                            label="Quản lí Chi Nhánh"
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Gas Quỳnh Lưu - Chi Nhánh Đà Nẵng"
                            options={branchOptions}
                            value={formik.values.branchId || null}
                        />
                    </div>
                    <div className="w-1/4">
                        <SelectGroup
                            nameField="role"
                            isRequire
                            label="Vai trò"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Quản lí chi nhánh"
                            options={APP_ROLE_OPTIONS}
                            value={formik.values.role || null}
                        />
                    </div>

                    {formik.values.role && (
                        <div className="w-1/4">
                            <SelectGroup
                                nameField="accessControl"
                                isRequire
                                label="Quyền Truy Cập"
                                tutorial={<div>meo meo</div>}
                                className="w-full"
                                setFieldValue={formik.setFieldValue}
                                placeholder="Cấp 1"
                                options={accessControl}
                                // mode="multiple"
                                value={formik.values.accessControl || []}
                            />
                        </div>
                    )}
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="avatar"
                            isRequire
                            placeholder="http://gasquynhluu.com/images/gas.png"
                            label="Ảnh đại diện"
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1 h-[300px] flex justify-center items-center">
                        <img
                            src={formik.values.avatar || '/images/default-img.jpg'}
                            alt=""
                            className="w-[200px] h-[200px] object-contain rounded-full"
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <button
                        type="button"
                        className="text-white p-2 px-6 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                        onClick={() => {
                            handleCreateManager(formik.values);
                            formik.resetForm();
                        }}
                    >
                        Tạo nhân sự mới
                    </button>
                </div>
            </form>
        </FormikProvider>
    );
}

export default FormCreateDebit;
