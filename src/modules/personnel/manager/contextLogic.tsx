import React, { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { APP_ROLE, LOCAL_STORAGE_KEY } from '~/core/common/constants';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';

import LocalStorage from '~/core/util/storage';
import ManagerService, { GetManagerParamsType } from './service';
import { CompanyAttributes } from '~/modules/company/detail/dto';
import { ManagerAttributes, ManagerAttributesPayload } from './dto';

const ManagerLogicContext = createContext({} as ManagerLogicContextType);

export interface ManagerLogicContextType {
    navigate: ReturnType<typeof useNavigate>;
    branchs: CompanyAttributes[];
    managers: ManagerAttributes[];
    branchOptions: { value?: string; label?: string }[];
    handleCreateManager: (payload: ManagerAttributesPayload) => Promise<any>;
    handleGetManagers: (payload: GetManagerParamsType) => Promise<any>;
}

function ManagerProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const { companyId, userAgentId } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);
    const [branchs, setBranchs] = useState<CompanyAttributes[]>([]);
    const [managers, setManagers] = useState<ManagerAttributes[]>([]);
    const { request: getBranchOfManager } = usePrepareExecuteApi<string, any>({
        apiWillExecute: ManagerService.getBranchOfManager,
        failMes: 'Lấy thông tin chi nhánh thất bại!',
    });
    const { request: getManagerByCompanyId } = usePrepareExecuteApi<GetManagerParamsType, any>({
        apiWillExecute: ManagerService.getManagerByCompanyId,
        failMes: 'Lấy thông tin danh sách quản lí thất bại!',
    });
    const { request: createManager } = usePrepareExecuteApi<ManagerAttributesPayload, any>({
        apiWillExecute: ManagerService.createManager,
        isUseServerMes: true,
        successMes: 'Đã tạo mới nhân sự thành công',
        fallback: () => handleGetManagers(),
    });

    const handleGetManagers = useCallback(async (params?: GetManagerParamsType) => {
        const {
            managers: { rows },
        } = await getManagerByCompanyId({ ...params, companyId });
        setManagers(rows);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            const [{ branchs }] = await Promise.all([getBranchOfManager(companyId), handleGetManagers()]);
            setBranchs(branchs);
        };
        fetchData();
    }, []);

    const branchOptions = useMemo(
        () => branchs.map((branch) => ({ label: branch.companyName, value: branch.companyId })),
        [branchs],
    );

    const handleCreateManager = useCallback(async (payload: ManagerAttributesPayload) => {
        const preparePayload: ManagerAttributesPayload = {
            ...payload,
            companyId,
            createdBy: userAgentId,
            updatedBy: userAgentId,
            accessControl: Array(payload.accessControl).join(','),
        };
        await createManager(preparePayload);
    }, []);

    const valuesContext: ManagerLogicContextType = useMemo(
        () => ({
            navigate,
            branchs,
            managers,
            branchOptions,
            handleCreateManager,
            handleGetManagers,
        }),
        [navigate, branchs, managers, branchOptions, handleCreateManager, handleGetManagers],
    );

    return <ManagerLogicContext.Provider value={valuesContext}>{children}</ManagerLogicContext.Provider>;
}

export { ManagerLogicContext, ManagerProvider };
