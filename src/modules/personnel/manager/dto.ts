export interface DebitAttributes {
    id?: number;
    debitId: string;
    userName: string;
    addressDebit: string;
    sellerId: string;
    products: string;
    totalDebit: number;
    evaluate: EnumEvaluate;
    status: EnumDebitStatus;
    note: string;
    phone: string;
    debitDate: Date;
    paymentAppointmentDate: Date;
    createdBy: string;
    updatedBy: string;
    createdAt?: string;
    updatedAt?: string;
}

export enum EnumEvaluate {
    DANGER = 'DANGER',
    WARNING = 'WARNING',
    SUCCESS = 'SUCCESS',
}

export enum EnumDebitStatus {
    DEBITING = 'DEBITING',
    PAYED = 'PAYED',
    DESTROY = 'DESTROY',
}

export interface DebitAttributesPayload {
    userName: string;
    addressDebit: string;
    sellerId: string;
    products: string;
    totalDebit: number;
    evaluate: EnumEvaluate | string;
    note: string;
    phone: string;
    debitDate: string;
    paymentAppointmentDate: string;
}

export interface ManagerAttributesPayload {
    name: string;
    email: string;
    companyId: string;
    branchId: string;
    role: string;
    password: string;
    avatar: string;
    accessControl: string;
    createdBy: string;
    updatedBy: string;
}

export interface ManagerAttributes {
    id: number;
    managerId: string;
    name: string;
    email: string;
    companyId: string;
    role: string;
    password: string;
    avatar: string;
    accessControl: string; // mảng chứa id của các quyền
    salt: string;
    isInit: boolean;
    isActive: boolean;
    isDelete: boolean;
    branchId: string;
    updatedBy: string;
    createdBy: string;
}
