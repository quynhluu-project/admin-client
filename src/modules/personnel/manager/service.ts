import AxiosInstance from '~/core/util/axios';
import { ConfigEnv } from '~/configs';
import { ManagerAttributesPayload } from './dto';

export type GetManagerParamsType = {
    search?: string;
    companyId?: string;
};
class ManagerService {
    static async getBranchOfManager(companyId?: string): Promise<any> {
        const result = await AxiosInstance.get(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/companies/${companyId}`);
        return result.data;
    }
    static async createManager(payload?: ManagerAttributesPayload): Promise<any> {
        const result = await AxiosInstance.post(`${ConfigEnv.ADMIN_DOMAIN}/api-v1/managers/`, payload);
        return result.data;
    }
    static async getManagerByCompanyId(params?: GetManagerParamsType): Promise<any> {
        const result = await AxiosInstance.get(`${ConfigEnv.ADMIN_DOMAIN}/api-v1/managers/list`, {
            params,
        });
        return result.data;
    }
}
export default ManagerService;
