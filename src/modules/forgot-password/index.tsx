import React from 'react';
import { ErrorMessage, Field, FormikProvider, useFormik } from 'formik';
import { Link } from 'react-router-dom';
import { validateEmail } from './schema';

function ForgotPassword() {
    const formik = useFormik({
        initialValues: {
            email: '',
        },
        validationSchema: validateEmail,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    const handleLogin = () => {
        console.log(formik.values);
    };

    return (
        <div className="flex justify-center items-center w-screen h-screen">
            <div className="w-[500px] rounded-md md:shadow-custom-4 md:px-8 py-4">
                <div className="">
                    <img src="/images/logo.png" alt="" className="w-[100px] h-[100px] mx-auto" />
                </div>
                <div className="mt-4 mb-16">
                    <h2 className="mx-auto text-3xl font-bold text-center uppercase">Quên mật khẩu</h2>
                </div>
                <div>
                    <FormikProvider value={formik}>
                        <form onSubmit={(e) => e.preventDefault()}>
                            <div className="flex flex-col px-6">
                                <div className="flex flex-col">
                                    <div className={`bg-gray-200 py-2 px-6 rounded-full border h-[50px]`}>
                                        <Field
                                            type="email"
                                            name={'email'}
                                            placeholder={'Nhập tài khoản Email'}
                                            className={'bg-transparent outline-none border-none w-full h-full'}
                                        />
                                    </div>
                                    <div className="text-red-500 p-2">
                                        <ErrorMessage name={'email'} />
                                    </div>
                                </div>

                                <div className="mt-2">
                                    <button
                                        disabled={!formik.dirty || !formik.isValid}
                                        onClick={handleLogin}
                                        className={` ${
                                            !formik.dirty || !formik.isValid ? 'disabled' : ''
                                        } disabled:opacity-75 disabled:cursor-not-allowed outline-none border-none w-full h-full rounded-full  bg-gradient-to-r from-[#A75D5D] to-[#FFC3A1] text-2xl text-white p-3 font-medium`}
                                    >
                                        Gửi mã khôi phục
                                    </button>
                                </div>
                                <div className="text-center mt-2">
                                    <Link to={'/login'} className="text-[#A75D5D] font-medium">
                                        Quay lại đăng nhập?
                                    </Link>
                                </div>
                            </div>
                        </form>
                    </FormikProvider>
                </div>
            </div>
        </div>
    );
}

export default ForgotPassword;
