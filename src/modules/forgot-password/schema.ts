import * as yup from 'yup';
import { emailRegex } from '~/core/common/constants';
export const validateEmail = yup.object().shape({
    email: yup.string().matches(emailRegex, 'Tài khoản email không hợp lệ').required('Vui lòng nhập tài khoản email'),
});
