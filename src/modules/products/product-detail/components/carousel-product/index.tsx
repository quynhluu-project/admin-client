import React, { memo, useContext } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Arrow } from '~/core/common/components/arrow';
import './carousel-proudct.scss';
import { ProductDetailLogicContext } from '../../contextLogic';
function CarouselProduct() {
    const { productDetail } = useContext(ProductDetailLogicContext);
    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 6000,
        cssEase: 'linear',
        pauseOnHover: true,
        nextArrow: <Arrow type={'NEXT'} />,
        prevArrow: <Arrow type={'PREV'} />,
        appendDots: (dots: any) => (
            <div>
                <ul style={{ margin: '0px' }}> {dots} </ul>
            </div>
        ),
        customPaging: (i: any) => (
            <div className="p-1 carousel-item">
                <img src={productDetail?.imageExtra?.split(',')[i]} alt="" className="w-full h-full object-cover" />
            </div>
        ),
    };
    return (
        <div className="flex items-center flex-col mt-4 shadow-custom-4">
            <div className="relative h-[200px] md:h-[350px] w-full">
                <Slider {...settings}>
                    {productDetail &&
                        productDetail?.imageExtra?.split(',').map((img) => {
                            return (
                                <div className="w-full h-[200px] md:h-[350px]">
                                    <img src={img} alt="" className="w-full h-full object-contain" />
                                </div>
                            );
                        })}
                </Slider>
            </div>
        </div>
    );
}

export default memo(CarouselProduct);
