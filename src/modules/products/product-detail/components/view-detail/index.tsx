import React, { useContext } from 'react';

import CarouselProduct from '../carousel-product';
import Characteristic from '../characteristic';
import { Breadcrumb, Col, Row, Tabs } from 'antd';
import { Link } from 'react-router-dom';
import { ProductDetailLogicContext } from '../../contextLogic';

function ViewProductDetail() {
    const { productDetail } = useContext(ProductDetailLogicContext);

    return (
        <main>
            <section className="bg-[#ecf0f1]">
                <div className="mx-auto container">
                    <div className="py-8">
                        <div className="bg-white p-4">
                            <Breadcrumb
                                items={[
                                    {
                                        title: <Link to={'/'}>Trang Chủ</Link>,
                                    },
                                    {
                                        title: <Link to={'/products'}>Sản phẩm</Link>,
                                    },
                                    {
                                        title: (
                                            <Link to={`/product/${productDetail?.slug}`}>
                                                {productDetail?.productName}
                                            </Link>
                                        ),
                                    },
                                ]}
                            />
                            <div className="py-4 border-b-[1px] border-[#A75D5D]">
                                <h2 className="text-xl font-bold uppercase"> {productDetail?.productName}</h2>
                            </div>
                            <div className="flex flex-wrap gap-4 justify-between">
                                <div className="w-full lg:w-[45%] mb-[60px] md:mb-[140px] ">
                                    <CarouselProduct />
                                </div>
                                <div className="w-full lg:w-[53%]">
                                    <Characteristic />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <ViewDescription /> */}
                </div>
            </section>
        </main>
    );
}

export default ViewProductDetail;
