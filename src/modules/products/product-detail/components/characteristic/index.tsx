import React, { memo, useState, useContext } from 'react';
import { Rate } from 'antd';
import { CommentIcon } from '~/core/common/icons/comment';
import { SeeIcon } from '~/core/common/icons/see';
import { Link } from 'react-router-dom';
import './characteristic.scss';
import { convertVnd } from '~/core/util/common';
import { ProductDetailLogicContext } from '../../contextLogic';
function Characteristic() {
    const { productDetail } = useContext(ProductDetailLogicContext);
    const desc = ['terrible', 'bad', 'normal', 'good', 'wonderful'];
    const [value, setValue] = useState(3);
    const [amount, setAmount] = useState(1);
    const onChange = (e: any) => {
        setAmount((prev) =>
            isNaN(Number(e.target.value)) ? prev : Number(e.target.value) > 5 ? 5 : Number(e.target.value),
        );
    };

    const handleUpdateAmount = (type?: string) => {
        if (type === 'INCREMENT') {
            setAmount((prev) => (prev > 4 ? prev : prev + 1));
            return;
        }
        setAmount((prev) => (prev > 0 ? prev - 1 : 0));
    };
    return (
        <div className="mt-8">
            <div className="p-6 mb-4 flex justify-end discount shadow-custom-4 bg-slate-100 relative">
                <div className="ribbin-genuien"></div>
                <div className="flex items-end gap-2">
                    <span className="line-through text-base md:text-xl">{convertVnd(productDetail?.price)}</span>
                    <span className="font-bold text-2xl text-red-500">
                        {convertVnd(productDetail?.price - (productDetail?.price / 100) * productDetail?.discount)}
                    </span>
                </div>
            </div>
            <div className="flex gap-4 items-center justify-start flex-wrap ">
                <div className="flex items-center gap-1">
                    <CommentIcon /> <span className="text-sm text-[#333] ">Bình luận: 1</span>
                </div>
                <div className="flex items-center gap-1">
                    <SeeIcon width={25} height={25} /> <span className="text-sm text-[#333]">lượt xem: 576</span>
                </div>
                <div className="flex items-center gap-2 mt-[-5px]">
                    <Rate tooltips={desc} onChange={setValue} value={value} />
                    <div className="text-sm text-[#333] mt-[5px]">(1 Đánh giá)</div>
                </div>
            </div>
            <div className="flex gap-2 my-4">
                <span>Mã: </span>
                <span className="text-[#A75D5D]">SHB5548MT</span>
            </div>
            {productDetail && productDetail?.salientFeatureItems && (
                <div className="flex my-4 flex-col characteristic">
                    <h3 className="uppercase font-bold">Đặt Điểm Nổi Bật</h3>
                    <div className="flex flex-col">
                        {productDetail.salientFeatureItems.map((salient) => {
                            return (
                                <div className="flex items-center">
                                    <div className="w-[6px] h-[6px] rounded-full bg-slate-300 m-2"></div>
                                    <div>{salient.description}</div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}

            <div className="flex items-center justify-center my-4 gap-2 flex-wrap">
                <button className="outline-none border-none p-2 w-full md:w-[45%]  bg-[#3498db] text-center text-sm uppercase text-white rounded-full font-bold">
                    Yêu cầu nhập hàng
                </button>
                <Link
                    to={'tel:0366361624'}
                    className="outline-none border-none p-2 w-full md:w-[45%]  bg-[#A75D5D] text-center text-sm uppercase text-white rounded-full font-bold"
                >
                    Liên hệ ngay
                </Link>
            </div>
        </div>
    );
}

export default memo(Characteristic);
