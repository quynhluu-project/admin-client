import React, { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { ProductAttributes } from '../dto';
import productService from '../service';

const ProductDetailLogicContext = createContext({} as ProductDetailLogicContextType);

export interface ProductDetailLogicContextType {
    productDetail: ProductAttributes;
    navigate: ReturnType<typeof useNavigate>;
    getProductBySlug: (slug: string) => Promise<void>;
}

function ProductDetailLogicProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const { slug } = useParams();

    const [productDetail, setProductDetail] = useState<ProductAttributes>({} as ProductAttributes);

    const getProductBySlug = useCallback(async (slug: string) => {
        const { product } = await productService.getProductBySlug(slug);
        setProductDetail(product);
    }, []);

    useEffect(() => {
        !!slug && getProductBySlug(slug);
    }, [slug]);

    const valuesContext: ProductDetailLogicContextType = useMemo(
        () => ({
            productDetail,
            navigate,
            getProductBySlug,
        }),
        [productDetail, getProductBySlug],
    );

    return <ProductDetailLogicContext.Provider value={valuesContext}>{children}</ProductDetailLogicContext.Provider>;
}

export { ProductDetailLogicContext, ProductDetailLogicProvider };
