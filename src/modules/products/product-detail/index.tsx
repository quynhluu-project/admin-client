import React, { memo } from 'react';

import { ProductDetailLogicProvider } from './contextLogic';
import ViewProductDetail from './components/view-detail';
function ProductDetail() {
    return (
        <ProductDetailLogicProvider>
            <ViewProductDetail />
        </ProductDetailLogicProvider>
    );
}

export default ProductDetail;
