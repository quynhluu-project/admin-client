import React, { useContext, useState, useEffect } from 'react';
import dayjs from 'dayjs';
import { Breadcrumb, Card, Col, DatePicker, Divider, Rate, Row, Select, Space, Modal } from 'antd';
import type { SelectProps } from 'antd';
import { Link } from 'react-router-dom';
import { DiscountProductLogicContext } from '../contextLogic';
import Meta from 'antd/es/card/Meta';
import { SearchIcon } from '~/core/common/icons/search';
import { TitleCartItem } from '~/layouts/components/header/components/title-cart-item';
import ProductPrice from './product-price';
import { ProductAttributes } from '../../dto';
import ButtonApply from '~/core/common/components/button/apply';

const ViewCreateDiscount: React.FC = () => {
    const {
        products,
        isOpen,
        weekDiscount,
        disableChoseProduct,
        productSelected,
        giftValue,
        giftProductData,
        productDiscount,
        setGiftProductData,
        setIsOpen,
        customWeekStartEndFormat,
        setWeekDiscount,
        setProductSelected,
        setGiftValue,
        handleCancelAddGift,
        handleOkAddGift,
        handleCreateProductDiscount,
        loadingCreatePromotion,
    } = useContext(DiscountProductLogicContext);

    interface ItemProps {
        label: string;
        value: string;
    }

    const options: ItemProps[] = products.map((product) => ({ label: product.productName, value: product.productId }));

    const selectProps: SelectProps = {
        mode: 'multiple',
        style: { width: '100%' },
        value: giftValue,
        options,
        onChange: (newValue: string[]) => {
            setGiftValue(newValue);
        },
        placeholder: 'Select Item...',
        maxTagCount: 'responsive',
    };

    const productDefault = [
        {
            id: 10000,
            productName: 'Sản Phẩm Khuyến Mãi 1',
            productId: 'PRODUCT_C6573E9C998Hkkyygbscfbasww',
            imageMain: '/images/default-img.jpg',
            gifts: [
                {
                    id: 10000,
                    productName: 'Quà  tặng 1',
                    productId: 'PRODUCT_C6573E9C998HGHGokhtgysvvc',
                    imageMain: '/images/default-img.jpg',
                },
            ],
        },
        {
            id: 10001,
            productName: 'Sản Phẩm Khuyến Mãi 2',
            productId: 'PRODUCT_C6573E9C998okngftdjkbvgfs',
            imageMain: '/images/default-img.jpg',
            gifts: [
                {
                    id: 10000,
                    productName: 'Quà  tặng 2',
                    productId: 'PRODUCT_C6573E9C998Hlohnamamsb',
                    imageMain: '/images/default-img.jpg',
                },
            ],
        },
        {
            id: 10002,
            productName: 'Sản Phẩm Khuyến Mãi 3',
            productId: 'PRODUCT_C6573E9C998HGugcmkkarsg',
            imageMain: '/images/default-img.jpg',
            gifts: [
                {
                    id: 10000,
                    productName: 'Quà  tặng 3',
                    productId: 'PRODUCT_C6573E9C998HGloayasvcas',
                    imageMain: '/images/default-img.jpg',
                },
            ],
        },
        {
            id: 10003,
            productName: 'Sản Phẩm Khuyến Mãi 4',
            productId: 'PRODUCT_C6573E9C998Hygbclsabtdf',
            imageMain: '/images/default-img.jpg',
            gifts: [
                {
                    id: 10000,
                    productName: 'Quà  tặng 4',
                    productId: 'PRODUCT_C6573E9C998Hlpytvlwqamoch',
                    imageMain: '/images/default-img.jpg',
                },
            ],
        },
    ];

    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/product/create'}>Product create</Link>,
                        },
                    ]}
                />
            </div>
            <div className="p-4">
                <h2 className="text-xl font-medium pb-4">Tìm kiếm sản phẩm: </h2>
                <div className="flex flex-col justify-center items-center">
                    <div className="rounded-full border-[2px] p-2">
                        <div className="bg-white lg:w-[400px] xl:w-[450px] rounded-full hidden flex-1 lg:flex">
                            <input
                                type="text"
                                placeholder="Tên sản phẩm..."
                                className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                            />
                            <button className=" rounded-full px-2 py-1">
                                <SearchIcon fill="#7286D3" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <div className="bg-white shadow-sm p-2">
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="mr-4">Lọc Theo: </div>
                        <div>
                            <Space size={[0, 8]} wrap>
                                {/* {['Tên công ty', 'Năm thành lập', 'Khu vực', 'Quy mô'].map((tag) => (
                                    <Tag.CheckableTag
                                        key={tag}
                                        checked={selectedTags.includes(tag)}
                                        onChange={() => handleTagClick(tag)}
                                    >
                                        {tag}
                                    </Tag.CheckableTag>
                                ))} */}
                            </Space>
                        </div>
                    </div>
                    <Divider orientation="left"></Divider>
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="mr-4">Sắp xếp Theo: </div>
                        <div>
                            <Select
                                defaultValue="id"
                                style={{ width: 160 }}
                                onChange={() => {}}
                                options={[
                                    { value: 'id', label: 'Mã định danh' },
                                    { value: 'createdAt', label: 'Thời gian tạo' },
                                    { value: 'updatedAt', label: 'Thời gian chỉnh sửa' },
                                    { value: 'productSize', label: 'Quy mô' },
                                ]}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <div className="bg-white shadow-sm p-2">
                    <div className="flex items-center gap-4">
                        <div>Tuần Khuyến Mãi: </div>
                        <Space direction="vertical" size={12}>
                            <DatePicker
                                defaultValue={dayjs()}
                                format={customWeekStartEndFormat}
                                picker="week"
                                onChange={(date, dateString) => setWeekDiscount(dateString)}
                            />
                        </Space>
                    </div>
                    {weekDiscount && (
                        <div className="pt-4">
                            <Row gutter={[16, 16]}>
                                {[...[...productDiscount, ...productDefault].splice(0, 4)].map((product) => {
                                    return (
                                        <Col span={6} key={product.productId}>
                                            <Card
                                                hoverable
                                                cover={
                                                    <div className="flex justify-center items-center h-[300px] ">
                                                        <img
                                                            alt="example"
                                                            src={product.imageMain}
                                                            className="mx-auto"
                                                        />
                                                    </div>
                                                }
                                            >
                                                <Meta
                                                    className="mb-2"
                                                    description={
                                                        <div className="flex flex-col">
                                                            {product?.gifts.map((gift) => {
                                                                return (
                                                                    <div className="flex justify-between items-center my-1 p-1 border rounded-md">
                                                                        <div className="line-clamp-2">
                                                                            {gift.productName}
                                                                        </div>
                                                                        <div className="w-[80px] h-[80px] object-cover">
                                                                            <img alt="example" src={gift.imageMain} />
                                                                        </div>
                                                                    </div>
                                                                );
                                                            })}
                                                        </div>
                                                    }
                                                />
                                            </Card>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </div>
                    )}
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <Row gutter={[16, 16]}>
                    {products?.map((product) => {
                        return (
                            <Col xs={24} sm={12} md={12} lg={12} xl={6} key={product.productId}>
                                <Card
                                    hoverable
                                    cover={
                                        <div className="flex justify-center items-center ">
                                            <img alt="example" src={product.imageMain} className="mx-auto" />
                                        </div>
                                    }
                                >
                                    <Meta
                                        className="mb-2"
                                        description={
                                            <>
                                                <TitleCartItem
                                                    title={product.productName}
                                                    href={`/product/${product.slug}`}
                                                />
                                                <Rate value={3} />
                                                <ProductPrice price={product.price} discount={product.discount} />
                                                <div className="flex flex-col items-center justify-center my-4 gap-2 flex-wrap">
                                                    <button
                                                        className={`outline-none border-none p-2 w-full  ${
                                                            disableChoseProduct ||
                                                            productDiscount.some(
                                                                (discount) => discount.productId === product.productId,
                                                            )
                                                                ? 'bg-[#7f8c8d]'
                                                                : 'bg-[#3498db]'
                                                        } text-center text-sm uppercase text-white rounded-full font-bold`}
                                                        disabled={
                                                            disableChoseProduct ||
                                                            productDiscount.some(
                                                                (discount) => discount.productId === product.productId,
                                                            )
                                                        }
                                                        onClick={() => {
                                                            setIsOpen(true);
                                                            setProductSelected(product);
                                                            setGiftProductData([]);
                                                            setGiftValue([]);
                                                        }}
                                                    >
                                                        Thêm vào khuyến mãi
                                                    </button>
                                                </div>
                                            </>
                                        }
                                    />
                                </Card>
                            </Col>
                        );
                    })}
                </Row>
            </div>
            <div className="bg-white fixed w-[calc(100%-256px)] bottom-0 shadow-custom-4">
                <div className="p-[7px] flex items-end justify-end">
                    <ButtonApply
                        onClick={() => {
                            handleCreateProductDiscount();
                        }}
                        isDisabled={!disableChoseProduct}
                        isLoading={loadingCreatePromotion}
                        icon={
                            <svg
                                fill="#000000"
                                width="24px"
                                height="24px"
                                viewBox="0 0 1920 1920"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="m764.386 112.941 225.882 338.824H1920v1185.882c0 88.213-67.799 160.913-154.016 168.718l-15.396.694H169.412c-88.214 0-160.913-67.799-168.718-154.016L0 1637.647V112.941h764.386Zm-60.537 112.941H112.941v1411.765c0 27.708 20.079 50.776 46.354 55.56l10.117.91h1581.176c27.608 0 50.754-19.989 55.557-46.324l.914-10.146V564.706H225.882V451.765H854.4L703.85 225.882Zm312.622 564.706v282.353h282.353v112.941H1016.47v282.353H903.529v-282.353H621.176v-112.94H903.53V790.587h112.942Z"
                                    fill-rule="evenodd"
                                />
                            </svg>
                        }
                        label={'Xác nhận thêm hàng khuyến mãi'}
                    />
                </div>
            </div>
            <Modal
                title={
                    <div className="flex flex-col justify-center items-center">
                        <div>Chọn sản phẩm khuyên mãi</div>
                        <div>{productSelected.productName}</div>
                    </div>
                }
                centered
                open={isOpen}
                onOk={handleOkAddGift}
                onCancel={handleCancelAddGift}
                okText="Đồng ý"
                okButtonProps={{
                    className: 'bg-[#4096ff]',
                }}
                cancelText="Hủy"
            >
                <div className="flex justify-center items-center mx-auto">
                    <img alt="example" className="w-[250px]" src={productSelected.imageMain} />
                </div>
                {giftProductData.length > 0 && (
                    <div className="my-2">
                        <h3>Sản phẩm tặng kèm:</h3>
                        <div className="">
                            {giftProductData.map((product) => {
                                return (
                                    <div className="flex items-center justify-between p-2 mb-1 border rounded-md">
                                        <div className="line-clamp-1 w-[300px]">{product.productName}</div>
                                        <div>
                                            <img src={product.imageMain} alt="" className="w-[80px] h-[80px]" />
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}

                <div className="mt-2">
                    <Select {...selectProps} />
                </div>
            </Modal>
        </section>
    );
};

export default ViewCreateDiscount;
