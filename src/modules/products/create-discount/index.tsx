import React from 'react';

import ViewCreateDiscount from './components/view';
import { DiscountProductLogicProvider } from './contextLogic';

const CreateDiscount: React.FC = () => {
    return (
        <DiscountProductLogicProvider>
            <ViewCreateDiscount />
        </DiscountProductLogicProvider>
    );
};

export default CreateDiscount;
