import React, { createContext, useMemo, useState, useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import productService from '../service';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { ProductAttributes, ProductAttributesWithGift } from '../dto';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';

dayjs.extend(customParseFormat);
const DiscountProductLogicContext = createContext({} as DiscountProductLogicContextType);

export interface DiscountProductLogicContextType {
    products: ProductAttributes[];
    isOpen: boolean;
    setIsOpen: React.Dispatch<boolean>;
    weekDiscount: string;
    setWeekDiscount: React.Dispatch<string>;
    disableChoseProduct: boolean;
    setDisableChoseProduct: React.Dispatch<boolean>;
    productDiscount: ProductAttributesWithGift[];
    setProductDiscount: React.Dispatch<ProductAttributesWithGift[]>;
    productSelected: ProductAttributes;
    setProductSelected: React.Dispatch<ProductAttributes>;
    giftProductData: ProductAttributes[];
    setGiftProductData: React.Dispatch<ProductAttributes[]>;
    giftValue: string[];
    setGiftValue: React.Dispatch<string[]>;
    navigate: ReturnType<typeof useNavigate>;
    customWeekStartEndFormat: (value: any) => string;
    handleCancelAddGift: () => void;
    handleOkAddGift: () => void;
    handleCreateProductDiscount: () => void;
    loadingCreatePromotion: boolean;
}
const weekFormat = 'DD/MM/YYYY';

function DiscountProductLogicProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const customWeekStartEndFormat = (value: any) =>
        `${dayjs(value).startOf('week').format(weekFormat)} ~ ${dayjs(value).endOf('week').format(weekFormat)}`;
    const [products, setProducts] = useState<ProductAttributes[]>([]);
    const [productDiscount, setProductDiscount] = useState<ProductAttributesWithGift[]>([]);

    const [weekDiscount, setWeekDiscount] = useState<string>(customWeekStartEndFormat(dayjs()));
    const [disableChoseProduct, setDisableChoseProduct] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [productSelected, setProductSelected] = useState<ProductAttributes>({} as ProductAttributes);

    const [giftValue, setGiftValue] = useState<string[]>([]);
    const [giftProductData, setGiftProductData] = useState<ProductAttributes[]>([]);

    const handleGetAllProductOfOwner = useCallback(async () => {
        const {
            products: { rows },
        } = await productService.getAllProductOfCompany({
            companyId: 'SUPER_COMPANY_CF0C92B3DE6B4E99977C53A90BC184AB',
        });
        setProducts(rows);
    }, []);

    const { request: createProductDiscount, isLoading: loadingCreatePromotion } = usePrepareExecuteApi({
        apiWillExecute: productService.createProductDiscount,
        successMes: 'Đã cập nhật sản phẩm khuyên mãi thành công',
        failMes: 'Cập nhật sản phẩm khuyến mãi thất bại',
    });

    const handleCreateProductDiscount = useCallback(async () => {
        const payload = {
            weekDiscount,
            products: productDiscount.map((product) => {
                return {
                    productId: product.productId,
                    gifts: product.gifts.map((gift) => ({ productId: gift.productId })),
                };
            }),
        };
        await createProductDiscount(payload);
        setDisableChoseProduct(false);
        setProductDiscount([]);
    }, [productDiscount, weekDiscount]);

    const handleOkAddGift = useCallback(() => {
        setProductDiscount((prev: any) => {
            if (prev.length === 4) return prev;
            prev.push({
                ...productSelected,
                gifts: [...giftProductData],
            });
            return prev;
        });
        setIsOpen(false);
    }, [productSelected, giftProductData]);

    const handleCancelAddGift = useCallback(() => {
        setIsOpen(false);
        setGiftProductData([]);
        setGiftValue([]);
    }, []);

    useEffect(() => {
        setGiftProductData(products.filter((product) => giftValue.includes(product.productId)));
    }, [giftValue]);

    useEffect(() => {
        if (productDiscount.length === 4) {
            setDisableChoseProduct(true);
            return;
        }
        setDisableChoseProduct(false);
    }, [productDiscount.length]);

    useEffect(() => {
        handleGetAllProductOfOwner();
    }, []);

    const valuesContext: DiscountProductLogicContextType = useMemo(
        () => ({
            products,
            weekDiscount,
            setWeekDiscount,
            disableChoseProduct,
            setDisableChoseProduct,
            isOpen,
            setIsOpen,
            productDiscount,
            setProductDiscount,
            productSelected,
            setProductSelected,
            giftProductData,
            setGiftProductData,
            giftValue,
            setGiftValue,
            customWeekStartEndFormat,
            handleOkAddGift,
            handleCancelAddGift,
            handleCreateProductDiscount,
            navigate,
            loadingCreatePromotion,
        }),
        [
            products,
            disableChoseProduct,
            setDisableChoseProduct,
            productDiscount,
            setProductDiscount,
            isOpen,
            setIsOpen,
            productSelected,
            setProductSelected,
            giftProductData,
            setGiftProductData,
            giftValue,
            setGiftValue,
            handleOkAddGift,
            handleCancelAddGift,
            handleCreateProductDiscount,
            navigate,
            loadingCreatePromotion,
            weekDiscount,
        ],
    );

    return (
        <DiscountProductLogicContext.Provider value={valuesContext}>{children}</DiscountProductLogicContext.Provider>
    );
}

export { DiscountProductLogicContext, DiscountProductLogicProvider };
