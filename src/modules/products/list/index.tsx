import React, { useState } from 'react';

import './list.scss';
import { ListProductProvider } from './contextLogic';
import ViewListProduct from './components/list';
function ListProduct() {
    return (
        <ListProductProvider>
            <ViewListProduct />
        </ListProductProvider>
    );
}

export default ListProduct;
