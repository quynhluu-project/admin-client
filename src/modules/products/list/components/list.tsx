import React, { useContext, useState } from 'react';
import { Breadcrumb, Pagination } from 'antd';
import { Link } from 'react-router-dom';
import { SearchIcon } from '~/core/common/icons/search';
import { Card, Col, Row, Avatar, Divider, Tooltip, Tag, Space, Select } from 'antd';
import { ListProductLogicContext } from '../contextLogic';
import { FromNow } from '~/core/util/date';
import AppLoading from '~/core/common/components/app-loading';
import PaginationApp from '~/core/common/components/pagination';
import useDebounce from '~/core/hook/useDebounce';
function ViewListProduct() {
    const { Meta } = Card;
    const { products, setFilterParams, filterParams, loadingProducts } = useContext(ListProductLogicContext);
    const [search, setSearch] = useState<string>();
    useDebounce(search, 500, () => {
        if (search !== undefined) {
            setFilterParams({ ...filterParams, productName: search });
        }
    });
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/product/list'}>Product (list)</Link>,
                        },
                    ]}
                />
                <div className="pt-4">
                    <h2 className="text-xl font-medium pb-4">Tìm kiếm sản phẩm: </h2>
                    <div className="flex flex-col justify-center items-center">
                        <div className="rounded-full border-[2px] p-2">
                            <div className="bg-white lg:w-[400px] xl:w-[450px] rounded-full hidden flex-1 lg:flex">
                                <input
                                    type="text"
                                    // value={search}
                                    placeholder="Tên sản phẩm..."
                                    className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                                    onChange={(e) => setSearch(e.target.value)}
                                />
                                <button className=" rounded-full px-2 py-1">
                                    <SearchIcon fill="#7286D3" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <div className="bg-white shadow-sm p-2">
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="mr-4">Lọc Theo: </div>
                        <div>
                            <Space size={[0, 8]} wrap>
                                {/* {['Tên công ty', 'Năm thành lập', 'Khu vực', 'Quy mô'].map((tag) => (
                                    <Tag.CheckableTag
                                        key={tag}
                                        checked={selectedTags.includes(tag)}
                                        onChange={() => handleTagClick(tag)}
                                    >
                                        {tag}
                                    </Tag.CheckableTag>
                                ))} */}
                            </Space>
                        </div>
                    </div>
                    <Divider orientation="left"></Divider>
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="mr-4">Sắp xếp Theo: </div>
                        <div>
                            <Select
                                defaultValue="id"
                                style={{ width: 160 }}
                                onChange={() => {}}
                                options={[
                                    { value: 'id', label: 'Mã định danh' },
                                    { value: 'createdAt', label: 'Thời gian tạo' },
                                    { value: 'updatedAt', label: 'Thời gian chỉnh sửa' },
                                    { value: 'productSize', label: 'Quy mô' },
                                ]}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="p-4 flex items-center justify-center">
                <PaginationApp
                    limit={filterParams.limit}
                    total={filterParams.total}
                    onChange={(page, pageSize) => {
                        setFilterParams({
                            ...filterParams,
                            limit: pageSize,
                            pageIndex: page,
                        });
                    }}
                />
            </div>
            <div className="bg-gray-100 p-4">
                {loadingProducts ? (
                    <AppLoading />
                ) : (
                    <Row gutter={[16, 16]}>
                        {products?.map((product) => {
                            return (
                                <Col span={6} key={product.productId}>
                                    <Card
                                        hoverable
                                        cover={
                                            <Link to={`/product/create?slug=${product.slug}`}>
                                                <div className="flex justify-center items-center">
                                                    <img alt="example" src={product.imageMain} />
                                                </div>
                                            </Link>
                                        }
                                    >
                                        <Meta
                                            className="mb-2"
                                            title={product?.productName}
                                            description={
                                                <div>
                                                    <p className="text-gray-800 font-normal text-base line-clamp-3">
                                                        {product.productOverview}
                                                    </p>
                                                    <div className="flex justify-between items-center mt-2">
                                                        <div className=""> {FromNow(product.createdAt)}</div>
                                                    </div>
                                                </div>
                                            }
                                        />
                                    </Card>
                                </Col>
                            );
                        })}
                    </Row>
                )}
            </div>
        </section>
    );
}

export default ViewListProduct;
