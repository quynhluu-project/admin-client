export interface ParamsSearchDTO {
    limit?: number;
    companyId?: string;
    categories?: string[];
    typeDisplay?: string;
    pageIndex?: number;
    total?: number;
}

export interface ParamsSearchProductDTO extends ParamsSearchDTO {
    productName?: string;
    categories?: string[];
}
