import React, { createContext, useMemo, useState, useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import productService from '../service';
import LocalStorage from '~/core/util/storage';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import { ProductAttributes } from '../dto';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';
import { ParamsSearchProductDTO } from './dto';

const ListProductLogicContext = createContext({} as ListProductLogicContextType);

export interface ListProductLogicContextType {
    products: ProductAttributes[];
    navigate: ReturnType<typeof useNavigate>;
    filterParams: ParamsSearchProductDTO;
    setFilterParams: React.Dispatch<React.SetStateAction<ParamsSearchProductDTO>>;
    loadingProducts: boolean;
}

function ListProductProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const { companyId } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);
    const [products, setProducts] = useState<ProductAttributes[]>([]);
    const [filterParams, setFilterParams] = useState<ParamsSearchProductDTO>({
        limit: 16,
        pageIndex: 1,
        total: 0,
    } as ParamsSearchProductDTO);

    const { request: getAllProductOfCompany, isLoading: loadingProducts } = usePrepareExecuteApi({
        apiWillExecute: productService.getAllProductOfCompany,
        isUseServerMes: true,
    });
    const handleGetAllProductOfOwner = useCallback(async () => {
        const data = await getAllProductOfCompany({ ...filterParams, companyId, total: undefined });
        console.log('🚀 ~ file: contextLogic.tsx:39 ~ handleGetAllProductOfOwner ~ data:', data);
        setProducts(data?.products?.rows);
        setFilterParams({ ...filterParams, total: data?.products?.count });
    }, [companyId, filterParams]);

    useEffect(() => {
        handleGetAllProductOfOwner();
    }, [
        filterParams.categories,
        filterParams.companyId,
        filterParams.limit,
        filterParams.pageIndex,
        filterParams.productName,
        filterParams.typeDisplay,
    ]);

    const valuesContext: ListProductLogicContextType = useMemo(
        () => ({
            products,
            navigate,
            filterParams,
            setFilterParams,
            loadingProducts,
        }),
        [products, navigate, filterParams, setFilterParams, loadingProducts],
    );

    return <ListProductLogicContext.Provider value={valuesContext}>{children}</ListProductLogicContext.Provider>;
}

export { ListProductLogicContext, ListProductProvider };
