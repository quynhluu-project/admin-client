import React from 'react';

import { CreateProductLogicProvider } from './contextLogic';
import CreateProductComponent from './components';

function ViewCreateProduct() {
    return (
        <CreateProductLogicProvider>
            <CreateProductComponent />
        </CreateProductLogicProvider>
    );
}

export default ViewCreateProduct;
