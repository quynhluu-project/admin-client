export interface ProductAttributesPayload {
    productName: string;
    categories: any[];
    price: number;
    discount: number;
    imageMain: string;
    imageExtra: string;
    productOverviewImage: string;
    productOverview: string;
    typeDisplay?: string;
    overViewSub: any[]; // danh sachs id cua mo ta san pham
    designs: any[]; // danh sachs id cua thiet ke
    functions: any[]; // danh sachs id cua  cong nang
    specifications: any[];
    salientFeatures: any[];
    slug?: string;
}

export interface ProductAttributes {
    id: number;
    productName: string;
    productId: string;
    slug: string;
    categories: string;
    price: number;
    discount: number;
    imageMain: string;
    imageExtra: string;
    productOverviewImage: string;
    productOverview: string;
    overViewSub: string; // danh sachs id cua mo ta san pham
    designItem: DesignAttributes[]; // danh sachs id cua thiet ke
    functionItem: FunctionAttributes[]; // danh sachs id cua  cong nang
    specificationItem: SpecificationAttributes[];
    salientFeatureItems: SalientFeaturesAttributes[];
    createdBy: string;
    updatedBy: string;
    createdAt: string;
    updatedAt: string;
}

export interface SalientFeaturesAttributes {
    id?: number;
    description?: string;
    productId?: string;
    createdAt?: string;
    updatedAt?: string;
}

export interface DesignAttributes {
    id: number;
    title: string;
    image: string;
    description: string;
    productId: string;
    createdAt: string;
    updatedAt: string;
}

export interface FunctionAttributes {
    id: number;
    title: string;
    image: string;
    productId: string;
    description: string;
    createdAt: string;
    updatedAt: string;
}

export interface SpecificationAttributes {
    id: number;
    title: string;
    description: string;
    productId: string;
    createdAt: string;
    updatedAt: string;
}
