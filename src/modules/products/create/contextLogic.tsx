import React, { createContext, useCallback, useEffect, useMemo, useState } from 'react';
import { ProductAttributesPayload } from './dto';
import productService from '../service';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';
import { useLocation } from 'react-router-dom';
import { ProductAttributes } from '../dto';
const CreateProductLogic = createContext({} as CreateProductContextType);

export interface CreateProductContextType {
    optionCategories: any[];
    optionTypeDisplay: any[];
    handelCreateProduct: (payload: ProductAttributesPayload, callback: (data: any) => any) => any;
    loadingCreateProduct: boolean;
    loadingUpdateProduct: boolean;
    detailProduct: ProductAttributes;
    handelUpdateProduct: (payload: ProductAttributesPayload, callback: (data: any) => any) => any;
    searchQuery: URLSearchParams;
}

function CreateProductLogicProvider({ children }: { children: React.ReactNode }) {
    const location = useLocation();
    const searchQuery = new URLSearchParams(location.search);
    const slug = searchQuery.get('slug');

    const [detailProduct, setDetailProduct] = useState<ProductAttributes>({} as ProductAttributes);
    const { request: createNewProduct, isLoading: loadingCreateProduct } = usePrepareExecuteApi<
        ProductAttributesPayload,
        any
    >({
        apiWillExecute: productService.createProduct,
        isUseServerMes: true,
        successMes: 'Sản phầm đã được thêm vào kho',
    });

    const { request: getProductBySlug } = usePrepareExecuteApi<string, any>({
        apiWillExecute: productService.getProductBySlug,
        isUseServerMes: true,
    });

    const { request: updateProduct, isLoading: loadingUpdateProduct } = usePrepareExecuteApi<
        ProductAttributesPayload,
        ProductAttributes
    >({
        apiWillExecute: productService.updateProduct,
        isUseServerMes: true,
        successMes: 'Sản phầm đã được cập nhật',
    });

    const handelCreateProduct = useCallback(
        async (payload: ProductAttributesPayload, callback: (data: any) => any) => {
            const preparePayload: ProductAttributesPayload = {
                ...payload,
                designs: payload.designs.map((des) => {
                    delete des?.productId;
                    delete des?.id;
                    delete des?.createdAt;
                    delete des?.updatedAt;
                    return { ...des };
                }),
                salientFeatures: payload.salientFeatures.map((des) => {
                    delete des?.productId;
                    delete des?.id;
                    delete des?.createdAt;
                    delete des?.updatedAt;
                    return { ...des };
                }),
                specifications: payload.specifications.map((des) => {
                    delete des?.productId;
                    delete des?.id;
                    delete des?.createdAt;
                    delete des?.updatedAt;
                    return { ...des };
                }),
                functions: payload.functions.map((des) => {
                    delete des?.productId;
                    delete des?.id;
                    delete des?.createdAt;
                    delete des?.updatedAt;
                    return { ...des };
                }),
            };
            const data = await createNewProduct(preparePayload);
            callback(data);
        },
        [createNewProduct],
    );

    const handelUpdateProduct = useCallback(
        async (payload: ProductAttributesPayload, callback: (data: any) => any) => {
            const data = await updateProduct(payload);
            callback(data);
        },
        [updateProduct],
    );

    useEffect(() => {
        const fetchDetailProduct = async () => {
            const data = await getProductBySlug(slug || '');
            setDetailProduct(data?.product);
        };
        slug && fetchDetailProduct();
    }, [slug]);

    const optionCategories = useMemo(
        () => [
            {
                label: 'Bình Gas An Toàn',
                options: [
                    { label: 'Bình Gas Công Nghiệp', value: 'binh-gas-cong-nghiep' },
                    { label: 'Bình Gas Công Nghệ', value: 'binh-gas-cong-nghe' },
                    { label: 'Bình Gas Dân Dụng', value: 'binh-gas-dan-dung' },
                    { label: 'Bình Gas Mini', value: 'binh-gas-mini' },
                ],
            },
            {
                label: 'Bếp Gas',
                options: [
                    { label: 'Bếp Gas Công Nghiệp', value: 'bep-gas-cong-nghiep' },
                    { label: 'Bếp Gas Đôi', value: 'bep-gas-doi' },
                    { label: 'Bếp Gas Đơn', value: 'bep-gas-don' },
                    { label: 'Bếp Gas Âm', value: 'bep-gas-am' },
                    {
                        label: 'Bếp Gas Mini',
                        options: [
                            { label: 'Bếp Gas Mini', value: 'bep-gas-mini' },
                            { label: 'Bếp Gas Hồng Ngoại', value: 'bep-gas-hong-ngoai' },
                        ],
                    },
                ],
            },
            {
                label: 'Bếp Điện',
                options: [
                    { label: 'Bếp Điện Từ', value: 'bep-dien-tu' },
                    { label: 'Bếp Điện Hồng Ngoại', value: 'bep-dien-hong-ngoai' },
                    { label: 'Bếp Điện Từ Hồng Ngoại', value: 'bep-dien-tu-hong-ngoai' },
                    { label: 'Bếp Điện Từ Mini', value: 'bep-dien-tu-mini' },
                    { label: 'Bếp Điện Hồng Ngoại Mini', value: 'bep-dien-hong-ngoai-mini' },
                ],
            },
            {
                label: 'Linh Kiện -Phụ Kiện',
                options: [
                    { label: 'Dây Dẫn Gas An Toàn', value: 'day-dan-gas-an-toan-linh-kien' },
                    { label: 'Van Điều Áp Gas', value: 'van-dieu-ap-gas-linh-kien' },
                    { label: 'Kiềng Bếp Gas', value: 'kieng-bep-linh-kien' },
                    { label: 'Nút Vặn Bếp Gas', value: 'nut-van-bep-linh-kien' },
                    { label: 'Máy Báo Rò Rỉ Gas', value: 'may-bao-ro-ri-gas-linh-kien' },
                ],
            },
            { label: 'Máy Hút Mùi', value: 'may-hut-mui' },
            { label: 'Máy Lọc Nước', value: 'may-loc-nuoc' },
            { label: 'Bộ Bình Gas Chính Hãng', value: 'bo-binh-gas-chinh-hang' },
            { label: 'Bếp Cũ Đổi Bếp Mới', value: 'bep-cu-doi-bep-moi' },
            { label: 'Combo Bình Và Bếp Gas', value: 'combo-binh-va-bep-gas' },
            { label: 'Quà tặng', value: 'gift' },
        ],
        [],
    );

    const optionTypeDisplay = useMemo(
        () => [
            { label: 'Sản Phẩm Mới', value: 'NEW' },
            { label: 'Sản Phẩm Giảm Giá', value: 'DISCOUNT' },
            { label: 'Quà tặng', value: 'GIFT' },
        ],
        [],
    );

    const valuesContext: CreateProductContextType = useMemo(
        () => ({
            optionCategories,
            optionTypeDisplay,
            handelCreateProduct,
            loadingCreateProduct,
            detailProduct,
            handelUpdateProduct,
            searchQuery,
            loadingUpdateProduct,
        }),
        [
            optionCategories,
            optionTypeDisplay,
            handelCreateProduct,
            loadingCreateProduct,
            detailProduct,
            handelUpdateProduct,
            searchQuery,
            loadingUpdateProduct,
        ],
    );

    return <CreateProductLogic.Provider value={valuesContext}>{children}</CreateProductLogic.Provider>;
}

export { CreateProductLogic, CreateProductLogicProvider };
