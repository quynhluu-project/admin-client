import React, { useContext } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import FormCreateProduct from './form/form';
import { CreateProductLogic } from '../contextLogic';
import { useFormik } from 'formik';
import { ProductAttributesPayload } from '../dto';
import ButtonApply from '~/core/common/components/button/apply';

function CreateProductComponent() {
    const {
        handelCreateProduct,
        handelUpdateProduct,
        loadingCreateProduct,
        detailProduct,
        searchQuery,
        loadingUpdateProduct,
    } = useContext(CreateProductLogic);
    const slug = searchQuery.get('slug');
    const formik = useFormik<ProductAttributesPayload>({
        initialValues: {
            price: detailProduct?.price || 0,
            productOverview: detailProduct?.productOverview || '',
            productOverviewImage: detailProduct?.productOverviewImage || '',
            productName: detailProduct?.productName || '',
            discount: detailProduct?.discount || 0,
            imageMain: detailProduct?.imageMain || '',
            imageExtra: detailProduct?.imageExtra || '',
            typeDisplay: detailProduct?.typeDisplay || undefined,
            overViewSub: [],
            designs: detailProduct?.designItems || [],
            functions: detailProduct?.functionItems || [],
            specifications: detailProduct?.specificationItems || [],
            categories: detailProduct?.categories?.split(',') || [],
            salientFeatures: detailProduct?.salientFeatureItems || [],
        },
        enableReinitialize: true,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/product/create'}>Product create</Link>,
                        },
                    ]}
                />
                <div className="pt-4">
                    <h2 className="text-xl font-medium pb-4">Biểu mẫu thêm sản phẩm vào hệ thống</h2>
                    <div className="flex flex-col">
                        <p>Hãy chuẩn bị: </p>
                        <div className="p-2 text-sm">
                            <ul>
                                <li>Tổng quan: hình ảnh, tên sản phẩm, giá thành, số lượng nhập, quà tặng...</li>
                                <li>Thông số kỹ thuật: chiều dài, chiều rộng, đồ dày, các phụ tùng đi kèm...</li>
                                <li>khác...</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4 mb-20">
                <div className="bg-white shadow-sm p-2">
                    <FormCreateProduct formik={formik} />
                </div>
            </div>
            <div className="bg-white fixed w-[calc(100%-256px)] bottom-0 shadow-custom-4">
                <div className="p-[7px] flex items-end justify-end gap-4">
                    <ButtonApply
                        onClick={() => {
                            !!slug
                                ? handelUpdateProduct({ ...formik.values, slug }, () => {})
                                : handelCreateProduct(formik.values, (isSuccess: any) => {
                                      !!isSuccess && formik.resetForm();
                                  });
                        }}
                        isDisabled={!formik.dirty}
                        isLoading={(!slug && loadingCreateProduct) || loadingUpdateProduct}
                        icon={
                            <svg
                                fill="#000000"
                                width="24px"
                                height="24px"
                                viewBox="0 0 1920 1920"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="m764.386 112.941 225.882 338.824H1920v1185.882c0 88.213-67.799 160.913-154.016 168.718l-15.396.694H169.412c-88.214 0-160.913-67.799-168.718-154.016L0 1637.647V112.941h764.386Zm-60.537 112.941H112.941v1411.765c0 27.708 20.079 50.776 46.354 55.56l10.117.91h1581.176c27.608 0 50.754-19.989 55.557-46.324l.914-10.146V564.706H225.882V451.765H854.4L703.85 225.882Zm312.622 564.706v282.353h282.353v112.941H1016.47v282.353H903.529v-282.353H621.176v-112.94H903.53V790.587h112.942Z"
                                    fill-rule="evenodd"
                                />
                            </svg>
                        }
                        label={slug ? 'Cập nhật sản phẩm' : 'Thêm sản phẩm'}
                    />
                    {!!slug && (
                        <ButtonApply
                            onClick={() => {
                                handelCreateProduct(formik.values, (isSuccess: any) => {
                                    // !!isSuccess && formik.resetForm();
                                });
                            }}
                            isDisabled={!formik.dirty}
                            isLoading={loadingCreateProduct}
                            icon={
                                <svg
                                    fill="#000000"
                                    width="32px"
                                    height="32px"
                                    viewBox="0 0 32 32"
                                    version="1.1"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <title>copy</title>
                                    <path d="M30.731 8.16c-0.005-0.035-0.011-0.066-0.019-0.095l0.001 0.005c-0.031-0.134-0.094-0.249-0.182-0.342l0 0-6.297-6.296c-0.093-0.087-0.208-0.15-0.336-0.181l-0.005-0.001c-0.026-0.008-0.058-0.014-0.091-0.019l-0.004-0c-0.026-0.007-0.059-0.014-0.092-0.018l-0.004-0h-9.445c-0.414 0-0.75 0.336-0.75 0.75v0 22.038c0 0.414 0.336 0.75 0.75 0.75h15.742c0.414-0 0.75-0.336 0.75-0.75v0-15.742c-0.005-0.038-0.012-0.071-0.020-0.103l0.001 0.005zM24.453 3.773l3.736 3.735h-3.736zM15.008 23.25v-20.538h7.945v5.546c0 0.414 0.336 0.75 0.75 0.75h5.547v14.242zM17.742 27.25c-0.414 0-0.75 0.336-0.75 0.75v0 1.27h-14.242v-20.539h7.25c0.414 0 0.75-0.336 0.75-0.75s-0.336-0.75-0.75-0.75v0h-8c-0.414 0-0.75 0.336-0.75 0.75v0 22.039c0 0.414 0.336 0.75 0.75 0.75h15.742c0.414-0 0.75-0.336 0.75-0.75v0-2.020c-0-0.414-0.336-0.75-0.75-0.75v0z"></path>
                                </svg>
                            }
                            label={'Sao chép sản phẩm'}
                        />
                    )}
                </div>
            </div>
        </section>
    );
}

export default CreateProductComponent;
