import React, { useContext, useMemo } from 'react';
import { FormikProvider, FieldArray, FormikContextType } from 'formik';
import InputFiled, { TypeInput } from '~/core/common/components/input-field';
import './form.scss';
import SelectGroup from '~/core/common/components/select-group';
import { ProductAttributesPayload } from '../../dto';
import { CreateProductLogic } from '../../contextLogic';

type TypeInputDesign = {
    image: string;
    description: string;
    title: string;
};

function FormCreateProduct({ formik }: { formik: FormikContextType<ProductAttributesPayload> }) {
    const { optionCategories, optionTypeDisplay } = useContext(CreateProductLogic);

    const arrayImageSub = useMemo(() => {
        return (!!formik.values.imageExtra ? formik.values.imageExtra : ',,,').split(',').slice(0, 4);
    }, [formik.values.imageExtra]);

    return (
        <FormikProvider value={formik}>
            <form>
                <div className="flex items-center justify-start gap-4 mb-4">
                    <div className="w-1/3">
                        <SelectGroup
                            nameField="categories"
                            isRequire
                            label="Chọn danh mục sản phẩm"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Vui lòng chọn danh mục cho sản phẩm"
                            options={optionCategories}
                            mode="multiple"
                            value={formik.values.categories}
                        />
                    </div>
                    <div className="w-1/3">
                        <SelectGroup
                            nameField="typeDisplay"
                            isRequire
                            label="Kiểu hiển thị"
                            tutorial={<div>meo meo</div>}
                            className="w-full"
                            setFieldValue={formik.setFieldValue}
                            placeholder="Vui lòng chọn danh mục cho sản phẩm"
                            options={optionTypeDisplay}
                            value={formik.values.typeDisplay}
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="productName"
                            isRequire
                            placeholder="Vui lòng nhập tên sản phẩm!"
                            label="Tên sản phẩm"
                            tutorial={<div>meo meo</div>}
                        />
                    </div>
                    <div className="flex-1">
                        <InputFiled
                            nameField="discount"
                            isRequire
                            placeholder="Vui lòng nhập % giảm giá!"
                            label="Giảm giá sản phẩm"
                        />
                    </div>
                    <div className="flex-1">
                        <InputFiled
                            nameField="price"
                            isRequire
                            placeholder="Vui lòng nhập giá sản phẩm!"
                            label="Giá thành sản phẩm"
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="imageMain"
                            isRequire
                            placeholder="Vui lòng nhập đường dẫn ảnh sản phẩm!"
                            label="Link ảnh sản phẩm (ảnh chính)"
                            tutorial={<div>Nhập đường dẫn đến ảnh mặc định của bạn</div>}
                            type={TypeInput.TEXTAREA}
                        />
                    </div>
                </div>
                <div className="flex items-center justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <InputFiled
                            nameField="imageExtra"
                            isRequire
                            placeholder="Vui lòng nhập đường dẫn ảnh sản phẩm!"
                            label="Link ảnh sản phẩm (ảnh phụ)"
                            tutorial={
                                <div>
                                    Nhập đường dẫn đến các ảnh phụ của sản phẩm của bạn, tối đa là 4 link ảnh và chung
                                    cách nhau bởi dấu ","
                                </div>
                            }
                            type={TypeInput.TEXTAREA}
                        />
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4">
                    <div className="flex-1">
                        <div className="w-full">
                            <img
                                src={formik.values.imageMain.trim() || '/images/default-img.jpg'}
                                alt="Ảnh sản phẩm"
                                className="w-full h-[400px] object-cover"
                            />
                        </div>
                    </div>
                    <div className="flex-1">
                        <div className="w-full flex flex-wrap gap-2 items-start">
                            {arrayImageSub.map((img, i) => {
                                return (
                                    <div className="w-[48%]" key={i}>
                                        <img
                                            src={img.trim() || '/images/default-img.jpg'}
                                            alt="Ảnh sản phẩm"
                                            className="w-full h-[196px] object-cover"
                                        />
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4 flex-col">
                    <div className="flex items-center">
                        <label htmlFor="" className="text-lg font-bold">
                            Đặc Điểm Nổi Bật:
                        </label>
                    </div>
                    <div className="w-full">
                        <FieldArray name="salientFeatures">
                            {(arrayHelpers: any) => (
                                <div className="w-full">
                                    {formik.values.salientFeatures.map((item, index) => (
                                        <div key={index} className="flex items-end gap-4">
                                            <InputFiled
                                                nameField={`salientFeatures.${index}.description`}
                                                placeholder="Vui lòng nhập mô tả đặt điểm của sản phẩm"
                                                tutorial={<div>meo meo</div>}
                                            />
                                            <div className="w-[140px]">
                                                <button
                                                    type="button"
                                                    className="text-white p-2 bg-red-500 hover:bg-red-600 active:bg-red-700 focus:outline-none focus:ring focus:ring-red-300 rounded-md"
                                                    onClick={() => arrayHelpers.remove(index)}
                                                >
                                                    Xóa đặt điểm
                                                </button>
                                            </div>
                                        </div>
                                    ))}
                                    <button
                                        type="button"
                                        className="text-white p-2 mt-4 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                                        onClick={() =>
                                            arrayHelpers.push({
                                                description: '',
                                            })
                                        }
                                    >
                                        Thêm đặt điểm
                                    </button>
                                </div>
                            )}
                        </FieldArray>
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4 flex-col">
                    <div className="flex items-center">
                        <label htmlFor="" className="text-lg font-bold">
                            Tổng Quan:
                        </label>
                    </div>
                    <div className="flex items-end gap-4 w-full">
                        <InputFiled
                            label="Ảnh mô tả tổng quan"
                            nameField="productOverviewImage"
                            placeholder="Vui lòng nhập đường dẫn ảnh tổng quan"
                            tutorial={<div>meo meo</div>}
                        />
                    </div>
                    <div className="flex-1 w-full">
                        <InputFiled
                            type={TypeInput.TEXTAREA}
                            label="Chi tiết mô tả"
                            nameField="productOverview"
                            placeholder="Vui lòng nhập mô tả tông quan của sản phẩm"
                            tutorial={<div>meo meo</div>}
                            rows="6"
                        />
                    </div>
                    <div className="w-full">
                        <FieldArray name="overViewSub">
                            {(arrayHelpers: any) => (
                                <div className="w-full">
                                    {formik.values.overViewSub.map((item, index) => (
                                        <div key={index} className="flex items-end gap-4">
                                            <InputFiled
                                                nameField={`overViewSub.${index}`}
                                                placeholder="Vui lòng nhập mô tả tông quan của sản phẩm"
                                                tutorial={<div>meo meo</div>}
                                            />
                                            <div className="w-[100px]">
                                                <button
                                                    type="button"
                                                    className="text-white p-2 bg-red-500 hover:bg-red-600 active:bg-red-700 focus:outline-none focus:ring focus:ring-red-300 rounded-md"
                                                    onClick={() => arrayHelpers.remove(index)}
                                                >
                                                    Xóa mô tả
                                                </button>
                                            </div>
                                        </div>
                                    ))}
                                    <button
                                        type="button"
                                        className="text-white p-2 mt-4 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                                        onClick={() => arrayHelpers.push('')}
                                    >
                                        Thêm mô tả
                                    </button>
                                </div>
                            )}
                        </FieldArray>
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4 flex-col">
                    <div className="flex items-center">
                        <label htmlFor="" className="text-lg font-bold">
                            Thiết kết:
                        </label>
                    </div>

                    <div className="w-full">
                        <FieldArray name="designs">
                            {(arrayHelpers: any) => (
                                <div className="w-full">
                                    {formik.values.designs.map((item: TypeInputDesign, index) => (
                                        <div key={index} className="flex items-start gap-4">
                                            <div className="w-full">
                                                <div className="flex gap-4 items-end">
                                                    <InputFiled
                                                        label={`Tiều đề thiết kế ${index + 1}`}
                                                        nameField={`designs.${index}.title`}
                                                        placeholder="Vui lòng nhập mô tả thiết kế của sản phẩm"
                                                        tutorial={<div>meo meo</div>}
                                                    />
                                                    <div className="w-[100px]">
                                                        <button
                                                            type="button"
                                                            className="text-white p-2 bg-red-500 hover:bg-red-600 active:bg-red-700 focus:outline-none focus:ring focus:ring-red-300 rounded-md"
                                                            onClick={() => arrayHelpers.remove(index)}
                                                        >
                                                            Xóa mô tả
                                                        </button>
                                                    </div>
                                                </div>
                                                <InputFiled
                                                    label="Ảnh mô tả thiết kế"
                                                    nameField={`designs.${index}.image`}
                                                    placeholder="Vui lòng nhập đường dẫn ảnh thiết kế"
                                                    tutorial={<div>meo meo</div>}
                                                />
                                                <div className="flex gap-4 items-end py-2">
                                                    <InputFiled
                                                        type={TypeInput.TEXTAREA}
                                                        label="Chi tiết mô tả thiết kế"
                                                        nameField={`designs.${index}.description`}
                                                        placeholder="Vui lòng nhập mô tả thiết kế của sản phẩm"
                                                        tutorial={<div>meo meo</div>}
                                                        rows="10"
                                                    />
                                                    <div className="w-1/2">
                                                        <img
                                                            src={item?.image.trim() || '/images/default-img.jpg'}
                                                            alt=""
                                                            className="w-full"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                    <button
                                        type="button"
                                        className="text-white p-2 mt-4 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                                        onClick={() => arrayHelpers.push({ title: '', description: '', image: '' })}
                                    >
                                        Thêm mô tả thiết kết
                                    </button>
                                </div>
                            )}
                        </FieldArray>
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4 flex-col">
                    <div className="flex items-center">
                        <label htmlFor="" className="text-lg font-bold">
                            Công Năng:
                        </label>
                    </div>

                    <div className="w-full">
                        <FieldArray name="functions">
                            {(arrayHelpers: any) => (
                                <div className="w-full">
                                    {formik.values.functions.map((item: TypeInputDesign, index) => (
                                        <div key={index} className="flex items-start gap-4">
                                            <div className="w-full">
                                                <div className="flex gap-4 items-end">
                                                    <InputFiled
                                                        label={`Tiều đề công năng ${index + 1}`}
                                                        nameField={`functions.${index}.title`}
                                                        placeholder="Vui lòng nhập mô tả công năng của sản phẩm"
                                                        tutorial={<div>meo meo</div>}
                                                    />
                                                    <div className="w-[160px]">
                                                        <button
                                                            type="button"
                                                            className="text-white p-2 bg-red-500 hover:bg-red-600 active:bg-red-700 focus:outline-none focus:ring focus:ring-red-300 rounded-md"
                                                            onClick={() => arrayHelpers.remove(index)}
                                                        >
                                                            Xóa công năng
                                                        </button>
                                                    </div>
                                                </div>
                                                <InputFiled
                                                    label="Ảnh mô tả công năng"
                                                    nameField={`functions.${index}.image`}
                                                    placeholder="Vui lòng nhập đường dẫn ảnh công năng"
                                                    tutorial={<div>meo meo</div>}
                                                />
                                                <div className="flex gap-4 items-end py-2">
                                                    <InputFiled
                                                        type={TypeInput.TEXTAREA}
                                                        label="Chi tiết mô tả công năng"
                                                        nameField={`functions.${index}.description`}
                                                        placeholder="Vui lòng nhập mô tả công năng tông quan của sản phẩm"
                                                        tutorial={<div>meo meo</div>}
                                                        rows="10"
                                                    />
                                                    <div className="w-1/2">
                                                        <img
                                                            src={item?.image.trim() || '/images/default-img.jpg'}
                                                            alt=""
                                                            className="w-full"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                    <button
                                        type="button"
                                        className="text-white p-2 mt-4 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                                        onClick={() => arrayHelpers.push({ title: '', description: '', image: '' })}
                                    >
                                        Thêm mô tả công năng
                                    </button>
                                </div>
                            )}
                        </FieldArray>
                    </div>
                </div>
                <div className="flex items-start justify-between gap-4 mb-4 flex-col">
                    <div className="flex items-center">
                        <label htmlFor="" className="text-lg font-bold">
                            Chi Tiết Thông số Kỹ Thuật:
                        </label>
                    </div>

                    <div className="w-full">
                        <FieldArray name="specifications">
                            {(arrayHelpers: any) => (
                                <div className="w-full">
                                    {formik.values.specifications.map((item: TypeInputDesign, index) => (
                                        <div key={index} className="flex items-start gap-4">
                                            <div className="w-full">
                                                <div className="flex gap-4 items-end">
                                                    <InputFiled
                                                        label={`Tiều đề thông số kỹ thuật ${index + 1}`}
                                                        nameField={`specifications.${index}.title`}
                                                        placeholder="Vui lòng nhập mô tả thông số kỹ thuật của sản phẩm"
                                                        tutorial={<div>meo meo</div>}
                                                    />
                                                    <div className="w-[206px]">
                                                        <button
                                                            type="button"
                                                            className="text-white p-2 bg-red-500 hover:bg-red-600 active:bg-red-700 focus:outline-none focus:ring focus:ring-red-300 rounded-md"
                                                            onClick={() => arrayHelpers.remove(index)}
                                                        >
                                                            Xóa thông số kỹ thuật
                                                        </button>
                                                    </div>
                                                </div>
                                                <InputFiled
                                                    label="Mô tả thông số kỹ thuật"
                                                    nameField={`specifications.${index}.description`}
                                                    placeholder="Vui lòng nhập đường dẫn ảnh thông số kỹ thuật"
                                                    tutorial={<div>meo meo</div>}
                                                />
                                            </div>
                                        </div>
                                    ))}
                                    <button
                                        type="button"
                                        className="text-white p-2 mt-4 bg-green-500 hover:bg-green-600 active:bg-green-700 focus:outline-none focus:ring focus:ring-green-300 rounded-md"
                                        onClick={() => arrayHelpers.push({ title: '', description: '' })}
                                    >
                                        Thêm mô tả thông số kỹ thuật
                                    </button>
                                </div>
                            )}
                        </FieldArray>
                    </div>
                </div>
            </form>
        </FormikProvider>
    );
}

export default FormCreateProduct;
