import AxiosInstance from '~/core/util/axios';
import { ProductAttributesPayload } from './create/dto';
import { ProductAttributes } from './dto';
import { ConfigEnv } from '~/configs';
import { ParamsSearchProductDTO } from './list/dto';
import { isNil, pickBy } from 'lodash';
class ProductService {
    constructor(readonly HTTPClient: typeof AxiosInstance) {}
    async getProductBySlug(slug?: string): Promise<{ product: ProductAttributes }> {
        const result = await AxiosInstance.get(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/product/slug`, {
            params: { slug, isGetRelated: 0 },
        });

        return result.data;
    }

    async createProduct(payload?: ProductAttributesPayload) {
        const result = await AxiosInstance.post(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/product/`, payload);
        return result?.data;
    }

    async updateProduct(payload?: ProductAttributesPayload) {
        const result = await AxiosInstance.put(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/product/`, payload);
        return result?.data;
    }

    async createProductDiscount(payload: any) {
        const result = await AxiosInstance.post(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/product/product-discount`, payload);
        return result?.data;
    }

    async getAllProductOfCompany(payload?: ParamsSearchProductDTO) {
        console.log('🚀 ~ file: service.ts:33 ~ ProductService ~ getAllProductOfCompany ~ payload:', payload);
        const newParams = pickBy(
            {
                ...payload,
            },
            (value) => !isNil(value) && value !== '',
        );
        console.log('🚀 ~ file: service.ts:39 ~ ProductService ~ getAllProductOfCompany ~ newParams:', newParams);
        const result = await AxiosInstance.get(`${ConfigEnv.PRODUCT_DOMAIN}/api-v1/product/company`, {
            params: {
                ...newParams,
            },
        });
        return result?.data;
    }
}

const productService = new ProductService(AxiosInstance);

export default productService;
