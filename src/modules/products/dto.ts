export interface ProductAttributes {
    id: number;
    productName: string;
    productId: string;
    slug: string;
    categories: string;
    price: number;
    discount: number;
    imageMain: string;
    imageExtra: string;
    productOverviewImage: string;
    productOverview: string;
    overViewSub: string; // danh sachs id cua mo ta san pham
    designItems: DesignAttributes[]; // danh sachs id cua thiet ke
    functionItems: FunctionAttributes[]; // danh sachs id cua  cong nang
    specificationItems: SpecificationAttributes[];
    salientFeatureItems: SalientFeaturesAttributes[];
    typeDisplay?: string;
    count?: string;
    createdBy: string;
    updatedBy: string;
    createdAt: string;
    updatedAt: string;
}

export interface ProductAttributesWithGift extends ProductAttributes {
    gifts: ProductAttributes[];
}

export interface SalientFeaturesAttributes {
    id?: number;
    description?: string;
    productId?: string;
    createdAt?: string;
    updatedAt?: string;
}

export interface DesignAttributes {
    id: number;
    title: string;
    image: string;
    description: string;
    productId: string;
    createdAt: string;
    updatedAt: string;
}

export interface FunctionAttributes {
    id: number;
    title: string;
    image: string;
    productId: string;
    description: string;
    createdAt: string;
    updatedAt: string;
}

export interface SpecificationAttributes {
    id: number;
    title: string;
    description: string;
    productId: string;
    createdAt: string;
    updatedAt: string;
}
