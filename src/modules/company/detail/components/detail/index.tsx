import React, { memo, useContext } from 'react';
import { Badge, Breadcrumb, Button, Col, Row, Tabs, Modal, Drawer } from 'antd';
import { Link } from 'react-router-dom';
import DescriptionCompany from '../description-company';
import ProductOfCompany from '../product-of-company';
import { DetailCompanyLogicContext } from '../../contextLogic';
import './detail.scss';
import { EnumInitState } from '../../dto';
import ButtonApply from '~/core/common/components/button/apply';
import DrawerCreateContract from '../drawer/create-contract';

function ViewDetailCompany() {
    const {
        companyId,
        companyData,
        randomPassword,
        isShowModalVerify,
        setIsShowModalVerify,
        passwordRef,
        handleShowPass,
        loadingVerify,
        setLoadingVerify,
        handleVerifyCompany,
        openDrawer,
        setOpenDrawer,
    } = useContext(DetailCompanyLogicContext);
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/company/list'}>Company (list)</Link>,
                        },
                        {
                            title: <Link to={`/company/detail/${companyId}`}>Công ty ...</Link>,
                        },
                    ]}
                />

                <div className="pt-4">
                    <div className="flex items-center">
                        <h2 className="text-xl font-medium pb-4 mr-2">Mã định danh: </h2>
                        <div className="text-[18px] font-medium pb-4">{companyId?.replace('COMPANY_', '')}</div>
                    </div>
                </div>
                <div className="">
                    <Row gutter={[16, 16]}>
                        <Col span={8}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Tên công ty: </div>
                                <div className="text-sm "> {companyData?.companyName} </div>
                            </div>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Người đại diện: </div>
                                <div className="text-sm "> {companyData?.managerRootName} </div>
                            </div>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Mã số kinh doanh: </div>
                                <div className="text-sm ">{companyData?.businessCode}</div>
                            </div>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Email quản lí: </div>
                                <div className="text-sm ">{companyData?.emailRoot}</div>
                            </div>
                        </Col>
                        <Col span={8}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Quy mô: </div>
                                <div className="text-sm "> 60 nhân sự </div>
                            </div>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Tuổi đời: </div>
                                <div className="text-sm "> 8 năm </div>
                            </div>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Ngày thành lập: </div>
                                <div className="text-sm "> 18-2-2015 </div>
                            </div>
                        </Col>
                        <Col span={8}>
                            <div className="flex mb-2 items-center">
                                <div className="text-sm font-medium mr-2">Trạng thái: </div>
                                <Badge status="processing" text="Khởi tạo" />
                            </div>
                        </Col>
                    </Row>
                </div>
                <div className="pt-4">
                    <div className="flex items-center">
                        <ButtonApply
                            onClick={() =>
                                setOpenDrawer({ drawerName: 'Biểu Mẫu Tạo Hợp Đồng Nhập Hàng', isOpen: true })
                            }
                            isDisabled={false}
                            isLoading={false}
                            icon={
                                <svg
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M13 3H7C5.89543 3 5 3.89543 5 5V10M13 3L19 9M13 3V8C13 8.55228 13.4477 9 14 9H19M19 9V19C19 20.1046 18.1046 21 17 21H10C7.79086 21 6 19.2091 6 17V17C6 14.7909 7.79086 13 10 13H13M13 13L10 10M13 13L10 16"
                                        stroke="#000000"
                                        strokeWidth="2"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                </svg>
                            }
                            label={'Tạo hợp đồng nhập hàng'}
                        />
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <Tabs
                    defaultActiveKey="1"
                    type="card"
                    items={[
                        {
                            label: `Mô Tả`,
                            key: '1',
                            children: <DescriptionCompany />,
                        },
                        {
                            label: `Sản phẩm kinh doanh`,
                            key: '2',
                            children: <ProductOfCompany />,
                        },
                    ]}
                />
            </div>
            <Modal
                title="Xác thực và khởi tạo công ty"
                open={isShowModalVerify}
                onOk={() => handleVerifyCompany(EnumInitState.APPROVED)}
                confirmLoading={loadingVerify}
                onCancel={() => {
                    setIsShowModalVerify(false);
                    setLoadingVerify(false);
                }}
                okText="Đồng ý"
                okButtonProps={{
                    className: 'bg-[#4096ff]',
                }}
                cancelText="Hủy"
            >
                <div className="flex mb-2">
                    <div className="text-sm mr-2">Tên công ty: </div>
                    <div className="text-sm "> {companyData?.companyName} </div>
                </div>
                <div className="flex mb-2">
                    <div className="text-sm mr-2">Người đại diện: </div>
                    <div className="text-sm "> {companyData?.managerRootName} </div>
                </div>
                <div className="flex mb-2">
                    <div className="text-sm mr-2">Mã số kinh doanh: </div>
                    <div className="text-sm ">{companyData?.businessCode}</div>
                </div>
                <div className="flex mb-2 flex-col">
                    <h4 className="font-medium text-base">Tạo tài khoản chính: </h4>
                    <div className="flex gap-4 mt-4">
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm mb-2" htmlFor="username">
                                Email chính
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username"
                                type="text"
                                readOnly
                                value={companyData?.emailRoot}
                                onChange={() => {}}
                            />
                        </div>
                        <div className="mb-6">
                            <label className="block text-gray-700 text-sm mb-2" htmlFor="password">
                                Mật khẩu
                            </label>
                            <div className="flex gap-2 items-center">
                                <input
                                    ref={passwordRef}
                                    className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="password"
                                    type="password"
                                    placeholder="******************"
                                    value={randomPassword}
                                    onChange={() => {}}
                                />
                                <Button onClick={handleShowPass}>show</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
            <Drawer
                title={openDrawer.drawerName}
                placement="right"
                onClose={() => setOpenDrawer({ ...openDrawer, isOpen: false })}
                open={openDrawer.isOpen}
                width={1020}
            >
                <DrawerCreateContract />
            </Drawer>
        </section>
    );
}

export default memo(ViewDetailCompany);
