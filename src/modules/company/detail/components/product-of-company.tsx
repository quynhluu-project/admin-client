import { Card, Col, Row, Empty } from 'antd';
import Meta from 'antd/es/card/Meta';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { FromNow } from '~/core/util/date';
import { DetailCompanyLogicContext } from '../contextLogic';

function ProductOfCompany() {
    const { productOfCompany } = useContext(DetailCompanyLogicContext);

    return (
        <div className="flex  w-full bg-white p-4">
            <Row gutter={[16, 16]}>
                {productOfCompany.length === 0 ? (
                    <div className="">
                        <Empty />
                    </div>
                ) : (
                    productOfCompany?.map((product) => {
                        return (
                            <Col span={6} key={product.productId}>
                                <Card
                                    hoverable
                                    cover={
                                        <Link to={`/product/detail/${product.slug}`}>
                                            <div className="flex justify-center items-center">
                                                <img alt="example" src={product.imageMain} />
                                            </div>
                                        </Link>
                                    }
                                >
                                    <Meta
                                        className="mb-2"
                                        title={product?.productName}
                                        description={
                                            <div>
                                                <p className="text-gray-800 font-normal text-base line-clamp-3">
                                                    {product.productOverview}
                                                </p>
                                                <div className="flex justify-between items-center mt-2">
                                                    <div className=""> {FromNow(product.createdAt)}</div>
                                                </div>
                                            </div>
                                        }
                                    />
                                </Card>
                            </Col>
                        );
                    })
                )}
            </Row>
        </div>
    );
}

export default ProductOfCompany;
