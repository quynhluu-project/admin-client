import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { DetailCompanyLogicContext } from '../../contextLogic';
import { FormikProvider, useFormik } from 'formik';
import { ContractAttributesPayload } from '../../dto';
import InputFiled, { InputFiledOutSite } from '~/core/common/components/input-field';
import LocalStorage from '~/core/util/storage';
import { LOCAL_STORAGE_KEY } from '~/core/common/constants';
import SelectGroup from '~/core/common/components/select-group';
import { EditorApp } from '~/core/common/components/editor';
import { Card, Col, Row, Tooltip } from 'antd';
import {
    TEMPLATE_FIRE,
    TEMPLATE_FOUR,
    TEMPLATE_ONE,
    TEMPLATE_THREE,
    TEMPLATE_TWO,
} from '~/core/util/contract-template';
import Meta from 'antd/es/card/Meta';
import { TitleCartItem } from '~/layouts/components/header/components/title-cart-item';
import ProductPrice from '~/modules/products/create-discount/components/product-price';

function DrawerCreateContract() {
    const {
        companyData,
        productOfCompany,
        productOfContract,
        handleToggleChoseProduct,
        setContractWrite,
        contractWrite,
        handleCreateContract,
        productCount,
        setProductCount,
    } = useContext(DetailCompanyLogicContext);

    const { branchId, email } =
        LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) &&
        JSON.parse(LocalStorage.get(LOCAL_STORAGE_KEY.PROFILE) as string);

    const formHandler = useFormik<ContractAttributesPayload>({
        initialValues: {
            companyIdRequest: branchId,
            companyIdResponse: companyData.companyId || '',
            detailInfor: '',
            emailLegalRepresentativeRequest: email,
            emailLegalRepresentativeResponse: companyData.emailRoot || '',
            passwordVerify: '',
            period: 'SIX_MONTH',
        },
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    const listProductChose = useMemo(() => {
        return productOfContract.map((product) => product.productId);
    }, [productOfContract]);

    const templateContract: { [key: string]: string } = useMemo(() => {
        return {
            ONE: TEMPLATE_ONE,
            TWO: TEMPLATE_TWO,
            THREE: TEMPLATE_THREE,
            FOUR: TEMPLATE_FOUR,
            FIRE: TEMPLATE_FIRE,
        };
    }, []);

    const handleCountProductSelectChange = (e: any, productId: string) => {
        setProductCount((prev) => {
            return prev.map((item) => {
                if (productId === item.productId) {
                    return {
                        ...item,
                        count: e.target.value,
                    };
                }
                return item;
            });
        });
    };
    const handleSelectTemplate = useCallback((_: any, value: string) => {
        setContractWrite(templateContract[value]);
    }, []);

    useEffect(() => {
        setProductCount(
            productOfContract.map((product, i) => {
                const oldCount = productCount.find((ele) => ele.productId === product.productId);
                return {
                    productId: product.productId,
                    count: oldCount?.count || '1',
                };
            }),
        );
    }, [productOfContract]);

    return (
        <div>
            <FormikProvider value={formHandler}>
                <form onSubmit={(e) => e.preventDefault()}>
                    <div>
                        <Row gutter={[16, 16]}>
                            {productOfContract?.map((product, i) => {
                                return (
                                    <Col xs={24} sm={12} md={12} lg={12} xl={6} key={product.productId}>
                                        <Card
                                            hoverable
                                            cover={
                                                <div className="flex justify-center items-center ">
                                                    <img alt="example" src={product.imageMain} className="mx-auto" />
                                                </div>
                                            }
                                        >
                                            <Meta
                                                className="mb-2"
                                                description={
                                                    <>
                                                        <TitleCartItem
                                                            title={product.productName}
                                                            href={`/product/${product.slug}`}
                                                        />
                                                        <ProductPrice
                                                            price={product.price}
                                                            discount={product.discount}
                                                        />
                                                        <InputFiledOutSite
                                                            nameField={`productCount[${i}]`}
                                                            isRequire
                                                            placeholder="Nhập số lượng"
                                                            onChange={(e: any) =>
                                                                handleCountProductSelectChange(e, product.productId)
                                                            }
                                                            value={productCount[i]?.count}
                                                        />
                                                    </>
                                                }
                                            />
                                        </Card>
                                    </Col>
                                );
                            })}
                        </Row>
                    </div>
                    <div className="flex ">
                        <div className="flex flex-col px-1 w-1/4">
                            <div className="flex flex-col mb-4">
                                <InputFiled
                                    nameField="passwordVerify"
                                    isRequire
                                    placeholder="ComPaneo@%$77"
                                    label="Mật khẩu xác thực"
                                    inputClass="rounded-full px-[18px] py-[12px]"
                                    labelClass={'font-bold'}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <SelectGroup
                                    nameField="period"
                                    isRequire
                                    label="Thời hạn hợp dồng"
                                    tutorial={<div>meo meo</div>}
                                    className="w-full"
                                    setFieldValue={formHandler.setFieldValue}
                                    placeholder="1 Năm"
                                    options={[
                                        { label: '6 Tháng', value: 'SIX_MONTH' },
                                        { label: '1 Năm', value: 'ONE_YEAR' },
                                        { label: '2 Năm', value: 'TWO_YEAR' },
                                        { label: 'Vô thời hạn', value: 'ETERNAL' },
                                    ]}
                                    value={formHandler.values.period}
                                />
                            </div>
                            <div className="flex flex-col mb-4">
                                <SelectGroup
                                    nameField="period"
                                    // isRequire
                                    label="Mẫu hợp đồng"
                                    tutorial={<div>meo meo</div>}
                                    className="w-full"
                                    setFieldValue={handleSelectTemplate}
                                    placeholder="Biểu mẫu N"
                                    options={[
                                        { label: 'Biểu mẫu 1', value: 'ONE' },
                                        { label: 'Biểu mẫu 2', value: 'TWO' },
                                        { label: 'Biểu mẫu 3', value: 'THREE' },
                                        { label: 'Biểu mẫu 4', value: 'FOUR' },
                                        { label: 'Biểu mẫu 5', value: 'FIRE' },
                                    ]}
                                    // value={formHandler.values.period}
                                />
                            </div>
                        </div>
                        <div className="flex flex-wrap px-1 flex-1 p-1 bg-slate-200 max-h-[400px] overflow-y-auto">
                            {productOfCompany.map((product) => {
                                return (
                                    <Tooltip title={product.productName} key={product.productId}>
                                        <div
                                            className={`w-[70px] h-[70px] p-[2px] rounded-sm  shadow-custom-4 cursor-pointer ${
                                                listProductChose.includes(product.productId)
                                                    ? 'bg-[#a75d5d]'
                                                    : 'bg-slate-200'
                                            }`}
                                            onClick={() => handleToggleChoseProduct(product)}
                                        >
                                            <img
                                                className="w-full h-full object-contain"
                                                src={product?.imageMain}
                                                alt=""
                                            />
                                        </div>
                                    </Tooltip>
                                );
                            })}
                        </div>
                    </div>
                    <EditorApp
                        onSetContent={(data) => setContractWrite(data)}
                        label="Chi tiết hợp đồng"
                        isRequire
                        height={1000}
                        initialValue={contractWrite}
                    ></EditorApp>
                    <div className="w-1/2 mx-auto">
                        <div className="mt-8">
                            <button
                                disabled={!formHandler?.dirty || !formHandler?.isValid}
                                onClick={() => {
                                    handleCreateContract({ ...formHandler.values });
                                }}
                                className={` ${
                                    !formHandler?.dirty || !formHandler?.isValid ? 'disabled' : ''
                                } disabled:opacity-75 disabled:cursor-not-allowed disabled:from-slate-400 disabled:to-slate-500 outline-none border-none w-full h-full rounded-md  bg-gradient-to-r from-[#A75D5D] to-[#FFC3A1] text-2xl text-white p-3 font-medium`}
                            >
                                Yêu nhập hàng
                            </button>
                        </div>
                    </div>
                </form>
            </FormikProvider>
        </div>
    );
}

export default DrawerCreateContract;
