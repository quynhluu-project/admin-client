import { Col, Row } from 'antd';
import React, { useContext } from 'react';
import { DetailCompanyLogicContext } from '../contextLogic';

function DescriptionCompany() {
    const { companyData } = useContext(DetailCompanyLogicContext);
    return (
        <div className="bg-white p-4">
            <div className="border">
                <div className="p-4 px-6 border-b">Thị trường kinh doanh</div>
                <div className="py-4 px-6">
                    <div className="font-bold text-lg pb-4">Khu vực</div>
                    <Row gutter={[16, 16]}>
                        <Col span={8}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Mã số bưu chính: </div>
                                <div className="text-sm "> {companyData?.portalCode} </div>
                            </div>
                        </Col>
                        <Col span={8}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Tên công ty: </div>
                                <div className="text-sm "> Gas quỳnh lưu </div>
                            </div>
                        </Col>
                        <Col span={8}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Tên công ty: </div>
                                <div className="text-sm "> Gas quỳnh lưu </div>
                            </div>
                        </Col>
                        <Col span={8}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Tên công ty: </div>
                                <div className="text-sm "> Gas quỳnh lưu </div>
                            </div>
                        </Col>
                        <Col span={16}>
                            <div className="flex mb-2">
                                <div className="text-sm mr-2">Địa chỉ: </div>
                                <div className="text-sm "> {companyData?.address}</div>
                            </div>
                        </Col>
                    </Row>
                    <div className="flex">
                        <div className="mr-2">Chi nhánh 1: </div>
                        <div> 144 Huỳnh Văn Nghệ, Hòa Quý, Ngũ Hành sơn, Đà Nẵng</div>
                    </div>
                    <div className="flex">
                        <div className="mr-2">Chi nhánh 2: </div>
                        <div> 193 Ngã ba trị an, Cẩm lệ, Ngũ Hành sơn, Đà Nẵng</div>
                    </div>
                    <div className="flex">
                        <div className="mr-2">Chi nhánh 3: </div>
                        <div> 144 Huỳnh Văn Nghệ, Hòa Quý, Ngũ Hành sơn, Đà Nẵng</div>
                    </div>
                    <div className="flex">
                        <div className="mr-2">Chi nhánh 4: </div>
                        <div> 144 Huỳnh Văn Nghệ, Hòa Quý, Ngũ Hành sơn, Đà Nẵng</div>
                    </div>
                    <div className="flex">
                        <div className="mr-2">Chi nhánh 5: </div>
                        <div> 144 Huỳnh Văn Nghệ, Hòa Quý, Ngũ Hành sơn, Đà Nẵng</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DescriptionCompany;
