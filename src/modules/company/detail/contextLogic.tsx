import React, { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import companyService from '../service';
import { CompanyAttributes, ContractAttributesPayload, EnumInitState } from './dto';
import { ProductAttributes } from '~/modules/products/create/dto';
import productService from '~/modules/products/service';
import usePrepareExecuteApi from '~/core/hook/usePrepareExecuteApi';
import ContractService from '~/modules/book/contract/service';

const DetailCompanyLogicContext = createContext({} as DetailCompanyLogicContextType);

export interface DetailCompanyLogicContextType {
    companyId?: string;
    companyData: CompanyAttributes;
    randomPassword: string;
    isShowModalVerify: boolean;
    passwordRef: React.MutableRefObject<HTMLInputElement | any>;
    loadingVerify: boolean;
    productOfCompany: ProductAttributes[];
    openDrawer: DrawerType;
    setOpenDrawer: React.Dispatch<React.SetStateAction<DrawerType>>;
    setLoadingVerify: React.Dispatch<boolean>;
    setIsShowModalVerify: React.Dispatch<boolean>;
    navigate: ReturnType<typeof useNavigate>;
    handleShowPass: () => void;
    handleVerifyCompany: (status: EnumInitState) => void;
    productOfContract: ProductAttributes[];
    setProductOfContract: React.Dispatch<React.SetStateAction<ProductAttributes[]>>;
    handleToggleChoseProduct: (data: ProductAttributes) => void;
    contractWrite: string;
    setContractWrite: React.Dispatch<React.SetStateAction<string>>;
    handleCreateContract: (payload: any) => void;
    productCount: ProductCountType[];
    setProductCount: React.Dispatch<React.SetStateAction<ProductCountType[]>>;
}
export type ProductCountType = {
    productId: string;
    count: string;
};
type DrawerType = {
    drawerName: string;
    isOpen: boolean;
};

function DetailCompanyProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();
    const { companyId } = useParams();
    const [companyData, setCompanyData] = useState<CompanyAttributes>({});
    const [randomPassword, setRandomPassword] = useState('');
    const [isShowModalVerify, setIsShowModalVerify] = useState(false);
    const [loadingVerify, setLoadingVerify] = useState(false);
    const [productOfCompany, setProductOfCompany] = useState<ProductAttributes[]>([]);
    const [openDrawer, setOpenDrawer] = useState<DrawerType>({ drawerName: '', isOpen: false });

    const [productOfContract, setProductOfContract] = useState<ProductAttributes[]>([]);
    const [contractWrite, setContractWrite] = useState<string>('');
    const passwordRef = useRef<HTMLInputElement>();

    const [productCount, setProductCount] = useState<ProductCountType[]>([]);
    const { request: createContract } = usePrepareExecuteApi({
        apiWillExecute: ContractService.createContract,
    });

    const handleCreateContract = useCallback(
        async (payload: ContractAttributesPayload) => {
            const preparePayload = {
                ...payload,
                detailInfor: JSON.stringify({
                    detailContract: contractWrite,
                    products: productOfContract.map((pro, i) => ({ ...pro, count: productCount[i].count })),
                }),
            };
            await createContract(preparePayload);
            setOpenDrawer({ drawerName: '', isOpen: false });
        },
        [contractWrite, productOfCompany, productCount],
    );
    const getCompany = useCallback(async () => {
        if (companyId) {
            const { company } = await companyService.getDetailCompany(companyId);
            setCompanyData(company);
        }
    }, [companyId]);

    const getProductOfCompany = useCallback(async () => {
        if (companyId) {
            const {
                products: { rows },
            } = await productService.getAllProductOfCompany({ companyId });
            setProductOfCompany(rows);
        }
    }, [companyId]);
    const handleRandomPassword = () => {
        // setRandomPassword(generatePassword());
    };

    const handleShowPass = () => {
        const input = passwordRef.current;
        if (input) {
            input.type = input.type === 'password' ? 'text' : 'password';
        }
    };

    const handleToggleChoseProduct = useCallback(
        (product: ProductAttributes) => {
            if (productOfContract.some((pro) => product.productId === pro.productId)) {
                console.log(312312);

                setProductOfContract(productOfContract.filter((proc) => proc.productId !== product.productId));
            } else {
                setProductOfContract([...productOfContract, product]);
            }
        },
        [productOfContract],
    );

    const handleVerifyCompany = useCallback(
        async (status: EnumInitState) => {
            setLoadingVerify(true);
            const payload = {
                emailRoot: companyData.emailRoot,
                password: randomPassword,
                companyId: companyData.companyId,
                status,
            };

            const { isUpdated } = await companyService.updateStatusCompany(payload);
            if (isUpdated) {
                getCompany();
            }
            setLoadingVerify(false);
            setIsShowModalVerify(false);
        },
        [companyData, randomPassword],
    );
    useEffect(() => {
        if (!isShowModalVerify) {
            const input = passwordRef.current;
            if (input) {
                input.type = 'password';
            }
        }

        handleRandomPassword();
    }, [isShowModalVerify]);

    useEffect(() => {
        getCompany();
        getProductOfCompany();
    }, [companyId]);

    const valuesContext: DetailCompanyLogicContextType = useMemo(
        () => ({
            companyId,
            companyData,
            randomPassword,
            isShowModalVerify,
            passwordRef,
            loadingVerify,
            productOfCompany,
            openDrawer,
            setOpenDrawer,
            setLoadingVerify,
            setIsShowModalVerify,
            navigate,
            handleShowPass,
            handleVerifyCompany,
            productOfContract,
            setProductOfContract,
            handleToggleChoseProduct,
            contractWrite,
            setContractWrite,
            handleCreateContract,
            productCount,
            setProductCount,
        }),
        [
            companyId,
            companyData,
            randomPassword,
            isShowModalVerify,
            passwordRef,
            loadingVerify,
            productOfCompany,
            openDrawer,
            setOpenDrawer,
            setLoadingVerify,
            setIsShowModalVerify,
            navigate,
            handleVerifyCompany,
            productOfContract,
            setProductOfContract,
            handleToggleChoseProduct,
            contractWrite,
            setContractWrite,
            handleCreateContract,
            productCount,
            setProductCount,
        ],
    );

    return <DetailCompanyLogicContext.Provider value={valuesContext}>{children}</DetailCompanyLogicContext.Provider>;
}

export { DetailCompanyLogicContext, DetailCompanyProvider };
