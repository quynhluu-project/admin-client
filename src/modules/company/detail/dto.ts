export enum EnumTypeOrganization {
    COMPANY = 'COMPANY',
    BRANCH = 'BRANCH',
}
export interface CompanyAttributes {
    id?: number;
    companyName?: string;
    managerRootName?: string;
    emailRoot?: string;
    companyId?: string;
    address?: string;
    businessCode?: string;
    businessArea?: string;
    portalCode?: number;
    initState?: EnumInitState;
    approvedBy?: string;
    shortCompanyName?: string;
    establishmentTime?: string;
    websiteUrl?: string;
    introduction?: string;
    phone?: string;
    typeOrganization?: EnumTypeOrganization;
    parentCompanyId?: string;
    imageCompany?: string;
    createdAt?: string;
    updatedAt?: string;
}

export interface GetCompanyResponse {
    company: CompanyAttributes;
}
export interface GetAllCompanyResponse {
    companies: CompanyAttributes[];
}

export enum EnumInitState {
    CREATED = 'created',
    APPROVED = 'approved',
    ACTIVE = 'active',
}

export interface ContractAttributesPayload {
    companyIdRequest: string;
    companyIdResponse: string;
    period: string;
    detailInfor: string;
    passwordVerify: string;
    emailLegalRepresentativeRequest: string;
    emailLegalRepresentativeResponse: string;
}
