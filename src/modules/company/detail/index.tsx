import React from 'react';
import ViewDetailCompany from './components/detail';
import { DetailCompanyProvider } from './contextLogic';

function DetailCompany() {
    return (
        <DetailCompanyProvider>
            <ViewDetailCompany />
        </DetailCompanyProvider>
    );
}

export default DetailCompany;
