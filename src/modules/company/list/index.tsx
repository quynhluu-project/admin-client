import React, { useState } from 'react';

import './list.scss';
import { ListCompanyProvider } from './contextLogic';
import ViewListCompany from './components/list';
function ListCompany() {
    return (
        <ListCompanyProvider>
            <ViewListCompany />
        </ListCompanyProvider>
    );
}

export default ListCompany;
