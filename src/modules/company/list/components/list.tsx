import React, { useContext } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import { SearchIcon } from '~/core/common/icons/search';
import { Card, Col, Row, Avatar, Divider, Tooltip, Tag, Space, Select } from 'antd';
import { ListCompanyLogicContext } from '../contextLogic';
function ViewListCompany() {
    const { Meta } = Card;
    const { handleChange, selectedTags, handleTagClick, companies, navigate } = useContext(ListCompanyLogicContext);
    return (
        <section className="bg-white">
            <div className="p-4">
                <Breadcrumb
                    items={[
                        {
                            title: <Link to={'/'}>Home</Link>,
                        },
                        {
                            title: <Link to={'/company/list'}>Company (list)</Link>,
                        },
                    ]}
                />
                <div className="pt-4">
                    <h2 className="text-xl font-medium pb-4">Tìm kiếm công ty: </h2>
                    <div className="flex flex-col justify-center items-center">
                        <div className="rounded-full border-[2px] p-2">
                            <div className="bg-white lg:w-[400px] xl:w-[450px] rounded-full hidden flex-1 lg:flex">
                                <input
                                    type="text"
                                    placeholder="Tên công ty..."
                                    className="bg-transparent flex-1 outline-none border-none px-4 text-sm"
                                />
                                <button className=" rounded-full px-2 py-1">
                                    <SearchIcon fill="#7286D3" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <div className="bg-white shadow-sm p-2">
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="mr-4">Lọc Theo: </div>
                        <div>
                            <Space size={[0, 8]} wrap>
                                {['Tên công ty', 'Năm thành lập', 'Khu vực', 'Quy mô'].map((tag) => (
                                    <Tag.CheckableTag
                                        key={tag}
                                        checked={selectedTags.includes(tag)}
                                        onChange={() => handleTagClick(tag)}
                                    >
                                        {tag}
                                    </Tag.CheckableTag>
                                ))}
                            </Space>
                        </div>
                    </div>
                    <Divider orientation="left"></Divider>
                    <div className="flex items-center mt-2 mx-2 text-lg">
                        <div className="mr-4">Sắp xếp Theo: </div>
                        <div>
                            <Select
                                defaultValue="id"
                                style={{ width: 160 }}
                                onChange={handleChange}
                                options={[
                                    { value: 'id', label: 'Mã định danh' },
                                    { value: 'createdAt', label: 'Thời gian tạo' },
                                    { value: 'updatedAt', label: 'Thời gian chỉnh sửa' },
                                    { value: 'companySize', label: 'Quy mô' },
                                ]}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-100 p-4">
                <Row gutter={[16, 16]}>
                    {companies?.map((company) => {
                        return (
                            <Col span={6} key={company.companyId}>
                                <Card
                                    hoverable
                                    cover={
                                        <Link to={`/company/detail/${company.companyId}`}>
                                            {/* {company?.approvedBy ? (
                                                <img alt="example" src="/images/company.png" />
                                            ) : (
                                                <img alt="example" src="/images/company.png" />
                                            )} */}
                                            <div>
                                                <img alt="example" src="/images/company.png" />
                                                {/* <div className="my-3"> {company.companyName}</div> */}
                                            </div>
                                        </Link>
                                    }
                                >
                                    <Meta
                                        className="mb-2"
                                        title={company?.companyName}
                                        description={
                                            <div>
                                                <p className="text-gray-800 font-normal text-base">
                                                    Đây là công ty chuyên về cung cấp gas khắp thành phố đà nẵng
                                                </p>
                                                <div className="flex justify-between items-center mt-2">
                                                    <div className=""> 5 năm trước</div>
                                                    <div>
                                                        <Avatar.Group>
                                                            <Avatar src="https://xsgames.co/randomusers/avatar.php?g=pixel&key=1" />
                                                            <a href="https://ant.design">
                                                                <Avatar style={{ backgroundColor: '#f56a00' }}>
                                                                    K
                                                                </Avatar>
                                                            </a>
                                                            <Tooltip title="Ant User" placement="top">
                                                                <Avatar
                                                                    style={{ backgroundColor: '#87d068' }}
                                                                    // icon={<UserOutlined />}
                                                                />
                                                            </Tooltip>
                                                            <Avatar
                                                                style={{ backgroundColor: '#1677ff' }}
                                                                // icon={<AntDesignOutlined />}
                                                            />
                                                        </Avatar.Group>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    />
                                </Card>
                            </Col>
                        );
                    })}
                </Row>
            </div>
        </section>
    );
}

export default ViewListCompany;
