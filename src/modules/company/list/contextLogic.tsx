import React, { createContext, useMemo, useState, useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import companyService from '../service';
import { CompanyAttributes } from '../detail/dto';

const ListCompanyLogicContext = createContext({} as ListCompanyLogicContextType);

export interface ListCompanyLogicContextType {
    selectedTags: any;
    setSelectedTags: React.Dispatch<any>;
    companies: CompanyAttributes[];
    navigate: ReturnType<typeof useNavigate>;
    handleChange: (args: string) => void;
    handleTagClick: (args: any) => void;
}

function ListCompanyProvider({ children }: { children: React.ReactNode }) {
    const navigate = useNavigate();

    const [selectedTags, setSelectedTags] = useState<any>([]);
    const [companies, setCompanies] = useState<CompanyAttributes[]>([]);
    const handleTagClick = (tag: any) => {
        if (selectedTags.includes(tag)) {
            setSelectedTags(selectedTags.filter((t: any) => t !== tag));
        } else {
            setSelectedTags([...selectedTags, tag]);
        }
    };

    const handleChange = (value: string) => {
        console.log(`selected ${value}`);
    };

    const getAllCompany = useCallback(async () => {
        const { companies } = await companyService.getAllCompany();
        setCompanies(companies);
    }, []);

    useEffect(() => {
        getAllCompany();
    }, []);

    const valuesContext: ListCompanyLogicContextType = useMemo(
        () => ({
            selectedTags,
            setSelectedTags,
            companies,
            navigate,
            handleChange,
            handleTagClick,
        }),
        [selectedTags, setSelectedTags, navigate, companies],
    );

    return <ListCompanyLogicContext.Provider value={valuesContext}>{children}</ListCompanyLogicContext.Provider>;
}

export { ListCompanyLogicContext, ListCompanyProvider };
