import AxiosInstance from '~/core/util/axios';
import { GetAllCompanyResponse, GetCompanyResponse } from './detail/dto';
import { showMessageNotification } from '~/core/util/common';
import { ConfigEnv } from '~/configs';

class CompanyService {
    constructor(readonly HTTPClient: typeof AxiosInstance) {}

    async getDetailCompany(companyId: string): Promise<GetCompanyResponse> {
        try {
            const result = await this.HTTPClient.get(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/companies/`, {
                params: { companyId },
            });

            return result.data;
        } catch (error) {
            return {
                company: {},
            };
        }
    }

    async getAllCompany(): Promise<GetAllCompanyResponse> {
        try {
            const result = await this.HTTPClient.get(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/companies/all`);
            return result.data;
        } catch (error: any) {
            showMessageNotification({
                type: 'error',
                title: `Lấy thông tin tất cả công ty thất bại`,
                description: error.message,
            });
            return {
                companies: [],
            };
        }
    }

    async updateStatusCompany(payload: any): Promise<{ isUpdated: boolean }> {
        try {
            const result = await this.HTTPClient.put(`${ConfigEnv.COMPANY_DOMAIN}/api-v1/companies/status`, payload);
            return result.data;
        } catch (error: any) {
            showMessageNotification({
                type: 'error',
                title: `Cập nhật xác thực công ty thất bại`,
                description: error.message,
            });
            return {
                isUpdated: false,
            };
        }
    }
}

const companyService = new CompanyService(AxiosInstance);

export default companyService;
