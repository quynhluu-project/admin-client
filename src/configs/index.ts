import { APP_ENV } from '~/core/common/constants';
import configLocal from './local.json';
import configProduction from './production.json';
import configDev from './dev.json';

const getConfig = () => {
    if (process.env.REACT_APP_ENV === APP_ENV.PRODUCTION) {
        return configProduction;
    } else if (process.env.REACT_APP_ENV === APP_ENV.DEV) {
        return configDev;
    } else {
        return configLocal;
    }
};

const ConfigEnv = getConfig();

export { ConfigEnv };
